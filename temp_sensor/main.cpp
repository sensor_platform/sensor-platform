/* Nick van den Berg - Hogeschool Rotterdam - IHC Systems B.V.
 * Made for MSP430G2452 microcontroller
 * Used documents:
 * msp430x2xx family user guide
 * msp430g2452 datasheet
 * ads1148 datasheet                                                */

/*  MSP430G2452 - Temperature sensor without interrupts
 *
 *  Description: SPI Master communicates full-duplex with
 *  SPI Slave using 3-wire mode.
 *  ACLK = n/a, MCLK = SMCLK = Default DCO (1MHz)
 *
 *                    Slave                     Master
 *                 ADS1148IPW              MSP430G2452IPW14
 *             -----------------          -----------------
 *            |                 | /|\ /|\|                 |
 *            |                 |  |   | |                 |
 *            |              RST|--    --|RST            ??|->LED
 *            |                 |        |                 |
 *            |              nCS|<-------|2/P1.0/CS        |
 *            |            nDRDY|------->|13/2.6/DRDY      |
 *            |            START|<-------|12/2.7/START     |
 *            |                 |        |                 |
 *            |           DIN/27|<-------|8/P1.6/SDO       |
 *            |          DOUT/26|------->|9/P1.7/SDI       |
 *            |          SCLK/28|<-------|7/P1.5/SCLK      |
 *             -----------------          -----------------
 *                             <-01100111<-
 *                               |      |
 *                              \|/    \|/
 *                              MSB    LSB
 *
 * RAM: 0.25kB ROM: 8kB
 * Comments: Compiler complains about uninitialized ports, the ports are initialized in PortAddr constructor
 * (all disabled), the compiler only recognizes explicit initialization.                                        */

/* Libraries */
#include "src/types.hpp"
#include "src/gpio.hpp"
#include "src/usi.hpp"
#include "src/ads1148.hpp"
#include "src/mcp2517fd.hpp"

/* Namespaces */
using GPIO::Gpio;
using SPI::SpiSettings;
using ADS1148::Ads1148;
using MCP2517FD::Mcp2517fd;

/* Constants */
#define SCLK_PIN PORT_1, PIN_5    // P1.5 = SPI clock
#define SDO_PIN PORT_1, PIN_6     // P1.6 = Master data out
#define SDI_PIN PORT_1, PIN_7     // P1.7 = Master data in

/* Functions */
//uint16_t convertRtdResistanceToTemperature(int16_t resistance)
//{
//    /* T = 0.37986x + 100.30694 is used (from RTD research excel sheet: "Limited range 2") */
//    int32_t temperature =  static_cast<int32_t>(resistance);
//    temperature = (temperature*100000) - 1003069400;
//    temperature = temperature / 37986;
//    return temperature;
//}


/* Main */
int main(void)
 {
    // Watchdog timer
    WDTCTL = WDTPW + WDTHOLD;                       // Stop watchdog timer

    // Clock 8 MHz
    DCOCTL = 0;
    BCSCTL1 = CALBC1_8MHZ;
    DCOCTL = CALDCO_8MHZ;

    // GPIO
    PIN::Pin::resetPorts();
    Gpio startPin(PORT_B, PIN_7, OUTPUT, LOW);     // P2.7 (12) = START (start conversion needs to be high for

    Gpio drdyPin(PORT_B, PIN_6, INPUT, LOW);        // P2.6 (13) = DRDY  (conversion done)

    Gpio csPin(PORT_A, PIN_0, OUTPUT, LOW);        // P1.0 (2)  = chip select -> High = ADC && Low = CAN-FD (Both active low using external transistor)

    __delay_cycles(1000000); // Let devices settle (turn-on time)

    ///* Temperature sensor */
    SpiSettings tempSpiSettings(
    USI::MASTER,
    USI::SMCLK,
    USI::DIV_1,
    USI::BIT_MODE_8,
    USI::MSB,
    USI::INACTIVE_LOW,
    USI::PHASE_MODE_0,
    USI::LATCH_MODE_0);

    Ads1148 tempSensor(SCLK_PIN, SDO_PIN, SDI_PIN, csPin, HIGH, startPin, drdyPin, tempSpiSettings);

    /* CAN controller */
    const SpiSettings canSpiSettings(
                                    USI::MASTER,
                                    USI::SMCLK,
                                    USI::DIV_1,
                                    USI::BIT_MODE_8,
                                    USI::MSB,
                                    USI::INACTIVE_LOW,
                                    USI::PHASE_MODE_1,
                                    USI::LATCH_MODE_0);

    Mcp2517fd canController(SCLK_PIN, SDO_PIN, SDI_PIN, csPin, LOW, canSpiSettings);


    volatile uint8_t size = sizeof(tempSensor);
    volatile uint8_t size2 = sizeof(canController);

    // Low Power Mode
    //__bis_SR_register(LPM0_bits + GIE); // Enter LPM0 w/ interrupt

    // Loop variables
    volatile int16_t resistance_1 = 0x0;
    volatile int16_t resistance_2 = 0x0;
    enum class Event{NORMAL = 0x1, NONE = 0x2};
    Event event = Event::NORMAL;
    // Loop
    while(true)
    {
        // <-- reset watchdog here
       // Main event loop
    switch(event)
        {
            case Event::NORMAL: // Check sensors and send data
            {
                resistance_1 = tempSensor.measSensor1(); // Sensor 1
                resistance_2 = tempSensor.measSensor2(); // Sensor 2
                canController.configurePayload();
                canController.addDataToPayload(resistance_1, 0x0000); // Sensor 1 output value + sensor ID
                canController.addDataToPayload(resistance_2, 0x0001); // Sensor 2 output value + sensor ID
                canController.sendPayload(); // Send sensor data 1 and 2.

                resistance_1 = tempSensor.measSensor3(); // Sensor 3
                resistance_2 = tempSensor.measSensor4(); // Sensor 4
                canController.configurePayload();
                canController.addDataToPayload(resistance_1, 0x0002); // Sensor 3 output value + sensor ID
                canController.addDataToPayload(resistance_2, 0x0003); // Sensor 4 output value + sensor ID
                canController.sendPayload(); // Send sensor data 3 and 4.

                resistance_1 = tempSensor.measSensor5(); // Sensor 5
                resistance_2 = tempSensor.measSensor6(); // Sensor 6
                canController.configurePayload();
                canController.addDataToPayload(resistance_1, 0x0004); // Sensor 5 output value + sensor ID
                canController.addDataToPayload(resistance_2, 0x0005); // Sensor 6 output value + sensor ID
                canController.sendPayload(); // Send sensor data 5 and 6.

                resistance_1 = tempSensor.measSensor7(); // Sensor 7
                resistance_2 = tempSensor.measSensor8(); // Sensor 8
                canController.configurePayload();
                canController.addDataToPayload(resistance_1, 0x0006); // Sensor 7 output value + sensor ID
                canController.addDataToPayload(resistance_2, 0x0007); // Sensor 8 output value + sensor ID
                canController.sendPayload(); // Send sensor data 7 and 8.

                resistance_1 = tempSensor.measSensor9(); // Sensor 9
                resistance_2 = tempSensor.measSensor10(); // Sensor 10
                canController.configurePayload();
                canController.addDataToPayload(resistance_1, 0x0008); // Sensor 9 output value + sensor ID
                canController.addDataToPayload(resistance_2, 0x0009); // Sensor 10 output value + sensor ID
                canController.sendPayload(); // Send sensor data 9 and 10.

                resistance_1 = tempSensor.measSensor11(); // Sensor 11
                resistance_2 = tempSensor.measSensor12(); // Sensor 12
                canController.configurePayload();
                canController.addDataToPayload(resistance_1, 0x000A); // Sensor 11 output value + sensor ID
                canController.addDataToPayload(resistance_2, 0x000B); // Sensor 12 output value + sensor ID
                canController.sendPayload(); // Send sensor data 11 and 12.

                resistance_1 = tempSensor.measSensor13(); // Sensor 13
                resistance_2 = tempSensor.measSensor14(); // Sensor 14
                canController.configurePayload();
                canController.addDataToPayload(resistance_1, 0x000C); // Sensor 13 output value + sensor ID
                canController.addDataToPayload(resistance_2, 0x000D); // Sensor 14 output value + sensor ID
                canController.sendPayload(); // Send sensor data 13 and 14.

                resistance_1 = tempSensor.measSensor15(); // Sensor 15
                resistance_2 = tempSensor.measSensor16(); // Sensor 16
                canController.configurePayload();
                canController.addDataToPayload(resistance_1, 0x000E); // Sensor 15 output value + sensor ID
                canController.addDataToPayload(resistance_2, 0x000F); // Sensor 16 output value + sensor ID
                canController.sendPayload(); // Send sensor data 15 and 16.
                break;
            }
            case Event::NONE: // Do nothing (only check messages)
            {
                // Empty
                break;
            }
            default: // Do nothing (only check messages)
            {
                // Empty
                break;
            }
        }
        // Read a message to see if event loop needs to change mode
        uint32_t message = canController.readData();
        if(message != 0xFFFFFFFF) // If a message is available
        {
            event = static_cast<Event>(message); // 0x01 (NORMAL) or 0x02 (NONE)
        }
    }
    return 0; // End of main
}

