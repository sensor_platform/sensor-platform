/* types.cpp  */
/* Nick van den Berg - Hogeschool Rotterdam - IHC Systems B.V.
 * Made for MSP430G2452 microcontroller                        */

/* This is purely for users, libraries should work
 *  without types.hpp, be aware: instances of PIN::Port
 *  will have to be declared manually without types.hpp/cpp */

#include "types.hpp"

// Empty
