/* usi.cpp */
/* Nick van den Berg - IHC Systems B.V.
 * Made for MSP430G2452 microcontroller
 * Used documents:
 * msp430x2xx family user guide p??-p??
 * msp430g2452 datasheet p??-p??
 * */

#include "usi.hpp"

namespace USI
{
/* USI Constructor methods */
    USI::Usi::Usi(const uint8_t& ppSclk, const uint8_t& pSclk,
                  const uint8_t& ppSdo, const uint8_t& pSdo,
                  const uint8_t& ppSdi, const uint8_t& pSdi):
                          clockPin(PIN::Pin(ppSclk, pSclk)),
                          dataOutPin(PIN::Pin(ppSdo, pSdo)),
                          dataInPin(PIN::Pin(ppSdi, pSdi))
    {
        this->clockPin.setSel(PIN::HIGH);
        this->clockPin.setSel2(PIN::LOW);

        this->dataOutPin.setSel(PIN::HIGH);
        this->dataOutPin.setSel2(PIN::LOW);

        this->dataInPin.setSel(PIN::HIGH);
        this->dataInPin.setSel2(PIN::LOW);
    }

    USI::Usi::Usi(const uint8_t& ppScl, const uint8_t& pScl,
                  const uint8_t& ppSda, const uint8_t& pSda):
                          clockPin(PIN::Pin(ppScl, pScl)),
                          dataOutPin(PIN::Pin(ppSda, pSda)),
                          dataInPin(this->dataOutPin)
    {
        this->clockPin.setSel(PIN::HIGH);
        this->clockPin.setSel2(PIN::LOW);

        this->dataOutPin.setSel(PIN::HIGH);
        this->dataOutPin.setSel2(PIN::LOW);
    }


/* USI control register 0 methods*/
    /* USI SDI/SDA port enable. Input in SPI mode, input or open drain output in I2C mode.
     * 0b0xxxxxxx USI function disabled
     * 0b1xxxxxxx USI function enabled                                                                  */
    void USI::Usi::setSdiSda(const uint8_t& mode) const
    {
        switch(mode)
        {
        case USI::DISABLED:
            *this->usi.control0 &= ~USIPE7;
            break;
        case USI::ENABLED:
            *this->usi.control0 |= USIPE7;
            break;
        }
    }

    /* USI SDO/SCL port enable. Output in SPI mode, input or open drain output in I2C mode.
     * 0bx0xxxxxx USI function disabled
     * 0bx1xxxxxx USI function enabled                                                               */
    void USI::Usi::setSdoScl(const uint8_t& mode) const
    {
        switch(mode)
        {
        case USI::DISABLED:
            *this->usi.control0 &= ~USIPE6;
            break;
        case USI::ENABLED:
            *this->usi.control0 |= USIPE6;
            break;
        }
    }

    /* USI SCLK port enable. Input in SPI slave mode, or I2C mode, output in SPI master mode.
     * 0bxx0xxxxx USI function disabled
     * 0bxx1xxxxx USI function enabled                                                                   */
    void USI::Usi::setSclk(const uint8_t& mode) const
    {
        switch(mode)
        {
        case USI::DISABLED:
            *this->usi.control0 &= ~USIPE5;
            break;
        case USI::ENABLED:
            *this->usi.control0 |= USIPE5;
            break;
        }
    }

    /* LSB first select. This bit controls the direction of the receive and transmit shift register.
     * 0bxxx0xxxx MSB first
     * 0bxxx1xxxx LSB first                                                                                      */
    void USI::Usi::setFirstSendBit(const uint8_t& mode) const
    {
        switch(mode)
        {
        case USI::MSB:
            *this->usi.control0 &= ~USILSB;
            break;
        case USI::LSB:
            *this->usi.control0 |= USILSB;
            break;
        }
    }

    /* Bit 3 Master select
     * 0bxxxx0xxx Slave mode
     * 0bxxxx1xxx Master mode        */
    void USI::Usi::setMasterSlave(const uint8_t& mode) const
    {
        switch(mode)
        {
        case USI::SLAVE:
            *this->usi.control0 &= ~USIMST;
            break;
        case USI::MASTER:
            *this->usi.control0 |= USIMST;
            break;
        }
    }

    /* Output latch control
     * 0bxxxxx0xx Output latch enable depends on shift clock
     * 0bxxxxx1xx Output latch always enabled and transparent    */
    void USI::Usi::setOutputLatch(const uint8_t& mode) const
    {
        switch(mode)
        {
        case USI::LATCH_MODE_0:
            *this->usi.control0 &= ~USIGE;
            break;
        case USI::LATCH_MODE_1:
            *this->usi.control0 |= USIGE;
            break;
        }
    }

    /* Data output enable
     * 0bxxxxxx0x Output disabled
     * 0bxxxxxx1x Output enabled   */
    void USI::Usi::setDataOutput(const uint8_t& mode) const
    {
        switch(mode)
        {
        case DISABLED:
            *this->usi.control0 &= ~USIOE;
            break;
        case USI::ENABLED:
            *this->usi.control0 |= USIOE;
            break;
        }
    }

    /* USI software reset
     * 0bxxxxxxx0 USI released for operation.
     * 0bxxxxxxx1 USI logic held in reset state.  */
    void USI::Usi::setResetState(const uint8_t& mode) const
    {
        switch(mode)
        {
        case DISABLED:
            *this->usi.control0 &= ~USISWRST;
            break;
        case USI::ENABLED:
            *this->usi.control0 |= USISWRST;
            break;
        }
    }

/* USI control register 1 methods */
    /* Clock phase select
     * 0b0xxxxxxx Data is changed on the first SCLK edge and captured on the following edge.
     * 0b1xxxxxxx Data is captured on the first SCLK edge and changed on the following edge. */
    void USI::Usi::setClockPhase(const uint8_t& mode) const
    {
        switch(mode)
        {
        case USI::PHASE_MODE_0:
            *this->usi.control1 &= ~USICKPH;
            break;
        case USI::PHASE_MODE_1:
            *this->usi.control1 |= USICKPH;
            break;
        }
    }

    /* I2C mode enable
     * 0bx0xxxxxx I2C mode disabled
     * 0bx1xxxxxx I2C mode enabled   */
    void USI::Usi::setI2cMode(const uint8_t& mode) const
    {
        switch(mode)
        {
        case USI::DISABLED:
            *this->usi.control1 &= ~USII2C;
            break;
        case USI::ENABLED:
            *this->usi.control1 |= USII2C;
            break;
        }
    }

    /* START condition interrupt-enable
     * 0bxx0xxxxx Interrupt on START condition disabled
     * 0bxx1xxxxx Interrupt on START condition enabled  */
    void USI::Usi::setStartInterrupt(const uint8_t& mode) const
    {
        switch(mode)
        {
        case USI::DISABLED:
            *this->usi.control1 &= ~USISTTIE;
            break;
        case USI::ENABLED:
            *this->usi.control1 |= USISTTIE;
            break;
        }
    }

    /* USI counter interrupt enable
     * 0bxxx0xxxx Interrupt disabled
     * 0bxxx1xxxx Interrupt enabled          */
    void USI::Usi::setCounterInterrupt(const uint8_t& mode) const
    {
        switch(mode)
        {
        case USI::DISABLED:
            *this->usi.control1 &= ~USIIE;
            break;
        case USI::ENABLED:
           *this->usi.control1 |= USIIE;
            break;
        }
    }

    /* Arbitration lost
     * 0bxxxx0xxx No arbitration lost condition
     * 0bxxxx1xxx Arbitration lost               */
    void USI::Usi::setArbitrationLostCondition(const uint8_t& mode) const
    {
        switch(mode)
        {
        case USI::DISABLED:
            *this->usi.control1 &= ~USIAL;
            break;
        case USI::ENABLED:
            *this->usi.control1 |= USIAL;
            break;
        }
    }

    /* STOP condition received. USISTP is automatically cleared if USICNTx is loaded with a value > 0 when
     * USIIFGCC = 0.
     * 0bxxxxx0xx No STOP condition received
     * 0bxxxxx1xx STOP condition received                                                                            */
    uint8_t USI::Usi::getStopCondition() const
    {
        return (*this->usi.control1 & (USISTP));
    }

    /* START condition interrupt flag
     * 0bxxxxxx0x No START condition received. No interrupt pending.
     * 0bxxxxxx1x START condition received. Interrupt pending        */
    uint8_t USI::Usi::getStartCondition() const
    {
        return (*this->usi.control1 & (USISTTIFG));
    }

    /* USI counter interrupt flag. Set when the USICNTx = 0. Automatically cleared if USICNTx is loaded with a
     * value > 0 when USIIFGCC = 0.
     * 0bxxxxxxx0 No interrupt pending
     * 0bxxxxxxx1 Interrupt pending                                                                                      */
    uint8_t USI::Usi::getCounterInterruptFlag() const
    {
        return (*this->usi.control1 & (USIIFG));
    }

/* USI clock control register methods */
    /* Set USI clock divider
     * 0b000xxxxx divide source by 1
     * 0b001xxxxx divide source by 2
     * 0b010xxxxx divide source by 4
     * 0b011xxxxx divide source by 8
     * 0b100xxxxx divide source by 16
     * 0b101xxxxx divide source by 32
     * 0b110xxxxx divide source by 64
     * 0b111xxxxx divide source by 128 */
    void USI::Usi::setClockDivider(const uint8_t& mode) const
    {
        *this->usi.controlClock &= ~USIDIV_7; // Reset clock divider
        switch(mode)
        {
        case USI::DIV_1:
            *this->usi.controlClock |= USIDIV_0;
            break;
        case USI::DIV_2:
            *this->usi.controlClock |= USIDIV_1;
            break;
        case USI::DIV_4:
            *this->usi.controlClock |= USIDIV_2;
            break;
        case USI::DIV_8:
            *this->usi.controlClock |= USIDIV_3;
            break;
        case USI::DIV_16:
            *this->usi.controlClock |= USIDIV_4;
            break;
        case USI::DIV_32:
            *this->usi.controlClock |= USIDIV_5;
            break;
        case USI::DIV_64:
            *this->usi.controlClock |= USIDIV_6;
            break;
        case USI::DIV_128:
            *this->usi.controlClock |= USIDIV_7;
            break;
        }
    }

    /* Set USI clock source
     * 0bxxx001xx ACLK
     * 0bxxx010xx SMCLK
     * 0bxxx100xx Software          */
    void USI::Usi::setClockSource(const uint8_t& mode) const
    {
        *this->usi.controlClock &= ~USISSEL_7; // reset clock source
        switch(mode)
        {
        case USI::ACLK:
            *this->usi.controlClock |= USISSEL_1;
            break;
        case USI::SMCLK:
            *this->usi.controlClock |= USISSEL_2;
            break;
        case USI::SOFTWARECLK:
            *this->usi.controlClock |= USISSEL_4;
            break;
        }
    }

    /* Set USI clock polarity
     * 0bxxxxxx0x inactive state low
     * 0bxxxxxx1x inactive state high        */
    void USI::Usi::setClockPolarity(const uint8_t& mode) const
    {
        switch(mode)
        {
        case USI::INACTIVE_LOW:
            *this->usi.controlClock &= ~USICKPL;
            break;
        case USI::INACTIVE_HIGH:
            *this->usi.controlClock |= USICKPL;
            break;
        }
    }

    /* Set USI software clock bit
     * 0bxxxxxxx0 USI input clock is low
     * 0bxxxxxxx1 USI input clock is high        */
    void USI::Usi::setSoftwareClockBit(const uint8_t& mode) const
    {
        switch(mode)
        {
        case USI::LOW:
            *this->usi.controlClock &= ~USISWCLK;
            break;
        case USI::HIGH:
            *this->usi.controlClock |= USISWCLK;
            break;
        }
    }

/* USI bit counter register */
    /* SCL release. The SCL line is released from low to idle. USISCLREL is cleared if a START condition is
     * detected.
     * 0 SCL line is held low if USIIFG is set
     * 1 SCL line is released                                                                                */
    void USI::Usi::setSclReleaseMode(const uint8_t& mode) const
    {
        switch(mode)
        {
        case USI::HALT:
            *this->usi.bitCounter &= ~USISCLREL;
            break;
        case USI::RELEASE:
            *this->usi.bitCounter |= USISCLREL;
            break;
        }
    }
    /* 16-bit shift register enable
     * 0 8-bit shift register mode. Low byte register USISRL is used.
     * 1 16-bit shift register mode. Both high and low byte registers USISRL and USISRH are used.
     * USISR addresses all 16 bits simultaneously.                                                  */
    void USI::Usi::setBitMode(const uint8_t& mode) const
    {
        switch(mode)
        {
        case USI::BIT_MODE_8:
            *this->usi.bitCounter &= ~USI16B;
            break;
        case USI::BIT_MODE_16:
            *this->usi.bitCounter |= USI16B;
            break;
        }
    }

    /* USI interrupt flag clear control. When USIIFGCC = 1 the USIIFG will not be cleared automatically when
     * USICNTx is written with a value > 0.
     * 0 USIIFG automatically cleared on USICNTx update
     * 1 USIIFG is not cleared automatically                                                                    */
    void USI::Usi::setInterruptFlagClearMode(const uint8_t& mode) const
    {
        switch(mode)
        {
        case USI::DISABLED:
            *this->usi.bitCounter &= ~USIIFGCC;
            break;
        case USI::ENABLED:
            *this->usi.bitCounter |= USIIFGCC;
            break;
        }
    }

    /* USI bit count. The USICNTx bits set the number of bits to be received or transmitted.
     * 0b0000 0 bits
     * 0b0001 1 bit
     * 0b0010 2 bits
     * 0b0100 4 bits
     * 0b1000 8 bits
     * 0b1111 16 bits (max value)                                                               */
    void USI::Usi::setBitCounter(uint8_t countAmount) const
    {
        if(countAmount >= 0x1F) // Max value
        {
            //USICNT |= 0b1111;
            *this->usi.bitCounter |= 0b1111;
        }
        else                    // User value
        {
            //USICNT |= countAmount;
            *this->usi.bitCounter |= countAmount;
        }
    }

    /* Returns bitCounter value, can be used to check if transmit successful
     * (first 5 bits of UCICNT register) */
    uint8_t USI::Usi::getBitCounter() const
    {
        volatile uint8_t test =  ((*this->usi.bitCounter) & (0x1F));
        return test;
    }

/* USI byte shift register methods */
    /* BELOW NEEDS WORK! */

    void USI::Usi::setLowByte(uint8_t byte) const
    {
        //USISRL = byte;
        *this->usi.lowByte = byte;
    }

    void USI::Usi::setHighByte(uint8_t byte) const
    {
        *this->usi.highByte = byte;
    }

    void USI::Usi::setHighAndLowByte(uint16_t bytes) const
    {
        *this->usi.shiftRegister = bytes;
    }

    void USI::Usi::sendByte(uint8_t byte) const
    {
        *this->usi.lowByte = byte;
        this->setBitCounter(8);
    }

    uint8_t USI::Usi::getLowByte() const
    {
        return *this->usi.lowByte;
    }

    uint8_t USI::Usi::getHighByte() const
    {
        return *this->usi.highByte;
    }

    void USI::Usi::sendTwoBytes(uint16_t bytes) const
    {
        this->setHighAndLowByte(bytes);
        this->setBitCounter(16);
    }
}
