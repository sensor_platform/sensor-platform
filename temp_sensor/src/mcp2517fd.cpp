#include "mcp2517fd.hpp"

namespace MCP2517FD
{
    /* Variable redefinition to force compiler to create variables during compile-time */
    /* Variables */
    /* Commands */
    constexpr std::uint8_t MCP2517FD::Mcp2517fd::commandReset;
    constexpr std::uint8_t MCP2517FD::Mcp2517fd::commandRead;
    constexpr std::uint8_t MCP2517FD::Mcp2517fd::commandWrite;
    constexpr std::uint8_t MCP2517FD::Mcp2517fd::commandNoOperation;
    /* addresses */
    constexpr std::uint16_t MCP2517FD::Mcp2517fd::addressReset;
    constexpr std::uint16_t MCP2517FD::Mcp2517fd::addressClock;
    constexpr std::uint16_t MCP2517FD::Mcp2517fd::addressInputOutput;
    constexpr std::uint16_t MCP2517FD::Mcp2517fd::addressCrc;
    constexpr std::uint16_t MCP2517FD::Mcp2517fd::addressCanControl;
    constexpr std::uint16_t MCP2517FD::Mcp2517fd::addressNominalBitTime;
    constexpr std::uint16_t MCP2517FD::Mcp2517fd::addressDataBitTime;
    constexpr std::uint16_t MCP2517FD::Mcp2517fd::addressTransmitterDelayCompensation;
    constexpr std::uint16_t MCP2517FD::Mcp2517fd::addressTxFifoControl;
    constexpr std::uint16_t MCP2517FD::Mcp2517fd::addressTxFifoStatus;
    constexpr std::uint16_t MCP2517FD::Mcp2517fd::addressTxFifoUserAddress;
    constexpr std::uint16_t MCP2517FD::Mcp2517fd::addressRxFifoControl;
    constexpr std::uint16_t MCP2517FD::Mcp2517fd::addressRxFifoStatus;
    constexpr std::uint16_t MCP2517FD::Mcp2517fd::addressRxFifoUserAddress;
    constexpr std::uint16_t MCP2517FD::Mcp2517fd::addressRxFilterControl;
    constexpr std::uint16_t MCP2517FD::Mcp2517fd::addressRxFilterObject;
    constexpr std::uint16_t MCP2517FD::Mcp2517fd::addressRxFilterMask;
    /* settings */
    constexpr std::uint32_t MCP2517FD::Mcp2517fd::settingsClock;
    constexpr std::uint32_t MCP2517FD::Mcp2517fd::settingsInputOutput;
    constexpr std::uint32_t MCP2517FD::Mcp2517fd::settingsCrc;
    constexpr std::uint32_t MCP2517FD::Mcp2517fd::settingsCanControl;
    constexpr std::uint32_t MCP2517FD::Mcp2517fd::settingsNominalBitTime;
    constexpr std::uint32_t MCP2517FD::Mcp2517fd::settingsDataBitTime;
    constexpr std::uint32_t MCP2517FD::Mcp2517fd::settingsTransmitterDelayCompensation;
    constexpr std::uint32_t MCP2517FD::Mcp2517fd::settingsTransmitEventFifoControl;
    constexpr std::uint32_t MCP2517FD::Mcp2517fd::settingsTxFifoControl;
    constexpr std::uint32_t MCP2517FD::Mcp2517fd::settingsRxFifoControl;
    constexpr std::uint32_t MCP2517FD::Mcp2517fd::settingsRxFilterControl;
    constexpr std::uint32_t MCP2517FD::Mcp2517fd::settingsRxFilterObject;
    constexpr std::uint32_t MCP2517FD::Mcp2517fd::settingsRxFilterMask;
    constexpr std::uint32_t MCP2517FD::Mcp2517fd::settingsCanConfigurationMode;
    constexpr std::uint32_t MCP2517FD::Mcp2517fd::settingsCanDebugModeExt;
    constexpr std::uint32_t MCP2517FD::Mcp2517fd::settingsCanDebugModeInt;
    constexpr std::uint32_t MCP2517FD::Mcp2517fd::requestSend;
    constexpr std::uint32_t MCP2517FD::Mcp2517fd::incrementTail;
    std::uint32_t MCP2517FD::Mcp2517fd::messageObjectAddr;
    /* User-Vars */
    constexpr std::uint32_t MCP2517FD::Mcp2517fd::settleTime;
    constexpr std::uint32_t MCP2517FD::Mcp2517fd::resetTime;
    constexpr std::uint32_t MCP2517FD::Mcp2517fd::csSettleTime;
    constexpr std::uint32_t MCP2517FD::Mcp2517fd::settingsCanLegacyMode;
    constexpr std::uint8_t  MCP2517FD::Mcp2517fd::SID;
    constexpr std::uint8_t  MCP2517FD::Mcp2517fd::SID11;
    constexpr std::uint32_t MCP2517FD::Mcp2517fd::EID;
    constexpr std::uint16_t MCP2517FD::Mcp2517fd::DLC;
    constexpr std::uint8_t  MCP2517FD::Mcp2517fd::IDE;
    constexpr std::uint8_t  MCP2517FD::Mcp2517fd::RTR;
    constexpr std::uint8_t  MCP2517FD::Mcp2517fd::FDF;
    constexpr std::uint8_t  MCP2517FD::Mcp2517fd::BRS;
    constexpr std::uint8_t  MCP2517FD::Mcp2517fd::ESI;
    constexpr std::uint32_t MCP2517FD::Mcp2517fd::SEQ;
    /* ID and message settings for transmission */
    constexpr std::uint32_t MCP2517FD::Mcp2517fd::t0;
    constexpr std::uint32_t MCP2517FD::Mcp2517fd::t1;

    MCP2517FD::Mcp2517fd::Mcp2517fd(const uint8_t& ppSclk, const uint8_t& pSclk,
                                    const uint8_t& ppSdo, const uint8_t& pSdo,
                                    const uint8_t& ppSdi, const uint8_t& pSdi,
                                    const GPIO::Gpio& csPin, const uint8_t& activeState,
                                    const SPI::SpiSettings& spiSettings):
                                         SPI::SpiDevice(ppSclk, pSclk,
                                                        ppSdo, pSdo,
                                                        ppSdi, pSdi,
                                                        csPin, activeState,
                                                        spiSettings, this->commandNoOperation)
    {
        /* Init vars */
        this->messageObjectAddr = 0;
        /* Initialization + reset for good measure (start-up glitches) */
        this->resetDevice();
    }

    void MCP2517FD::Mcp2517fd::sendData(int16_t data, uint8_t sensorNo) const
    {
        // check if room in FIFO
        this->select();
        volatile uint32_t helper = this->readRegister(this->addressTxFifoStatus);
        this->deselect();
        if(helper & 0x00000001) // FIFO not full
        {
            // Assemble message
            uint32_t message =  static_cast<uint32_t>(0x00000000 | ((data & 0xFFFF)<<0) | ((static_cast<uint32_t>(sensorNo) & 0x1F)<<16));
            // Check where data is supposed to be written
            this->select();
            helper = this->readRegister(this->addressTxFifoUserAddress); // Get Address of next message object
            this->deselect();
            // Send data (3x 4 bytes (1 byte padding on data))
            this->select();
            this->writeRegister(0x400+helper, this->t0); // First word (ID (0x00000002))
            this->deselect();
            this->select();
            this->writeRegister(0x400+helper+4, this->t1); // Second word (message settings (0x0000010A))
            this->deselect();
            this->select();
            this->writeRegister(0x400+helper+8, message); // Third word (message)
            this->deselect();
            // Request transmission send
            this->select();
            this->writeRegister(this->addressTxFifoControl, this->requestSend); // increase message-head and request transmission (0x03000380)
            this->deselect();
        }
    }

    void MCP2517FD::Mcp2517fd::configurePayload()
    {
        // check if room in FIFO
        this->select();
        volatile uint32_t helper = this->readRegister(this->addressTxFifoStatus);
        this->deselect();
        if(helper & 0x00000001) // FIFO not full
        {
            // Check where data is supposed to be written
            this->select();
            this->messageObjectAddr = this->readRegister(this->addressTxFifoUserAddress); // Get Address of next message object
            this->deselect();
            // Send ID
            this->select();
            this->writeRegister(0x400+this->messageObjectAddr, this->t0); // First word (ID (0x00000002))
            this->deselect();
            // Increase messageObjectAddr for next word
            this->messageObjectAddr += 4;
            // Send settings
            this->select();
            this->writeRegister(0x400+this->messageObjectAddr, this->t1); // Second word (message settings (0x00000110))
            this->deselect();
        }
    }

    void MCP2517FD::Mcp2517fd::addDataToPayload(int16_t data1, int16_t data2) const
    {
        // check if room in FIFO
        this->select();
        volatile uint32_t helper = this->readRegister(this->addressTxFifoStatus);
        this->deselect();
        if(helper & 0x00000001) // FIFO not full
        {
            // Assemble message
            uint32_t message =  static_cast<uint32_t>(0x00000000 | ((static_cast<uint32_t>(data1) & 0x0000FFFF)<<16) | ((static_cast<uint16_t>(data2) & 0xFFFF)<<0));
            // Increase messageObjectAddr for next word
            this->messageObjectAddr += 4;
            // Send data 4 bytes (word size)
            this->select();
            this->writeRegister(0x400+this->messageObjectAddr, message); // Third word (message)
            this->deselect();
        }
    }

    void MCP2517FD::Mcp2517fd::sendPayload() const
    {
        // Request transmission send
        this->select();
        this->writeRegister(this->addressTxFifoControl, this->requestSend); // increase message-head and request transmission (0x03000380)
        this->deselect();
    }
    std::uint32_t MCP2517FD::Mcp2517fd::readData() const
    {
        // check if FIFO is not empty
        this->select();
        volatile uint32_t helper = this->readRegister(this->addressRxFifoStatus);
        this->deselect();
        if(helper & 0x00000001) // FIFO not empty
        {
            // Declare message
            uint32_t message =  0x00000000;
            // Check where data is supposed to be read from
            this->select();
            helper = this->readRegister(this->addressRxFifoUserAddress); // Get Address of next message object
            this->deselect();
// This can be uncommented if there is an interest in sender ID/settings, for instance on the communication platform
//            // Read data (3x 4 bytes)
//            this->select();
//            message = this->readRegister(0x400+helper); // First word (ID) (Ignored, all messages filtered except ID 1)
//            this->deselect();
//            this->select();
//            message = this->readRegister(0x400+helper+4); // Second word (message settings) (Ignored, DNC)
//            this->deselect();
            this->select();
            message = this->readRegister(0x400+helper+8); // Third word (message) (Interesting content)
            this->deselect();
            // Increment message tail
            this->select();
            this->writeRegister(this->addressRxFifoControl, this->incrementTail); // increase message-head and request transmission (0x03000100)
            this->deselect();
            return message; // Return only data content
        }
        else
        {
            return 0xFFFFFFFF;
        }
    }

    void MCP2517FD::Mcp2517fd::wake() const
    {
        this->resetSpi();
        //this->sendTwoBytes(this->commandWake);
    }

    void MCP2517FD::Mcp2517fd::sleep() const
    {
        this->resetSpi();
        //this->sendTwoBytes(this->commandSleep);
    }

    void MCP2517FD::Mcp2517fd::resetSpi() const
    {
        this->deselect();
        this->select();
        __delay_cycles(100000);
    }

    void MCP2517FD::Mcp2517fd::resetDevice() const
    {
        /* Select device (CS low) */
        this->select();
        /* Send reset command */
        this->sendByte((this->commandRead<<4) | (addressReset>>8));
        this->sendByte(addressReset & 0x00ff);
        /* Command is not executed till deselect (CS high) */
        this->deselect();

        /* Let device settle and configure device for use */
        __delay_cycles(this->resetTime);
        this->init();
    }

    void MCP2517FD::Mcp2517fd::init() const
    {
        // Enter configuration mode
        this->select();
        this->writeRegister(this->addressCanControl, this->settingsCanConfigurationMode);
        this->deselect();
        // Configure oscillator and CLKO pin
        this->select();
        this->writeRegister(this->addressClock, this->settingsClock);
        this->deselect();
        // Configure I/O pins
        this->select();
        this->writeRegister(this->addressInputOutput, this->settingsInputOutput);
        this->deselect();
        // Configure CAN Control registers
        this->select();
        this->writeRegister(this->addressCrc, this->settingsCrc);
        this->deselect();
        // Configure Bit Time registers
        this->select();
        this->writeRegister(this->addressNominalBitTime, this->settingsNominalBitTime);
        this->deselect();
        this->select();
        this->writeRegister(this->addressDataBitTime, this->settingsDataBitTime);
        this->deselect();
        // Configure TEF
        this->select();
        this->writeRegister(this->addressTransmitterDelayCompensation, this->settingsTransmitterDelayCompensation);
        this->deselect();

        // Configure Rx filter
        this->select();
        this->writeRegister(this->addressRxFilterControl, 0x00000000); // Disable filter
        this->deselect();
        this->select();
        this->writeRegister(this->addressRxFilterObject, this->settingsRxFilterObject);
        this->deselect();
        this->select();
        this->writeRegister(this->addressRxFilterMask, this->settingsRxFilterMask);
        this->deselect();
        this->select();
        this->writeRegister(this->addressRxFilterControl, this->settingsRxFilterControl); // Enable filter 0
        this->deselect();

        // Configure Rx and Tx FIFO
        this->select();
        this->writeRegister(this->addressTxFifoControl, this->settingsTxFifoControl);
        this->deselect();

        this->select();
        this->writeRegister(this->addressRxFifoControl, this->settingsRxFifoControl);
        this->deselect();

//        /* Enable normal mode operation (in can-fd mode) */
//        this->select();
//        this->writeRegister(this->addressCanControl, this->settingsCanControl);
//        this->deselect();
        /* Enable normal mode operation (in legacy mode) */
        this->select();
        this->writeRegister(this->addressCanControl, this->settingsCanLegacyMode);
        this->deselect();
    }

    void MCP2517FD::Mcp2517fd::testRegisters() const
    {
        /* Test the registers of the ADS1148. Use debugger breakpoints to read them. */
        volatile uint32_t registerTest = 0x00000000;

        this->select();
        registerTest = this->readRegister(this->addressClock);
        this->deselect();

        this->select();
        registerTest = this->readRegister(this->addressInputOutput);
        this->deselect();

        this->select();
        registerTest = this->readRegister(this->addressCrc);
        this->deselect();

        this->select();
        registerTest = this->readRegister(this->addressCanControl);
        this->deselect();

        this->select();
        registerTest = this->readRegister(this->addressNominalBitTime);
        this->deselect();

        this->select();
        registerTest = this->readRegister(this->addressDataBitTime);
        this->deselect();

        this->select();
        registerTest = this->readRegister(this->addressTransmitterDelayCompensation);
        this->deselect();

        this->select();
        registerTest = this->readRegister(this->addressTxFifoControl);
        this->deselect();

        this->select();
        registerTest = this->readRegister(this->addressTxFifoStatus);
        this->deselect();

        this->select();
        registerTest = this->readRegister(this->addressTxFifoUserAddress);
        this->deselect();

        this->select();
        registerTest = this->readRegister(this->addressRxFifoControl);
        this->deselect();

        this->select();
        registerTest = this->readRegister(this->addressRxFifoStatus);
        this->deselect();

        this->select();
        registerTest = this->readRegister(this->addressRxFifoUserAddress);
        this->deselect();

        this->select();
        registerTest = this->readRegister(this->addressRxFilterControl);
        this->deselect();

        this->select();
        registerTest = this->readRegister(this->addressRxFilterObject);
        this->deselect();


        this->select();
        registerTest = this->readRegister(this->addressRxFilterMask);
        this->deselect();

        volatile const uint8_t breakPointHelper = 0x00;
    }

    void MCP2517FD::Mcp2517fd::sendTestMessage() const
    {
        // check if room in FIFO
        this->select();
        volatile uint32_t helper = this->readRegister(this->addressTxFifoStatus);
        if(helper & 0x00000001)
        {

            this->select();
            helper = this->readRegister(this->addressTxFifoControl);
            this->deselect();
            // Test message:
            // Get Address of next message object
            this->select();
            helper = this->readRegister(this->addressTxFifoUserAddress);
            this->deselect();

            this->select();
            this->writeRegister(0x400+helper, 0x00000002);
            this->deselect();
            this->select();
            this->writeRegister(0x400+helper+4, 0x00000104);
            this->deselect();
            this->select();
            this->writeRegister(0x400+helper+8, 0xFF00FF00);
            this->deselect();
            this->select();
            this->writeRegister(this->addressTxFifoControl, 0x03000380);
            this->deselect();

            volatile const uint8_t breakPointHelper = 0x00;
        }
    }

    std::uint32_t MCP2517FD::Mcp2517fd::readRegister(const std::uint16_t& address) const
    {
        /* Send the Read command and select address to read (4 bits command + 12 bits address in 2 steps) */
        this->sendByte((this->commandRead<<4) | (address>>8)); // 0bccccaaaa ( c = command and a = address )
        /* Send the rest of address */
        this->sendByte(address & 0x00ff);
        /* Send NOP (no operation 0x00) so device clocks out data */
        uint32_t regValue = 0x00000000;
        regValue = (this->receiveByte());
        regValue = (regValue | (static_cast<uint32_t>(this->receiveByte())<<8));
        regValue = (regValue | (static_cast<uint32_t>(this->receiveByte())<<16));
        regValue = (regValue | (static_cast<uint32_t>(this->receiveByte())<<24));
        return regValue;
    }

    void MCP2517FD::Mcp2517fd::writeRegister(const std::uint16_t& address, const std::uint32_t& data) const
    {
        /* Send the Write command and select address to write (4 bits command + 12 bits address in 2 steps) */
        this->sendByte((this->commandWrite<<4) | (address>>8)); // 0bccccaaaa ( c = command and a = address )
        /* Send the rest of address */
        this->sendByte(address & 0x00ff);
        /* Write to the 32 bit register, 8 bits at a time. Start at Bit 0 */
        uint8_t byte = static_cast<uint8_t>((data & 0x000000FF)>>0);
        this->sendByte(byte);
        byte = static_cast<uint8_t>((data & 0x0000FF00)>>8);
        this->sendByte(byte);
        byte = static_cast<uint8_t>((data & 0x00FF0000)>>16);
        this->sendByte(byte);
        byte = static_cast<uint8_t>((data & 0xFF000000)>>24);
        this->sendByte(byte);
    }

    void MCP2517FD::Mcp2517fd::select() const
    {
        /* MCP2517FD requires different clock polarity as ADS1148 */
        SPI::SpiDevice::setClockPhase();
        /* Chip-select must be high */
        SPI::SpiDevice::select();
    }

    void MCP2517FD::Mcp2517fd::deselect() const
    {
        SPI::SpiDevice::deselect();
    }

}
