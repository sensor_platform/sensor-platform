
#ifndef TEMPSENSOR_SRC_ADS1148_HPP_
#define TEMPSENSOR_SRC_ADS1148_HPP_

#include <cstdint>

#include "msp430.h"

#include "pin.hpp"
#include "gpio.hpp"
#include "spi.hpp"


namespace ADS1148
{
    /* Datatypes */

    /* Burn-out current in uA */
    enum burnOutCurrents : const std::uint8_t
    {
      BURNOUT_OFF = 0x0, BURNOUT_05  = 0x1, BURNOUT_2 = 0x2, BURNOUT_10 = 0x3
    };

    /* AIN0-AIN7 can be used for IDAC output as well */
    enum adcInputs : const std::uint8_t
    {
      AIN0 = 0x0, AIN1 = 0x1, AIN2 = 0x2, AIN3 = 0x3, AIN4 = 0x4, AIN5 = 0x5, AIN6 = 0x6, AIN7 = 0x7
    };

    enum internalReferences : const std::uint8_t
    {
      ALWAYS_OFF = 0x0, ALWAYS_ON = 0x1, FOLLOW_DEVICE_STATE = 0x2
    };

    enum externalReferences : const std::uint8_t
    {
        REF0 = 0x0, REF1 = 0x1, REFINT = 0x2, REF0INT = 0x3
    };

    enum gains : const std::uint8_t
    {
       GAIN_1 = 0x0, GAIN_2 = 0x1, GAIN_4 = 0x2, GAIN_8 = 0x3, GAIN_16 = 0x4, GAIN_32 = 0x5,
        GAIN_64 = 0x6, GAIN_128 = 0x7
    };

    enum sampleRates : const std::uint8_t
    {
      SPS_5 = 0x0, SPS_10 = 0x1, SPS_20 = 0x2, SPS_40 = 0x3, SPS_80 = 0x4, SPS_160 = 0x5,
      SPS_320 = 0x6, SPS_640 = 0x7, SPS_1000 = 0x8, SPS_2000 = 0x9
    };

    /* IDAC current in uA */
    enum idacCurrents : const std::uint8_t
    {
      IDAC_OFF = 0x0, IDAC_50 = 0x1, IDAC_100 = 0x2, IDAC_250 = 0x3, IDAC_500 = 0x4, IDAC_750 = 0x5,
      IDAC_1000 = 0x6, IDAC_1500 = 0x7
    };

    /* AIN0-AIN7 can be used for IDAC output as well as can these specific pins */
    enum idacPins : const std::uint8_t
    {
      IEXC1 = 0x8, IEXC2 = 0x9, IDAC_DISCONNECT = 0xF
    };

    enum gpios : const std::uint8_t
    {
        GPIO0 = 0x01, GPIO1 = 0x02, GPIO2 = 0x04, GPIO3 = 0x08, GPIO4 = 0x10, GPIO5 = 0x20, GPIO6 = 0x40, GPIO7 = 0x80
    };

    /* Classes */
    /* Device starts in sleep mode! To start conversation CS must be low AND startPin must be high.
     * (starts conversion) */
    class Ads1148: public SPI::SpiDevice
    {
    public:
        /* Constructors */
        Ads1148(const uint8_t& ppSclk, const std::uint8_t& pSclk,
                const uint8_t& ppSdo, const std::uint8_t& pSdo,
                const uint8_t& ppSdi, const std::uint8_t& pSdi,
                const GPIO::Gpio& chipSelectPin, const std::uint8_t& activeState,
                const GPIO::Gpio& startPin, const GPIO::Gpio& drdyPin,
                const SPI::SpiSettings& spiSettings);
        /* Sensors */
        std::int16_t measSensor1();  // (000 ABC), IDAC AIN0 & AIN3, measure AIN1(+) AIN2(-)
        std::int16_t measSensor2();  // (001 ABC), IDAC IEXC1 & IEXC2, measure AIN1(+) AIN2(-)
        std::int16_t measSensor3();  // (010 ABC), IDAC AIN0 & AIN3, measure AIN1(+) AIN2(-)
        std::int16_t measSensor4();  // (011 ABC), IDAC IEXC1 & IEXC2, measure AIN1(+) AIN2(-)
        std::int16_t measSensor5();  // (100 ABC), IDAC AIN0 & AIN3, measure AIN1(+) AIN2(-)
        std::int16_t measSensor6();  // (101 ABC), IDAC IEXC1 & IEXC2, measure AIN1(+) AIN2(-)
        std::int16_t measSensor7();  // (110 ABC), IDAC AIN0 & AIN3, measure AIN1(+) AIN2(-)
        std::int16_t measSensor8();  // (111 ABC), IDAC IEXC1 & IEXC2, measure AIN1(+) AIN2(-)
        std::int16_t measSensor9();  // (000 ABC), IDAC AIN0 & AIN3, measure AIN1(+) AIN2(-)
        std::int16_t measSensor10(); // (001 ABC), IDAC IEXC1 & IEXC2, measure AIN1(+) AIN2(-)
        std::int16_t measSensor11(); // (010 ABC), IDAC AIN0 & AIN3, measure AIN1(+) AIN2(-)
        std::int16_t measSensor12(); // (011 ABC), IDAC IEXC1 & IEXC2, measure AIN1(+) AIN2(-)
        std::int16_t measSensor13(); // (100 ABC), IDAC AIN0 & AIN3, measure AIN1(+) AIN2(-)
        std::int16_t measSensor14(); // (101 ABC), IDAC IEXC1 & IEXC2, measure AIN1(+) AIN2(-)
        std::int16_t measSensor15(); // (110 ABC), IDAC AIN0 & AIN3, measure AIN1(+) AIN2(-)
        std::int16_t measSensor16(); // (111 ABC), IDAC IEXC1 & IEXC2, measure AIN1(+) AIN2(-)
        /* Power Management */
        void wake() const;        // Device exists power down mode
        void sleep() const;       // Device enters power down mode
        /* SPI */
        void select() const;
        void deselect() const;
        void resetSpi() const;
        void resetDevice() const;
        /* Registers */
        void testRegisters() const;
        /* Burnout current sources (short or open circuit detection) */
        int16_t checkSensor() const;
    private:
        /* Methods */
        /* initialization */
        void init() const;
        /* ADS1148 internal multiplexer (ADS1148.pfd p48) */
        void setBurnOutCurrent(const std::uint8_t& current) const;
        void setPositiveInput(const std::uint8_t& pin) const;
        void setNegativeInput(const std::uint8_t& pin) const;
        void setIntChannel1() const; // IDACs on AIN0 & AIN3, measure AIN1(+) AIN2(-)
        void setIntChannel2() const; // IDACs on IEXC1 & IEXC2, measure AIN1(+) AIN2(-)
        void setIntChannelDisconnected() const; // IDACs disconnected
        /* Reference control (ADS1148.pfd p51) */
        void setIdacRef(const std::uint8_t& intRef) const;
        void setAdcRef(const std::uint8_t& adcRef) const;
        /* Sys control 0 (ADS1148.pfd p52) */
        void setGain(const std::uint8_t& gain) const;
        void setSampleRate(const std::uint8_t& rate) const;
        /* IDAC control (ADS1148.pfd p54,55) */
        void setIdacCurrent(const std::uint8_t& current) const;
        void setIdac1Pin(const std::uint8_t& pin) const;
        void setIdac2Pin(const std::uint8_t& pin) const;
//        /* Burnout current sources (short or open circuit detection) */
//        bool checkSensor() const;
        /* GPIO  (ADS1148.pfd p59) */
        void setGpio(const std::uint8_t& pin) const;
        void setOutput(const std::uint8_t& pin) const;
        void setInput(const std::uint8_t& pin) const;
        void setGpioHigh(const std::uint8_t& pin) const;
        void setGpioLow(const std::uint8_t& pin) const;
        /* External multiplexer */
        void selectExtChannel1() const; // External multiplexer channel 1 (000 ABC)
        void selectExtChannel2() const; // External multiplexer channel 2 (001 ABC)
        void selectExtChannel3() const; // External multiplexer channel 3 (010 ABC)
        void selectExtChannel4() const; // External multiplexer channel 4 (011 ABC)
        void selectExtChannel5() const; // External multiplexer channel 5 (100 ABC)
        void selectExtChannel6() const; // External multiplexer channel 6 (101 ABC)
        void selectExtChannel7() const; // External multiplexer channel 7 (110 ABC)
        void selectExtChannel8() const; // External multiplexer channel 8 (111 ABC)
        /* Register manipulation */
        std::uint8_t readRegister(const std::uint8_t& reg, std::uint8_t registersToRead = 0x01) const;
        void writeRegister(const std::uint8_t& reg, const std::uint8_t& data, std::uint8_t registersToWrite = 0x01) const;
        /* Conversion */
        void startConversion() const;
        int16_t getConversion() const;
        int16_t convertAdcOutputToResistance(int16_t& adcOut);
        /* Variables */
        /* Commands */
        static constexpr  std::uint16_t commandWake             = 0x0001;
        static constexpr  std::uint16_t commandSleep            = 0x0002;
        static constexpr  std::uint8_t commandWriteRegister     = 0x04;
        static constexpr  std::uint8_t commandReadRegister      = 0x02;
        static constexpr  std::uint8_t commandReadData          = 0x12;
        static constexpr  std::uint8_t commandNoOperation       = 0xff;
        static constexpr  std::uint8_t commandReset             = 0x06;
        /* Registers */
        static constexpr  std::uint8_t multiplexerControlRegister0  = 0x00;
        static constexpr  std::uint8_t multiplexerControlRegister1  = 0x02;
        static constexpr  std::uint8_t systemControlRegister0       = 0x03;
        static constexpr  std::uint8_t idacControlRegister0         = 0x0a;
        static constexpr  std::uint8_t idacControlRegister1         = 0x0b;
        static constexpr  std::uint8_t gpioConfigRegister           = 0x0c;
        static constexpr  std::uint8_t gpioDirectionRegister        = 0x0d;
        static constexpr  std::uint8_t gpioDataRegister             = 0x0e;
        /* Gpios */
        const GPIO::Gpio& startPin;
        const GPIO::Gpio& drdyPin;
        /* pin 5  - bit 5 - (designated: D)
         * pin 6  - bit 6 - (designated: E)
         * pin 16 - bit 7 - (designated: F) */
        static constexpr uint8_t D = GPIO0;
        static constexpr uint8_t E = GPIO1;
        static constexpr uint8_t F = GPIO7;
        /* Circuit specific */
        /* Rrtd = ADCout*Rref / Gain * 2^(adcBits-1) (Because of 2s complement) */
        static constexpr std::uint8_t measGain = ADS1148::GAIN_8;
        static constexpr std::uint8_t burnOutGain = ADS1148::GAIN_1;
        static constexpr  std::uint8_t burnOutCurrentOn = ADS1148::BURNOUT_10;
        static constexpr  std::uint8_t burnOutCurrentOff = ADS1148::BURNOUT_OFF;
        static constexpr  std::uint8_t iDacCurrentOn = ADS1148::IDAC_1500;
        static constexpr  std::uint8_t iDacCurrentOff = ADS1148::IDAC_OFF;
        static constexpr  std::uint8_t sampleRate = ADS1148::SPS_20;
        static constexpr  std::uint8_t adcRef = ADS1148::REF1;
        static constexpr  std::uint8_t iDacRef = ADS1148::ALWAYS_ON;
        const             std::uint8_t gainFactor = ((2<<(this->measGain-1))/4)+(15); // 2^17 for a gain of 8
        static constexpr  std::uint16_t rRef = 820;                         // 802 Ohm
        static constexpr  std::uint8_t channel1PositiveInput = ADS1148::AIN1;
        static constexpr  std::uint8_t channel1NegativeInput = ADS1148::AIN2;
        static constexpr  std::uint8_t channel1iDac1 = ADS1148::AIN0;
        static constexpr  std::uint8_t channel1iDac2 = ADS1148::AIN3;
        static constexpr  std::uint8_t channel2PositiveInput = ADS1148::AIN4;
        static constexpr  std::uint8_t channel2NegativeInput = ADS1148::AIN5;
        static constexpr  std::uint8_t channel2iDac1 = ADS1148::AIN6;
        static constexpr  std::uint8_t channel2IDac2 = ADS1148::IEXC2;
        static constexpr  std::uint8_t iDacDisconnect = ADS1148::IDAC_DISCONNECT;
        /*  Burn-out source is set with a gain of 1 and 10uA current source */
        static constexpr  std::int16_t OPEN_CIRCUIT_THRESHOLD = 29491;  // Full-scale - 5%
        static constexpr  std::int16_t SHORT_CIRCUIT_THRESHOLD = 82;//1638; // 5% reading
        /* GLOBAL OBJECT CONSTANTS */
        static constexpr std::uint32_t settleTime = 10000; // 10mS (must be known at compile time)
        static constexpr std::uint32_t resetTime = 160000; // 160mS (must be known at compile time)
        static constexpr std::uint32_t csSettleTime = 10; // 10uS (must be known at compile time);
    };

    /* Prototypes */
    // None
}



#endif /* TEMPSENSOR_SRC_ADS1148_HPP_ */
