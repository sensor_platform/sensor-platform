/* types.hpp */
/* Nick van den Berg - Hogeschool Rotterdam - IHC Systems B.V.
 * Made for MSP430G2452 microcontroller                        */

/* This is purely for users, libraries should work
 *  without types.hpp, be aware: instances of PIN::Port
 *  will have to be declared manually without types.hpp/cpp */

#ifndef TEMPSENSOR_SRC_TYPES_H_
#define TEMPSENSOR_SRC_TYPES_H_

#include <cstdint>

#include "pin.hpp"
#include "usi.hpp"


/* PIN */
enum pinState : const std::uint8_t
{
    LOW = 0x0, HIGH = 0x1, OFF = 0x0, ON = 0x1
};

enum pinMode : const std::uint8_t
{
    INPUT = 0x0, OUTPUT = 0x1
};

enum pin : const std::uint8_t
{
    PIN_0 = 0x01, PIN_1 = 0x02, PIN_2 = 0x04, PIN_3 = 0x08,
    PIN_4 = 0x10, PIN_5 = 0x20, PIN_6 = 0x40, PIN_7 = 0x80
};

enum port : const uint8_t
{
  PORT_A = 0x0, PORT_1 = 0x0, PORT_B = 0x1, PORT_2 = 0x1
};

/* USI */
enum enable : const std::uint8_t
{
    DISABLED = 0x0, ENABLED = 0x1
};

enum shiftRegFirst : const std::uint8_t
{
    MSB = 0x0, LSB = 0x1
};

enum masterSelect : const std::uint8_t
{
    SLAVE = 0x0, MASTER = 0x1
};

enum outputLatch : const std::uint8_t
{
    LATCH_MODE_0 = 0x0, LATCH_MODE_1 = 0x1
};

enum clockPhase : const std::uint8_t
{
    PHASE_MODE_0 = 0x0, PHASE_MODE_1= 0x1
};

enum clockSource : const std::uint8_t
{
    ACLK = 0x1, SMCLK = 0x2, SOFTWARECLK = 0x3
};

enum clockDivider : const std::uint8_t
{
    DIV_1   = 0x0, DIV_2   = 0x1, DIV_4   = 0x2, DIV_8   = 0x3,
    DIV_16  = 0x4, DIV_32  = 0x5, DIV_64  = 0x6, DIV_128 = 0x7
};

enum clockPolarity : const std::uint8_t
{
  INACTIVE_LOW = 0x0, INACTIVE_HIGH = 0x1
};

enum bitMode : const std::uint8_t
{
    BIT_MODE_8 = 0x0, BIT_MODE_16= 0x1
};

enum sclReleaseMode : const std::uint8_t
{
    HOLD_LOW = 0x0, RELEASED = 0x1
};

#endif // TEMPSENSOR_SRC_TYPES_H_
