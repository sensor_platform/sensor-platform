/* pin.cpp */
/* Nick van den Berg - Hogeschool Rotterdam - IHC Systems B.V.
 * Made for MSP430G2452 microcontroller
 * Used documents:
 * msp430x2xx family user guide p327-p334
 * msp430g2452 datasheet p37-p48                                 */

/* Example class:
 * NAMESPACE::Object::Method(NAMESPACE::attribute argument): attribute(argument)
 * {                                                                  ^
 *  method body                                                       |
 * }                                                            initialization
 */

#include "pin.hpp"


namespace PIN{

    PIN::Pin::Pin(const uint8_t& p, const uint8_t& pp): port(p), pin(pp) {}

    /* Pin direction select
     * 0 Input
     * 1 Output             */
    void PIN::Pin::setDir(std::uint8_t state) const
    {
        switch(this->port)
        {
        case PIN::PORT_A:
            if(state == PIN::LOW)
            {
                *this->portA.direction &= ~this->pin;
            }
            else if(state == PIN::HIGH)
            {
                *this->portA.direction |= this->pin;
            }
            break;
        case PIN::PORT_B:
            if(state == PIN::LOW)
            {
                *this->portB.direction &= ~this->pin;
            }
            else if(state == PIN::HIGH)
            {
                *this->portB.direction |= this->pin;
            }
            break;
        }
    }

    /* Pin Function select (setSel() and setSel2())
     * sel1 sel2
     *  0     0 I/O function is selected.
     *  0     1 Primary peripheral module function is selected.
     *  1     0 Reserved. See device-specific data sheet.
     *  1     1 Secondary peripheral module function is selected. */
    void PIN::Pin::setSel(std::uint8_t state) const
    {
        switch(this->port)
        {
        case PIN::PORT_A:
            if(state == PIN::LOW)
            {
                *this->portA.select &= ~this->pin;
            }
            else if(state == PIN::HIGH)
            {
                *this->portA.select |= this->pin;
            }
            break;
        case PIN::PORT_B:
            if(state == PIN::LOW)
            {
                *this->portB.select &= ~this->pin;
            }
            else if(state == PIN::HIGH)
            {
                *this->portB.select |= this->pin;
            }
            break;
        }
    }

    /* PxSEL2 PxSEL Pin Function
     * sel1 sel2
     *  0     0 I/O function is selected.
     *  0     1 Primary peripheral module function is selected.
     *  1     0 Reserved. See device-specific data sheet.
     *  1     1 Secondary peripheral module function is selected. */
    void PIN::Pin::setSel2(std::uint8_t state) const
    {
        switch(this->port)
        {
        case PIN::PORT_A:
            if(state == PIN::LOW)
            {
                *this->portA.select2 &= ~this->pin;
            }
            else if(state == PIN::HIGH)
            {
                *this->portA.select2 |= this->pin;
            }
            break;
        case PIN::PORT_B:
            if(state == PIN::LOW)
            {
                *this->portB.select2 &= ~this->pin;
            }
            else if(state == PIN::HIGH)
            {
                *this->portB.select2 |= this->pin;
            }
            break;
        }
    }

    /* Select pin function as GPIO */
    void PIN::Pin::setGpio() const
    {
        this->setSel(PIN::LOW);
        this->setSel2(PIN::LOW);
    }

    /* Set pin state
     * 0 LOW  (VSS)
     * 1 HIGH (VDD)  */
    void PIN::Pin::setState(std::uint8_t state) const
    {
        switch(this->port)
        {
        case PIN::PORT_A:
            if(state == PIN::LOW)
            {
                *this->portA.output &= ~this->pin;
            }
            else if(state == PIN::HIGH)
            {
                *this->portA.output |= this->pin;
            }
            break;
        case PIN::PORT_B:
            if(state == PIN::LOW)
            {
                *this->portB.output &= ~this->pin;
            }
            else if(state == PIN::HIGH)
            {
                *this->portB.output |= this->pin;
            }
            break;
        }
    }

    /* Return pin state
     * 0 LOW  (VSS)
     * 1 HIGH (VDD)  */
    std::uint8_t  PIN::Pin::getState() const
    {
        uint8_t state = 0x0;
        switch(this->port)
        {
        case PIN::PORT_A:
            state = ( ((*this->portA.input)&(this->pin)) != 0x0) ? PIN::HIGH : PIN::LOW;
            break;
        case PIN::PORT_B:
            state = ( ((*this->portB.input)&(this->pin)) != 0x0) ? PIN::HIGH : PIN::LOW;
            break;
        }
        return state;
    }

    void PIN::Pin::resetPorts()
    {
        //Initializes ports for low quiescent current as advised by CCS low power advisor
        // For port A
        *PIN::Pin::portA.input                  = 0x0;
        *PIN::Pin::portA.output                 = 0x0;
        *PIN::Pin::portA.direction              = 0x0;
        *PIN::Pin::portA.interruptFlag          = 0x0;
        *PIN::Pin::portA.interruptEdgeSelect    = 0x0;
        *PIN::Pin::portA.interruptEnable        = 0x0;
        *PIN::Pin::portA.select                 = 0x0;
        *PIN::Pin::portA.select2                = 0x0;
        *PIN::Pin::portA.resistorEnable         = 0x0;
        // For port B
        *PIN::Pin::portB.input                  = 0x0;
        *PIN::Pin::portB.output                 = 0x0;
        *PIN::Pin::portB.direction              = 0x0;
        *PIN::Pin::portB.interruptFlag          = 0x0;
        *PIN::Pin::portB.interruptEdgeSelect    = 0x0;
        *PIN::Pin::portB.interruptEnable        = 0x0;
        *PIN::Pin::portB.select                 = 0x0;
        *PIN::Pin::portB.select2                = 0x0;
        *PIN::Pin::portB.resistorEnable         = 0x0;
    }

}
