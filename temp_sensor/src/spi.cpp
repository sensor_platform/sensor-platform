/* Nick van den Berg - IHC Systems B.V.
 * Made for MSP430G2452 microcontroller
 * Used documents:
 * msp430x2xx family user guide p396-
 * msp430g2452 datasheet p37-p48
 * */

#include "spi.hpp"


namespace SPI{
/* SpiDevice */
    /* Constructors */
    SPI::SpiDevice::SpiDevice(const uint8_t& ppSclk, const std::uint8_t& pSclk,
                              const uint8_t& ppSdo, const std::uint8_t& pSdo,
                              const uint8_t& ppSdi, const std::uint8_t& pSdi,
                              const GPIO::Gpio& chipSelect, const std::uint8_t& activeState,
                              const SPI::SpiSettings& spiSettings, const std::uint8_t& NOP):
                                      usi(USI::Usi(ppSclk, pSclk, ppSdo, pSdo, ppSdi, pSdi)),
                                      chipSelectPin(chipSelect), activeState(activeState),
                                      settings(spiSettings), noOperation(NOP)
    {
        /* USI register configuration */
        this->halt(); // Halt USI operation

        this->applySettings(); // Apply SpiSettings and non-configurable spi(i2c) registers

        this->release(); // Release USI for operation
    }

    /* General */
    void SPI::SpiDevice::applySettings() const
    {
        this->halt(); // Halt USI operation

        /* SpiSettings configuratio */
        this->usi.setMasterSlave(this->settings.mode);
        this->usi.setClockSource(this->settings.clock);
        this->usi.setClockDivider(this->settings.clockDiv);
        this->usi.setBitMode(this->settings.bitMode);
        this->usi.setFirstSendBit(this->settings.firstBit);
        this->usi.setClockPolarity(this->settings.clockPolarity);
        this->usi.setClockPhase(this->settings.phaseMode);
        this->usi.setOutputLatch(this->settings.latchMode);

        /* I2c and interrupt settings */
        this->usi.setSdiSda(USI::ENABLED);
        this->usi.setSdoScl(USI::ENABLED);
        this->usi.setSclk(USI::ENABLED);
        this->usi.setDataOutput(USI::ENABLED);
        this->usi.setResetState(USI::DISABLED);

        this->usi.setI2cMode(USI::DISABLED);
        this->usi.setStartInterrupt(USI::DISABLED);
        this->usi.setCounterInterrupt(USI::DISABLED);
        this->usi.setArbitrationLostCondition(USI::DISABLED);

        this->usi.setInterruptFlagClearMode(USI::DISABLED);

        this->release(); // Release USI for operation
    }

    void SPI::SpiDevice::halt() const
    {
        this->usi.setSclReleaseMode(USI::HALT); // Halt USI operation
    }

    void SPI::SpiDevice::release() const
    {
        this->usi.setSclReleaseMode(USI::RELEASE); // Release USI for operation
    }

    void SPI::SpiDevice::select() const
    {
        this->chipSelectPin.setState(this->activeState);
    }

    void SPI::SpiDevice::deselect() const
    {
        this->chipSelectPin.setState(!(this->activeState));
    }

    void SPI::SpiDevice::sendByte(std::uint8_t byte) const
    {
        std::uint8_t x = 0;
        while(this->usi.getBitCounter() > 0) //&& x < 0xFF)  // Wait till no active transmission
        {
            //x++; // Timeout
        }
        this->usi.setLowByte(byte);
        this->usi.setBitCounter(8);
    }

    void SPI::SpiDevice::sendTwoBytes(std::uint16_t bytes) const
    {
        std::uint8_t x = 0;
        while(this->usi.getBitCounter() > 0 && x < 0xFF)  // Wait till no active transmission
        {
            x++; // Timeout
        }
        this->usi.setLowByte(bytes & 0x00FF);
        this->usi.setHighByte(bytes>>8);
        this->usi.setBitCounter(16);
    }

    /* receiveByte blocks while receiving (8 SPI clocks) */
    std::uint8_t SPI::SpiDevice::receiveByte() const
    {
        std::uint8_t x = 0;
        while(this->usi.getBitCounter() > 0)// && x < 0xFF)  // Wait till no active transmission
        {
            //x++; // Timeout
        }
        this->usi.setLowByte(this->noOperation);
        this->usi.setBitCounter(8);
        x = 0;
        while(this->usi.getBitCounter() > 0)// && x < 0xFF) // Wait for USI to count to 0 (and having shift in the data)
        {
            //x++; // Timeout
        }
        return this->usi.getLowByte();
    }

//    /* receiveTwoBytes blocks while receiving (16 SPI clocks) */
//    std::uint16_t SPI::SpiDevice::receiveTwoBytes() const
//    {
//        while(this->usi.getBitCounter() > 0) {} // Wait till no active transmission
//        this->usi.setLowByte(this->noOperation);
//        this->usi.setHighByte(this->noOperation);
//        this->usi.setBitCounter(16);
//        while(this->usi.getBitCounter() < 8) {}; // Wait for shiftregister to be filled
//        return (this->usi.getHighByte()<<8) | (this->usi.getLowByte());
//    }

    void SPI::SpiDevice::setClockPhase() const
    {
        this->usi.setClockPhase(this->settings.phaseMode);
    }
}

