/* usi.hpp */
/* Nick van den Berg - IHC Systems B.V.
 * Made for MSP430G2452 microcontroller
 * Used documents:
 * msp430x2xx family user guide p396-p409
 * msp430g2452 datasheet p37-p48
 * */

#ifndef TEMPSENSOR_SRC_USI_H_
#define TEMPSENSOR_SRC_USI_H_

#include <cstdint>

#include "msp430.h"

#include "pin.hpp"


namespace USI{
    /* Datatypes */
    enum masterSelect : const std::uint8_t
    {
        SLAVE = 0x0, MASTER = 0x1
    };

    enum shiftRegFirst : const std::uint8_t
    {
        MSB = 0x0, LSB = 0x1
    };

    enum outputLatch : const std::uint8_t
    {
        LATCH_MODE_0 = 0x0, LATCH_MODE_1 = 0x1
    };

    enum clockPhase : const std::uint8_t
    {
        PHASE_MODE_0 = 0x0, PHASE_MODE_1= 0x1
    };

    enum clockSource : const std::uint8_t
    {
        ACLK = 0x1, SMCLK = 0x2, SOFTWARECLK = 0x3
    };

    enum clockDivider : const std::uint8_t
    {
        DIV_1   = 0x0, DIV_2   = 0x1, DIV_4   = 0x2, DIV_8   = 0x3,
        DIV_16  = 0x4, DIV_32  = 0x5, DIV_64  = 0x6, DIV_128 = 0x7
    };

    enum bitMode : const std::uint8_t
    {
        BIT_MODE_8 = 0x0, BIT_MODE_16= 0x1
    };

    enum usiMode : const std::uint8_t
    {
        SPI_MODE = 0x0, I2C_MODE = 0x1
    };

    enum enable : const std::uint8_t
    {
        DISABLED = 0x0, ENABLED = 0x1, LOW = 0x0, HIGH = 0x1
    };

    enum clockPolarity : const std::uint8_t
    {
      INACTIVE_LOW = 0x0, INACTIVE_HIGH = 0x1
    };

    enum sclReleaseMode : const std::uint8_t
    {
        HALT = 0x0, RELEASE = 0x1
    };

    struct UsiAddr
    {
        /* Control registers */
        static volatile constexpr uint8_t* control0         = &USICTL0;
        static volatile constexpr uint8_t* control1         = &USICTL1;
        static volatile constexpr uint8_t* controlClock     = &USICKCTL;
        static volatile constexpr uint8_t* bitCounter       = &USICNT;
        static volatile constexpr uint8_t* lowByte          = &USISRL;
        static volatile constexpr uint8_t* highByte         = &P1IE;
        static volatile constexpr uint16_t* shiftRegister   = &USISR;
    };


    /* Classes */
    class Usi
    {
    public:
        /* Constructor method */
        Usi(const uint8_t& ppSclk, const std::uint8_t& pSclk,
            const uint8_t& ppSdo, const std::uint8_t& pSdo,
            const uint8_t& ppSdi, const std::uint8_t& pSdi); // SPI (3-PINS)
        Usi(const uint8_t& ppScl, const std::uint8_t& pScl,
            const uint8_t& ppSda, const std::uint8_t& pSda); // I2C (2-PINS) (data-in-pin = data-out-pin)
        /* USI control register 0 methods */
        void setSdiSda(const std::uint8_t& mode) const;
        void setSdoScl(const std::uint8_t& mode) const;
        void setSclk(const std::uint8_t& mode) const;
        void setFirstSendBit(const std::uint8_t& mode) const;
        void setMasterSlave(const std::uint8_t& mode) const;
        void setOutputLatch(const std::uint8_t& mode) const;
        void setDataOutput(const std::uint8_t& mode) const;
        void setResetState(const std::uint8_t& mode) const;
        /* USI control register 1 methods */
        void setClockPhase(const std::uint8_t& mode) const;
        void setI2cMode(const std::uint8_t& mode) const;
        void setStartInterrupt(const std::uint8_t& mode) const;
        void setCounterInterrupt(const std::uint8_t& mode) const;
        void setArbitrationLostCondition(const std::uint8_t& mode) const;
        std::uint8_t getStopCondition() const;
        std::uint8_t getStartCondition() const;
        std::uint8_t getCounterInterruptFlag() const;
        /* USI clock control register methods */
        void setClockSource(const std::uint8_t& mode) const;
        void setClockDivider(const std::uint8_t& mode) const;
        void setClockPolarity(const std::uint8_t& mode) const;
        void setSoftwareClockBit(const std::uint8_t& mode) const;
        /* USI bit counter register */
        void setSclReleaseMode(const std::uint8_t& mode) const;
        void setBitMode(const std::uint8_t& mode) const;
        void setInterruptFlagClearMode(const std::uint8_t& mode) const;
        void setBitCounter(std::uint8_t countAmount) const;
        std::uint8_t getBitCounter() const;
        /* USI byte shift register methods */
        void setLowByte(std::uint8_t byte) const;
        void setHighByte(std::uint8_t byte) const;
        void setHighAndLowByte(uint16_t bytes) const;
        void sendByte(std::uint8_t byte) const;
        std::uint8_t getLowByte() const;
        std::uint8_t getHighByte() const;
        void sendTwoBytes(uint16_t bytes) const;

    private:
        static const USI::UsiAddr usi;
        const PIN::Pin clockPin;
        const PIN::Pin dataOutPin;
        const PIN::Pin dataInPin;
    };

    /* Prototypes */
    // None
}
#endif // TEMPSENSOR_SRC_USI_H_
