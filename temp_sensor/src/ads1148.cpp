#include "ads1148.hpp"

namespace ADS1148
{

    /* Variable redefinition to force compiler to create variables during compile-time */
    /* Variables */
    /* Commands */
    constexpr  std::uint16_t ADS1148::Ads1148::commandWake;
    constexpr  std::uint16_t ADS1148::Ads1148::commandSleep;
    constexpr  std::uint8_t ADS1148::Ads1148::commandWriteRegister;
    constexpr  std::uint8_t ADS1148::Ads1148::commandReadRegister;
    constexpr  std::uint8_t ADS1148::Ads1148::commandReadData;
    constexpr  std::uint8_t ADS1148::Ads1148::commandNoOperation;
    constexpr  std::uint8_t ADS1148::Ads1148::commandReset;
    /* Registers */
    constexpr  std::uint8_t ADS1148::Ads1148::multiplexerControlRegister0;
    constexpr  std::uint8_t ADS1148::Ads1148::multiplexerControlRegister1;
    constexpr  std::uint8_t ADS1148::Ads1148::systemControlRegister0;
    constexpr  std::uint8_t ADS1148::Ads1148::idacControlRegister0;
    constexpr  std::uint8_t ADS1148::Ads1148::idacControlRegister1;
    constexpr  std::uint8_t ADS1148::Ads1148::gpioConfigRegister;
    constexpr  std::uint8_t ADS1148::Ads1148::gpioDirectionRegister;
    constexpr  std::uint8_t ADS1148::Ads1148::gpioDataRegister;
    /* Gpios */
    constexpr uint8_t ADS1148::Ads1148::D;
    constexpr uint8_t ADS1148::Ads1148::E;
    constexpr uint8_t ADS1148::Ads1148::F;
    /* Circuit specific */
    constexpr std::uint8_t ADS1148::Ads1148::measGain;
    constexpr std::uint8_t ADS1148::Ads1148::burnOutGain;
    constexpr  std::uint8_t ADS1148::Ads1148::burnOutCurrentOn;
    constexpr  std::uint8_t ADS1148::Ads1148::burnOutCurrentOff;
    constexpr  std::uint8_t ADS1148::Ads1148::iDacCurrentOn;
    constexpr  std::uint8_t ADS1148::Ads1148::iDacCurrentOff;
    constexpr  std::uint8_t ADS1148::Ads1148::sampleRate;
    constexpr  std::uint8_t ADS1148::Ads1148::adcRef;
    constexpr  std::uint8_t ADS1148::Ads1148::iDacRef;
    //constexpr  std::uint8_t ADS1148::Ads1148::gainFactor;
    constexpr  std::uint16_t ADS1148::Ads1148::rRef;
    constexpr  std::uint8_t ADS1148::Ads1148::channel1PositiveInput;
    constexpr  std::uint8_t ADS1148::Ads1148::channel1NegativeInput;
    constexpr  std::uint8_t ADS1148::Ads1148::channel1iDac1;
    constexpr  std::uint8_t ADS1148::Ads1148::channel1iDac2;
    constexpr  std::uint8_t ADS1148::Ads1148::channel2PositiveInput;
    constexpr  std::uint8_t ADS1148::Ads1148::channel2NegativeInput;
    constexpr  std::uint8_t ADS1148::Ads1148::channel2iDac1;
    constexpr  std::uint8_t ADS1148::Ads1148::channel2IDac2;
    constexpr  std::uint8_t ADS1148::Ads1148::iDacDisconnect;
    /*  Burn-out source is set with a gain of 1 and 10uA current source */
    constexpr  std::int16_t ADS1148::Ads1148::OPEN_CIRCUIT_THRESHOLD;
    constexpr  std::int16_t ADS1148::Ads1148::SHORT_CIRCUIT_THRESHOLD;
    /* GLOBAL OBJECT CONSTANTS */
    constexpr std::uint32_t ADS1148::Ads1148::settleTime;
    constexpr std::uint32_t ADS1148::Ads1148::resetTime;
    constexpr std::uint32_t ADS1148::Ads1148::csSettleTime;

    /* Constructor */
    ADS1148::Ads1148::Ads1148(const uint8_t& ppSclk, const uint8_t& pSclk,
                              const uint8_t& ppSdo, const uint8_t& pSdo,
                              const uint8_t& ppSdi, const uint8_t& pSdi,
                              const GPIO::Gpio& csPin, const uint8_t& activeState,
                              const GPIO::Gpio& startPin, const GPIO::Gpio& drdyPin,
                              const SPI::SpiSettings& spiSettings):
                                 SPI::SpiDevice(ppSclk, pSclk,
                                                ppSdo, pSdo,
                                                ppSdi, pSdi,
                                                csPin, activeState,
                                                spiSettings, this->commandNoOperation),
                                                startPin(startPin), drdyPin(drdyPin)
    {
        /* Initialization + reset for good measure (start-up glitches) */
        this->resetDevice();
    }

    /* Member functions */
    int16_t ADS1148::Ads1148::measSensor1()
    {
        /* Reset comms */
        this->resetSpi();
        /* Select proper internal and external mux-channels */
        this->selectExtChannel1();
        this->setIntChannel1();
        /* Measure using burn-out current sources */
        int16_t meas = checkSensor();
        /* Check shorted or open circuit */
        if(meas <= this->SHORT_CIRCUIT_THRESHOLD) // Sensor shorted
        {
            meas = 0x0000; // 0x0000 means short-circuit
        }
        else if(meas >= this->OPEN_CIRCUIT_THRESHOLD) // Sensor open-circuited
        {
            meas= 0x7FFF; // 0xFFFF means open-circuit
        }
        else
        {
            this->setIdacCurrent(this->iDacCurrentOn);      // Set IDAC current 1,5mA
            __delay_cycles(settleTime);                     // Let IDAC settle 1ms (could be less time? experiment.)
            meas = this->getConversion();
            this->setIdacCurrent(this->iDacCurrentOff);     // Set IDAC current 0
        }
        this->deselect();
        return meas;
    }

    int16_t ADS1148::Ads1148::measSensor2()
    {
        /* Reset comms */
        this->resetSpi();
        /* Select proper internal and external mux-channels */
        this->selectExtChannel2();
        this->setIntChannel1();
        /* Measure using burn-out current sources */
        int16_t meas = checkSensor();
        /* Check shorted or open circuit */
        if(meas <= this->SHORT_CIRCUIT_THRESHOLD) // Sensor shorted
        {
            meas = 0x0000; // 0x0000 means short-circuit
        }
        else if(meas >= this->OPEN_CIRCUIT_THRESHOLD) // Sensor open-circuited
        {
            meas= 0x7FFF; // 0xFFFF means open-circuit
        }
        else
        {
            this->setIdacCurrent(this->iDacCurrentOn);      // Set IDAC current 1,5mA
            __delay_cycles(settleTime);                     // Let IDAC settle 1ms (could be less time? experiment.)
            meas = this->getConversion();
            this->setIdacCurrent(this->iDacCurrentOff);     // Set IDAC current 0
        }
        this->deselect();
        return meas;
    }

    int16_t ADS1148::Ads1148::measSensor3()
    {
        /* Reset comms */
        this->resetSpi();
        /* Select proper internal and external mux-channels */
        this->selectExtChannel3();
        this->setIntChannel1();
        /* Measure using burn-out current sources */
        int16_t meas = checkSensor();
        /* Check shorted or open circuit */
        if(meas <= this->SHORT_CIRCUIT_THRESHOLD) // Sensor shorted
        {
            meas = 0x0000; // 0x0000 means short-circuit
        }
        else if(meas >= this->OPEN_CIRCUIT_THRESHOLD) // Sensor open-circuited
        {
            meas= 0x7FFF; // 0xFFFF means open-circuit
        }
        else
        {
            this->setIdacCurrent(this->iDacCurrentOn);      // Set IDAC current 1,5mA
            __delay_cycles(settleTime);                     // Let IDAC settle 1ms (could be less time? experiment.)
            meas = this->getConversion();
            this->setIdacCurrent(this->iDacCurrentOff);     // Set IDAC current 0
        }
        this->deselect();
        return meas;
    }

    int16_t ADS1148::Ads1148::measSensor4()
    {
        /* Reset comms */
        this->resetSpi();
        /* Select proper internal and external mux-channels */
        this->selectExtChannel4();
        this->setIntChannel1();
        /* Measure using burn-out current sources */
        int16_t meas = checkSensor();
        /* Check shorted or open circuit */
        if(meas <= this->SHORT_CIRCUIT_THRESHOLD) // Sensor shorted
        {
            meas = 0x0000; // 0x0000 means short-circuit
        }
        else if(meas >= this->OPEN_CIRCUIT_THRESHOLD) // Sensor open-circuited
        {
            meas= 0x7FFF; // 0xFFFF means open-circuit
        }
        else
        {
            this->setIdacCurrent(this->iDacCurrentOn);      // Set IDAC current 1,5mA
            __delay_cycles(settleTime);                     // Let IDAC settle 1ms (could be less time? experiment.)
            meas = this->getConversion();
            this->setIdacCurrent(this->iDacCurrentOff);     // Set IDAC current 0
        }
        this->deselect();
        return meas;
    }

    int16_t ADS1148::Ads1148::measSensor5()
    {
        /* Reset comms */
        this->resetSpi();
        /* Select proper internal and external mux-channels */
        this->selectExtChannel5();
        this->setIntChannel1();
        /* Measure using burn-out current sources */
        int16_t meas = checkSensor();
        /* Check shorted or open circuit */
        if(meas <= this->SHORT_CIRCUIT_THRESHOLD) // Sensor shorted
        {
            meas = 0x0000; // 0x0000 means short-circuit
        }
        else if(meas >= this->OPEN_CIRCUIT_THRESHOLD) // Sensor open-circuited
        {
            meas= 0x7FFF; // 0xFFFF means open-circuit
        }
        else
        {
            this->setIdacCurrent(this->iDacCurrentOn);      // Set IDAC current 1,5mA
            __delay_cycles(settleTime);                     // Let IDAC settle 1ms (could be less time? experiment.)
            meas = this->getConversion();
            this->setIdacCurrent(this->iDacCurrentOff);     // Set IDAC current 0
        }
        this->deselect();
        return meas;
    }


    int16_t ADS1148::Ads1148::measSensor6()
    {
        /* Reset comms */
        this->resetSpi();
        /* Select proper internal and external mux-channels */
        this->selectExtChannel6();
        this->setIntChannel1();
        /* Measure using burn-out current sources */
        int16_t meas = checkSensor();
        /* Check shorted or open circuit */
        if(meas <= this->SHORT_CIRCUIT_THRESHOLD) // Sensor shorted
        {
            meas = 0x0000; // 0x0000 means short-circuit
        }
        else if(meas >= this->OPEN_CIRCUIT_THRESHOLD) // Sensor open-circuited
        {
            meas= 0x7FFF; // 0xFFFF means open-circuit
        }
        else
        {
            this->setIdacCurrent(this->iDacCurrentOn);      // Set IDAC current 1,5mA
            __delay_cycles(settleTime);                     // Let IDAC settle 1ms (could be less time? experiment.)
            meas = this->getConversion();
            this->setIdacCurrent(this->iDacCurrentOff);     // Set IDAC current 0
        }
        this->deselect();
        return meas;
    }

    int16_t ADS1148::Ads1148::measSensor7()
    {
        /* Reset comms */
        this->resetSpi();
        /* Select proper internal and external mux-channels */
        this->selectExtChannel7();
        this->setIntChannel1();
        /* Measure using burn-out current sources */
        int16_t meas = checkSensor();
        /* Check shorted or open circuit */
        if(meas <= this->SHORT_CIRCUIT_THRESHOLD) // Sensor shorted
        {
            meas = 0x0000; // 0x0000 means short-circuit
        }
        else if(meas >= this->OPEN_CIRCUIT_THRESHOLD) // Sensor open-circuited
        {
            meas= 0x7FFF; // 0xFFFF means open-circuit
        }
        else
        {
            this->setIdacCurrent(this->iDacCurrentOn);      // Set IDAC current 1,5mA
            __delay_cycles(settleTime);                     // Let IDAC settle 1ms (could be less time? experiment.)
            meas = this->getConversion();
            this->setIdacCurrent(this->iDacCurrentOff);     // Set IDAC current 0
        }
        this->deselect();
        return meas;
    }

    int16_t ADS1148::Ads1148::measSensor8()
    {
        /* Reset comms */
        this->resetSpi();
        /* Select proper internal and external mux-channels */
        this->selectExtChannel8();
        this->setIntChannel1();
        /* Measure using burn-out current sources */
        int16_t meas = checkSensor();
        /* Check shorted or open circuit */
        if(meas <= this->SHORT_CIRCUIT_THRESHOLD) // Sensor shorted
        {
            meas = 0x0000; // 0x0000 means short-circuit
        }
        else if(meas >= this->OPEN_CIRCUIT_THRESHOLD) // Sensor open-circuited
        {
            meas= 0x7FFF; // 0xFFFF means open-circuit
        }
        else
        {
            this->setIdacCurrent(this->iDacCurrentOn);      // Set IDAC current 1,5mA
            __delay_cycles(settleTime);                     // Let IDAC settle 1ms (could be less time? experiment.)
            meas = this->getConversion();
            this->setIdacCurrent(this->iDacCurrentOff);     // Set IDAC current 0
        }
        this->deselect();
        return meas;
    }

    int16_t ADS1148::Ads1148::measSensor9()
    {
        /* Reset comms */
        this->resetSpi();
        /* Select proper internal and external mux-channels */
        this->selectExtChannel1();
        this->setIntChannel2();
        /* Measure using burn-out current sources */
        int16_t meas = checkSensor();
        /* Check shorted or open circuit */
        if(meas <= this->SHORT_CIRCUIT_THRESHOLD) // Sensor shorted
        {
            meas = 0x0000; // 0x0000 means short-circuit
        }
        else if(meas >= this->OPEN_CIRCUIT_THRESHOLD) // Sensor open-circuited
        {
            meas= 0x7FFF; // 0xFFFF means open-circuit
        }
        else
        {
            this->setIdacCurrent(this->iDacCurrentOn);      // Set IDAC current 1,5mA
            __delay_cycles(settleTime);                     // Let IDAC settle 1ms (could be less time? experiment.)
            meas = this->getConversion();
            this->setIdacCurrent(this->iDacCurrentOff);     // Set IDAC current 0
        }
        this->deselect();
        return meas;
    }

    int16_t ADS1148::Ads1148::measSensor10()
    {
        /* Reset comms */
        this->resetSpi();
        /* Select proper internal and external mux-channels */
        this->selectExtChannel2();
        this->setIntChannel2();
        /* Measure using burn-out current sources */
        int16_t meas = checkSensor();
        /* Check shorted or open circuit */
        if(meas <= this->SHORT_CIRCUIT_THRESHOLD) // Sensor shorted
        {
            meas = 0x0000; // 0x0000 means short-circuit
        }
        else if(meas >= this->OPEN_CIRCUIT_THRESHOLD) // Sensor open-circuited
        {
            meas= 0x7FFF; // 0xFFFF means open-circuit
        }
        else
        {
            this->setIdacCurrent(this->iDacCurrentOn);      // Set IDAC current 1,5mA
            __delay_cycles(settleTime);                     // Let IDAC settle 1ms (could be less time? experiment.)
            meas = this->getConversion();
            this->setIdacCurrent(this->iDacCurrentOff);     // Set IDAC current 0
        }
        this->deselect();
        return meas;
    }

    int16_t ADS1148::Ads1148::measSensor11()
    {
        /* Reset comms */
        this->resetSpi();
        /* Select proper internal and external mux-channels */
        this->selectExtChannel3();
        this->setIntChannel2();
        /* Measure using burn-out current sources */
        int16_t meas = checkSensor();
        /* Check shorted or open circuit */
        if(meas <= this->SHORT_CIRCUIT_THRESHOLD) // Sensor shorted
        {
            meas = 0x0000; // 0x0000 means short-circuit
        }
        else if(meas >= this->OPEN_CIRCUIT_THRESHOLD) // Sensor open-circuited
        {
            meas= 0x7FFF; // 0xFFFF means open-circuit
        }
        else
        {
            this->setIdacCurrent(this->iDacCurrentOn);      // Set IDAC current 1,5mA
            __delay_cycles(settleTime);                     // Let IDAC settle 1ms (could be less time? experiment.)
            meas = this->getConversion();
            this->setIdacCurrent(this->iDacCurrentOff);     // Set IDAC current 0
        }
        this->deselect();
        return meas;
    }

    int16_t ADS1148::Ads1148::measSensor12()
    {
        /* Reset comms */
        this->resetSpi();
        /* Select proper internal and external mux-channels */
        this->selectExtChannel4();
        this->setIntChannel2();
        /* Measure using burn-out current sources */
        int16_t meas = checkSensor();
        /* Check shorted or open circuit */
        if(meas <= this->SHORT_CIRCUIT_THRESHOLD) // Sensor shorted
        {
            meas = 0x0000; // 0x0000 means short-circuit
        }
        else if(meas >= this->OPEN_CIRCUIT_THRESHOLD) // Sensor open-circuited
        {
            meas= 0x7FFF; // 0xFFFF means open-circuit
        }
        else
        {
            this->setIdacCurrent(this->iDacCurrentOn);      // Set IDAC current 1,5mA
            __delay_cycles(settleTime);                     // Let IDAC settle 1ms (could be less time? experiment.)
            meas = this->getConversion();
            this->setIdacCurrent(this->iDacCurrentOff);     // Set IDAC current 0
        }
        this->deselect();
        return meas;
    }

    int16_t ADS1148::Ads1148::measSensor13()
    {
        /* Reset comms */
        this->resetSpi();
        /* Select proper internal and external mux-channels */
        this->selectExtChannel5();
        this->setIntChannel2();
        /* Measure using burn-out current sources */
        int16_t meas = checkSensor();
        /* Check shorted or open circuit */
        if(meas <= this->SHORT_CIRCUIT_THRESHOLD) // Sensor shorted
        {
            meas = 0x0000; // 0x0000 means short-circuit
        }
        else if(meas >= this->OPEN_CIRCUIT_THRESHOLD) // Sensor open-circuited
        {
            meas= 0x7FFF; // 0xFFFF means open-circuit
        }
        else
        {
            this->setIdacCurrent(this->iDacCurrentOn);      // Set IDAC current 1,5mA
            __delay_cycles(settleTime);                     // Let IDAC settle 1ms (could be less time? experiment.)
            meas = this->getConversion();
            this->setIdacCurrent(this->iDacCurrentOff);     // Set IDAC current 0
        }
        this->deselect();
        return meas;
    }

    int16_t ADS1148::Ads1148::measSensor14()
    {
        /* Reset comms */
        this->resetSpi();
        /* Select proper internal and external mux-channels */
        this->selectExtChannel6();
        this->setIntChannel2();
        /* Measure using burn-out current sources */
        int16_t meas = checkSensor();
        /* Check shorted or open circuit */
        if(meas <= this->SHORT_CIRCUIT_THRESHOLD) // Sensor shorted
        {
            meas = 0x0000; // 0x0000 means short-circuit
        }
        else if(meas >= this->OPEN_CIRCUIT_THRESHOLD) // Sensor open-circuited
        {
            meas= 0x7FFF; // 0xFFFF means open-circuit
        }
        else
        {
            this->setIdacCurrent(this->iDacCurrentOn);      // Set IDAC current 1,5mA
            __delay_cycles(settleTime);                     // Let IDAC settle 1ms (could be less time? experiment.)
            meas = this->getConversion();
            this->setIdacCurrent(this->iDacCurrentOff);     // Set IDAC current 0
        }
        this->deselect();
        return meas;
    }

    int16_t ADS1148::Ads1148::measSensor15()
    {
        /* Reset comms */
        this->resetSpi();
        /* Select proper internal and external mux-channels */
        this->selectExtChannel7();
        this->setIntChannel2();
        /* Measure using burn-out current sources */
        int16_t meas = checkSensor();
        /* Check shorted or open circuit */
        if(meas <= this->SHORT_CIRCUIT_THRESHOLD) // Sensor shorted
        {
            meas = 0x0000; // 0x0000 means short-circuit
        }
        else if(meas >= this->OPEN_CIRCUIT_THRESHOLD) // Sensor open-circuited
        {
            meas= 0x7FFF; // 0xFFFF means open-circuit
        }
        else
        {
            this->setIdacCurrent(this->iDacCurrentOn);      // Set IDAC current 1,5mA
            __delay_cycles(settleTime);                     // Let IDAC settle 1ms (could be less time? experiment.)
            meas = this->getConversion();
            this->setIdacCurrent(this->iDacCurrentOff);     // Set IDAC current 0
        }
        this->deselect();
        return meas;
    }

    int16_t ADS1148::Ads1148::measSensor16()
    {
        /* Reset comms */
        this->resetSpi();
        /* Select proper internal and external mux-channels */
        this->selectExtChannel8();
        this->setIntChannel2();
        /* Measure using burn-out current sources */
        int16_t meas = checkSensor();
        /* Check shorted or open circuit */
        if(meas <= this->SHORT_CIRCUIT_THRESHOLD) // Sensor shorted
        {
            meas = 0x0000; // 0x0000 means short-circuit
        }
        else if(meas >= this->OPEN_CIRCUIT_THRESHOLD) // Sensor open-circuited
        {
            meas= 0x7FFF; // 0xFFFF means open-circuit
        }
        else
        {
            this->setIdacCurrent(this->iDacCurrentOn);      // Set IDAC current 1,5mA
            __delay_cycles(settleTime);                     // Let IDAC settle 1ms (could be less time? experiment.)
            meas = this->getConversion();
            this->setIdacCurrent(this->iDacCurrentOff);     // Set IDAC current 0
        }
        this->deselect();
        return meas;
    }

    void ADS1148::Ads1148::wake() const
    {
        this->resetSpi();
        this->sendTwoBytes(this->commandWake);
    }

    void ADS1148::Ads1148::sleep() const
    {
        this->resetSpi();
        this->sendTwoBytes(this->commandSleep);
    }

    void ADS1148::Ads1148::resetSpi() const
    {
        this->deselect();
        this->select();
        __delay_cycles(100000);
    }

    void ADS1148::Ads1148::resetDevice() const
    {
        this->resetSpi();
        this->sendByte(this->commandReset);
        __delay_cycles(this->resetTime);
        this->resetSpi();
        this->init();
        this->deselect();
    }

    void ADS1148::Ads1148::init() const
    {
         /* Initialize GPIOs */
         this->setGpio(this->D | this->E | this->F);
         this->setOutput(this->D | this->E | this->F);
         this->setGpioLow(this->D | this->E | this->F);
         /* Set PGA gain */
         this->setGain(this->measGain);
         /* Set sample-rate */
         this->setSampleRate(this->sampleRate);
         /* Set internal (IDAC) reference */
         this->setIdacRef(this->iDacRef);
         /* Set external (ADC) reference */
         this->setAdcRef(this->adcRef);

         // TEMPORARY (BREABOARD) USE: select (external) channel 1
         //this->setGpioHigh(this->D);
    }

    void ADS1148::Ads1148::testRegisters() const
    {
        this->resetSpi();
        /* Test the registers of the ADS1148. Use debugger breakpoints to read them. */
        volatile uint8_t registerTest = 0x0;
        registerTest = this->readRegister(this->multiplexerControlRegister0);   // 00001010
        registerTest = this->readRegister(this->multiplexerControlRegister1);   // 00101000
        registerTest = this->readRegister(this->systemControlRegister0);        // 00110000
        registerTest = this->readRegister(this->idacControlRegister0);          // 10010000 V 10010000
        registerTest = this->readRegister(this->idacControlRegister1);          // 00000011 V 10001001 V 10011000
        registerTest = this->readRegister(this->gpioConfigRegister);            // 10000011
        registerTest = this->readRegister(this->gpioDirectionRegister);         // 00000000
        registerTest = this->readRegister(this->gpioDataRegister);              // 00000000
        volatile uint8_t breakpointHelper = 0x0;
    }

    void ADS1148::Ads1148::setBurnOutCurrent(const uint8_t& current) const
    {
        /* Save current register configuration */
        std::uint8_t currentSettings = this->readRegister(this->multiplexerControlRegister0);
        /* Remove Burn-Out Current register settings */
        currentSettings = currentSettings & ~0xC0;
        /* Combine configuration and data so current settings are preserved */
        std::uint8_t data = currentSettings | (current<<6);
        /* Write to the MUX0 register (000 = AIN0, 001 = AIN1, 010 = AIN2, 011 = AIN3,)
         *                             100 = AIN4, 101 = AIN5, 110 = AIN6, 111 = AIN7*/
        this->writeRegister(this->multiplexerControlRegister0, data);
    }

    void ADS1148::Ads1148::setPositiveInput(const uint8_t& pin) const
    {
        /* Save current register configuration */
        std::uint8_t currentSettings = this->readRegister(this->multiplexerControlRegister0);
        /* Remove positive input register settings */
        currentSettings = currentSettings & ~0x38;
        /* Combine configuration and data so current settings are preserved */
        std::uint8_t data = currentSettings | (pin<<3);
        /* Write to the MUX0 register (000 = AIN0, 001 = AIN1, 010 = AIN2, 011 = AIN3,)
         *                             100 = AIN4, 101 = AIN5, 110 = AIN6, 111 = AIN7*/
        this->writeRegister(this->multiplexerControlRegister0, data);
    }

    void ADS1148::Ads1148::setNegativeInput(const uint8_t& pin) const
    {
        /* Save current register configuration */
        std::uint8_t currentSettings = this->readRegister(this->multiplexerControlRegister0);
        /* Remove negative input register settings */
        currentSettings = currentSettings & ~0x07;
        /* Combine configuration and data so current settings are preserved */
        std::uint8_t data = currentSettings | (pin<<0);
        /* Write to the MUX0 register (000 = AIN0, 001 = AIN1, 010 = AIN2, 011 = AIN3,)
         *                             100 = AIN4, 101 = AIN5, 110 = AIN6, 111 = AIN7*/
        this->writeRegister(this->multiplexerControlRegister0, data);
    }

    void ADS1148::Ads1148::setIntChannel1() const // IDACs on AIN0 & AIN3, measure AIN1(+) AIN2(-)
    {
        this->setPositiveInput(this->channel1PositiveInput);
        this->setNegativeInput(this->channel1NegativeInput);
        this->setIdac1Pin(this->channel1iDac1);
        this->setIdac2Pin(this->channel1iDac2);
    }

    void ADS1148::Ads1148::setIntChannel2() const // IDACs on IEXC1 & IEXC2, measure AIN1(+) AIN2(-)
    {
        this->setPositiveInput(this->channel2PositiveInput);
        this->setNegativeInput(this->channel2NegativeInput);
        this->setIdac1Pin(this->channel2iDac1);
        this->setIdac2Pin(this->channel2IDac2);
    }

    void ADS1148::Ads1148::setIntChannelDisconnected() const // IDACs disconnected
    {
        this->setIdac1Pin(this->iDacDisconnect);
        this->setIdac2Pin(this->iDacDisconnect);
    }

    /* Write to internal reference control register (00 OFF, 01 ON, 11 Follow device state) */
    void ADS1148::Ads1148::setIdacRef(const std::uint8_t& intRef) const
    {
        /* Save current register configuration */
        std::uint8_t currentSettings = this->readRegister(this->multiplexerControlRegister1);
        /* Remove internal reference register settings */
        currentSettings = currentSettings & ~0x60;
        /* Combine configuration and data so current settings are preserved */
        std::uint8_t data = currentSettings | (intRef<<5);
        /* Write to internal reference control register (00 OFF, 01 ON, 11 Follow device state) */
        this->writeRegister(this->multiplexerControlRegister1, data);
    }

    /* Write to internal reference control register (00 REFP0, 01 REFP1, 10 INTERNAL, 11 REFP0+INTERNAL) */
    void ADS1148::Ads1148::setAdcRef(const std::uint8_t& adcRef) const
    {
        /* Save current register configuration */
        std::uint8_t currentSettings = this->readRegister(this->multiplexerControlRegister1);
        /* Remove internal reference register settings */
        currentSettings = currentSettings & ~0x18;
        /* Combine configuration and data so current settings are preserved */
        std::uint8_t data = currentSettings | (adcRef<<3);
        /* Write to internal reference control register (00 REFP0, 01 REFP1, 10 INTERNAL, 11 REFP0+INTERNAL) */
        this->writeRegister(this->multiplexerControlRegister1, data);
    }

    void ADS1148::Ads1148::setGain(const std::uint8_t& gain) const
    {
        /* Save current register configuration */
        std::uint8_t currentSettings = this->readRegister(this->systemControlRegister0);
        /* Remove pga register settings */
        currentSettings = currentSettings & ~0x70;
        /* Combine configuration and data so current settings are preserved */
        std::uint8_t data = currentSettings | (gain<<4);
        /* Write gain to system control register (000 1, 001 2, 010 4, ... ..., 111 128) */
        this->writeRegister(this->systemControlRegister0, data);
    }

    void ADS1148::Ads1148::setSampleRate(const std::uint8_t& rate) const
    {
        /* Save current register configuration */
        std::uint8_t currentSettings = this->readRegister(this->systemControlRegister0);
        /* Remove sample rate register settings */
        currentSettings = currentSettings & ~0x0F;
        /* Combine configuration and data so current settings are preserved */
        std::uint8_t data = currentSettings | (rate<<0);
        /* Write to internal reference control register (5 to 20000 samples per sec (see enum)) */
        this->writeRegister(this->systemControlRegister0, data);
    }

    void ADS1148::Ads1148::setIdacCurrent(const std::uint8_t& current) const
    {
        /* Save current register configuration */
        std::uint8_t currentSettings = this->readRegister(this->idacControlRegister0);
        /* Remove IDAC current register settings */
        currentSettings = currentSettings & ~0x07;
        /* Combine configuration and data so current settings are preserved */
        std::uint8_t data = currentSettings | (current<<0);
        /* Write to internal reference control register (000 OFF - 111 1500uA (see enum) */
        this->writeRegister(this->idacControlRegister0, data);
    }

    void ADS1148::Ads1148::setIdac1Pin(const std::uint8_t& pin) const
    {
        /* Save current register configuration */
        std::uint8_t currentSettings = this->readRegister(this->idacControlRegister1);
        /* Remove IDAC1 pin register settings */
        currentSettings = currentSettings & ~0xF0;
        /* Combine configuration and data so current settings are preserved */
        std::uint8_t data = currentSettings | (pin<<4);
        /* Write to internal reference control register (AIN0 - AIN1 - IEXC0 - IEXC1 - DISCONNECT (see enum ) */
        this->writeRegister(this->idacControlRegister1, data);
    }

    void ADS1148::Ads1148::setIdac2Pin(const std::uint8_t& pin) const
    {
        /* Save current register configuration */
        std::uint8_t currentSettings = this->readRegister(this->idacControlRegister1);
        /* Remove IDAC2 pin register settings */
        currentSettings = currentSettings & ~0x0F;
        /* Combine configuration and data so current settings are preserved */
        std::uint8_t data = currentSettings | (pin<<0);
        /* Write to internal reference control register (AIN0 - AIN1 - IEXC0 - IEXC1 - DISCONNECT (see enum ) */
        this->writeRegister(this->idacControlRegister1, data);
    }

    /* CheckSensor causes the device to enter an unresponsive state. Since the IDACs are perfectly capable of detecting shorts/open-circuits,
     * these are used instead of the burn-out current sensors, the IDACs also give a more accurate reading due to the higher current capabilities.
     * So this function is unused at this time. */
    int16_t ADS1148::Ads1148::checkSensor() const
    {
        /* When the sensor is open circuit, the reference will be missing, this makes the device give a full-scale
         * reading. When short circuited, there is still some resistance to GND so this is harder to read.
         * Also, 10uA and a gain of 8 causes saturation of the ADC:
         * Vref = Rref * iBurnOut = 820 Ohm*10 uA = 8.2 mV
         * Vsense = RsenseMax * iBurnOut * GAIN = 200 Ohm * 10 uA * 8 = 16mV
         * Low gain required for burn-out measurement: GAIN < 4.                                                                               */
        this->setGain(this->burnOutGain);
        this->setAdcRef(ADS1148::REFINT);
        this->setBurnOutCurrent(this->burnOutCurrentOn); // Set burn-out current 10uA
        // Get a reading
        int16_t meas = this->getConversion();
        this->setBurnOutCurrent(this->burnOutCurrentOff); // Turn off burn-out current source
        this->setGain(this->measGain);         // Switch back to normal gain
        this->setAdcRef(this->adcRef);
        return meas;
    }

    void ADS1148::Ads1148::setGpio(const std::uint8_t& pin) const
    {
        /* Save current register configuration */
        std::uint8_t currentSettings = this->readRegister(this->gpioConfigRegister);
        /* Combine configuration and data so current settings are preserved */
        std::uint8_t data = pin | currentSettings;
        /* Write to the gpio configuration register (0 = normal function, 1 = gpio) */
        this->writeRegister(this->gpioConfigRegister, data);
    }

    void ADS1148::Ads1148::setOutput(const std::uint8_t& pin) const
    {
        /* Save current register configuration */
        std::uint8_t currentSettings = this->readRegister(this->gpioDirectionRegister);
        /* Combine configuration and data so current settings are preserved */
        std::uint8_t data = currentSettings & ~pin;
        /* Write to the gpio direction register (1 = input, 0 = output) */
        this->writeRegister(this->gpioDirectionRegister, data);
    }

    void ADS1148::Ads1148::setInput(const std::uint8_t& pin) const
    {
        /* Save current register configuration */
        std::uint8_t currentSettings = this->readRegister(this->gpioDirectionRegister);
        /* Combine configuration and data so current settings are preserved */
        std::uint8_t data = currentSettings | pin;
        /* Write to the gpio direction register (1 = input, 0 = output) */
        this->writeRegister(this->gpioDirectionRegister, data);
    }

    void ADS1148::Ads1148::setGpioHigh(const std::uint8_t& pin) const
    {
        /* Save current register configuration */
        std::uint8_t currentSettings = this->readRegister(this->gpioDataRegister);
        /* Combine configuration and data so current settings are preserved */
        std::uint8_t data = currentSettings | pin;
        /* Write to the gpio data register (0 = LOW, 1 = HIGH) */
        this->writeRegister(this->gpioDataRegister, data);
    }

    void ADS1148::Ads1148::setGpioLow(const std::uint8_t& pin) const
    {
        /* Save current register configuration */
        std::uint8_t currentSettings = this->readRegister(this->gpioDataRegister);
        /* Combine configuration and data so current settings are preserved */
        std::uint8_t data = currentSettings & ~pin;
        /* Write to the gpio data register (0 = LOW, 1 = HIGH) */
        this->writeRegister(this->gpioDataRegister, data);
    }



    void ADS1148::Ads1148::selectExtChannel1() const // External multiplexer channel 1 (000 FED)
    {
       this->setGpioLow(this->D | this->E | this->F);
    }

    void ADS1148::Ads1148::selectExtChannel2() const // External multiplexer channel 2 (001 FED)
    {
        this->setGpioLow(this->E | this->F);
        this->setGpioHigh(this->D);
    }

    void ADS1148::Ads1148::selectExtChannel3() const // External multiplexer channel 3 (010 FED)
    {
        this->setGpioLow(this->D | this->F);
        this->setGpioHigh(this->E);
    }

    void ADS1148::Ads1148::selectExtChannel4() const // External multiplexer channel 4 (011 FED)
    {
        this->setGpioLow(this->F);
        this->setGpioHigh(this->D | this->E);
    }

    void ADS1148::Ads1148::selectExtChannel5() const // External multiplexer channel 5 (100 FED)
    {
        this->setGpioLow(this->D | this->E);
        this->setGpioHigh(this->F);
    }

    void ADS1148::Ads1148::selectExtChannel6() const // External multiplexer channel 6 (101 FED)
    {
        this->setGpioLow(this->E);
        this->setGpioHigh(this->D | this->F);
    }

    void ADS1148::Ads1148::selectExtChannel7() const // External multiplexer channel 7 (110 FED)
    {
        this->setGpioLow(this->D);
        this->setGpioHigh(this->E | this->F);
    }

    void ADS1148::Ads1148::selectExtChannel8() const // External multiplexer channel 8 (111 FED)
    {
        this->setGpioHigh(this->D | this->E | this->F);
    }

    std::uint8_t ADS1148::Ads1148::readRegister(const std::uint8_t& reg, std::uint8_t registersToRead) const
    {
        /* Send the Read Register command and select register to read */
        this->sendByte((this->commandReadRegister<<4) | (reg)); // 0bccccrrrr ( c = command and r = register )
        /* Select number of registers to write (fall-through) */
        this->sendByte(registersToRead -1);
        /* Send NOP (no operation 0xFF) so device clocks out data */
        return this->receiveByte();
    }

    void ADS1148::Ads1148::writeRegister(const std::uint8_t& reg, const std::uint8_t& data, std::uint8_t registersToWrite) const
    {
        /* Send the Write Register command and select register to write to */
        this->sendByte((this->commandWriteRegister<<4) | (reg)); // 0bccccrrrr ( c = command and r = register )
        /* Select number of registers to write (fall-through) */
        this->sendByte(registersToWrite -1);  // (p40 0b0000nnnn where n = number of bytes to write -1)
        /* Write data to the register */
        this->sendByte(data);
    }

    void ADS1148::Ads1148::select() const
    {
        /* ADS1148 requires different clock polarity as MCP2517FD */
        SPI::SpiDevice::setClockPhase();
        /* Chip-select must be high */
        SPI::SpiDevice::select();
        /* startPin must be high, else device is in sleep-mode  and won't respond*/
         this->startPin.setHigh();
    }

    void ADS1148::Ads1148::deselect() const
    {
        SPI::SpiDevice::deselect();
        this->startPin.setState(PIN::LOW);
    }

    void ADS1148::Ads1148::startConversion() const
    {
        this->startPin.setHigh();
    }

    int16_t ADS1148::Ads1148::getConversion() const
    {
        /* Send RDATA command: read most recent conversion */
        this->sendByte(this->commandReadData);
        while(this->drdyPin.getState()){}; // Wait till drdy is LOW
        uint8_t highByte = this->receiveByte();
        uint8_t lowByte = this->receiveByte();
        return ((highByte<<8) | (lowByte<<0));
    }

    int16_t ADS1148::Ads1148::convertAdcOutputToResistance(int16_t& adcOut)
    {
         int32_t resistance = static_cast<int32_t>(adcOut);
         resistance = (resistance * this->rRef * 100)>>(this->gainFactor); // Adcout * Rref / 2^17 (100 = double digit precision)
         return resistance;
    }

}
