/* pin.hpp */
/* Nick van den Berg - Hogeschool Rotterdam - IHC Systems B.V.
 * Made for MSP430G2452 microcontroller
 * Used documents:
 * msp430x2xx family user guide p??-p??
 * msp430g2452 datasheet p??-p??                                 */

#ifndef TEMPSENSOR_SRC_PIN_H_
#define TEMPSENSOR_SRC_PIN_H_

#include <cstdint>

#include "msp430.h"


namespace PIN{
    /* Datatypes */
    /* pinState, pinMode and pin redefined so pin.hpp does not depend on types.hpp */
    enum pinState : const uint8_t
    {
        LOW = 0x0, HIGH = 0x1, OFF = 0x0, ON = 0x1};

    enum pinMode : const uint8_t
    {
        INPUT = 0x0, OUTPUT = 0x1
    };

    enum pin : const uint8_t
    {
        PIN_0 = 0x01, PIN_1 = 0x02, PIN_2 = 0x04, PIN_3 = 0x08,
        PIN_4 = 0x10, PIN_5 = 0x20, PIN_6 = 0x40, PIN_7 = 0x80
    };

    enum port : const uint8_t
    {
      PORT_A = 0x0, PORT_B = 0x1
    };

    struct PortA
    {
        /* control registers */
        static volatile constexpr uint8_t* input                   = &P1IN;
        static volatile constexpr uint8_t* output                  = &P1OUT;
        static volatile constexpr uint8_t* direction               = &P1DIR;
        static volatile constexpr uint8_t* interruptFlag           = &P1IFG;
        static volatile constexpr uint8_t* interruptEdgeSelect     = &P1IES;
        static volatile constexpr uint8_t* interruptEnable         = &P1IE;
        static volatile constexpr uint8_t* select                  = &P1SEL;
        static volatile constexpr uint8_t* select2                 = &P1SEL2;
        static volatile constexpr uint8_t* resistorEnable          = &P1REN;
    };

    struct PortB
    {
        /* control registers */
        static volatile constexpr uint8_t* input                   = &P2IN;
        static volatile constexpr uint8_t* output                  = &P2OUT;
        static volatile constexpr uint8_t* direction               = &P2DIR;
        static volatile constexpr uint8_t* interruptFlag           = &P2IFG;
        static volatile constexpr uint8_t* interruptEdgeSelect     = &P2IES;
        static volatile constexpr uint8_t* interruptEnable         = &P2IE;
        static volatile constexpr uint8_t* select                  = &P2SEL;
        static volatile constexpr uint8_t* select2                 = &P2SEL2;
        static volatile constexpr uint8_t* resistorEnable          = &P2REN;
    };



    /* Prototypes */
    // None

    /* Classes */
    class Pin{
    public:
        /* Constructors */
        Pin(const uint8_t& p, const uint8_t& pp);
        /* General */
        void setDir(uint8_t state) const;
        void setSel(uint8_t state) const;
        void setSel2(uint8_t state) const;
        static void resetPorts();
        /* Gpio */
        void setGpio() const;
        void setState(uint8_t state) const;
        uint8_t getState() const;
        /* SPI */
        void setSdo() const;
        void setSdi() const;
        void setSclk() const;
        /* I2C */
        void setScl() const;
        void setSda() const;
    private:
        static PortA portA;
        static PortB portB;
        const uint8_t port;
        const uint8_t pin;
    };
}


#endif // TEMPSENSOR_SRC_PIN_H_
