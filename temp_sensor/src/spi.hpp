/* Nick van den Berg - IHC Systems B.V.
 * Made for MSP430G2452 microcontroller
 * Used documents:
 * msp430x2xx family user guide p396-
 * msp430g2452 datasheet p37-p48
 * */

#ifndef TEMPSENSOR_SRC_SPI_HPP_
#define TEMPSENSOR_SRC_SPI_HPP_

#include <cstdint>

#include "msp430.h"

#include "pin.hpp"
#include "gpio.hpp"
#include "usi.hpp"


namespace SPI{
    /* Datatypes */
    struct SpiSettings
    {
        // Custom constructor
        SpiSettings( const std::uint8_t& masterSlave,  const std::uint8_t& clockSource, const std::uint8_t& clockDivider,
                     const std::uint8_t& bit8Bit16, const std::uint8_t& msbLsb, const std::uint8_t& inactiveLowHigh,
                     const std::uint8_t& phaseMode, const std::uint8_t& latchMode):
                         mode(masterSlave), clock(clockSource), clockDiv(clockDivider),
                         bitMode(bit8Bit16), firstBit(msbLsb), clockPolarity(inactiveLowHigh),
                         phaseMode(phaseMode), latchMode(latchMode){ };
        SpiSettings(): mode(USI::MASTER),    // Use default settings
                       clock(USI::SMCLK),
                       clockDiv(USI::DIV_1),
                       bitMode(USI::BIT_MODE_8),
                       firstBit(USI::MSB),
                       clockPolarity(USI::INACTIVE_LOW),
                       phaseMode(USI::PHASE_MODE_0),
                       latchMode(USI::LATCH_MODE_0){};
        /* SPI control settings */
        const std::uint8_t mode;
        const std::uint8_t clock;
        const std::uint8_t clockDiv;
        const std::uint8_t bitMode;
        const std::uint8_t firstBit;
        const std::uint8_t clockPolarity;
        const std::uint8_t phaseMode;
        const std::uint8_t latchMode;
    };
    /* Classes */
    class SpiDevice
    {
    public:
        /* Constructors */
        SpiDevice(const uint8_t& ppSclk, const std::uint8_t& pSclk,
                  const uint8_t& ppSdo, const std::uint8_t& pSdo,
                  const uint8_t& ppSdi, const std::uint8_t& pSdi,
                  const GPIO::Gpio& chipSelect, const std::uint8_t& activeState,
                  const SPI::SpiSettings& spiSettings, const std::uint8_t& NOP);
        /* General */
        void applySettings() const;
        void halt() const;
        void release() const;
        void select() const;
        void deselect() const;
        void sendByte(std::uint8_t byte) const;
        void sendTwoBytes(std::uint16_t bytes) const;
        std::uint8_t receiveByte() const;
        //std::uint16_t receiveTwoBytes() const;
        void setClockPhase() const;

    private:
        const USI::Usi& usi;
        const GPIO::Gpio& chipSelectPin;
        const std::uint8_t activeState;
        const SPI::SpiSettings& settings;
        const std::uint8_t noOperation;
    };

    /* Prototypes */
    // None
}
#endif /* TEMPSENSOR_SRC_SPI_HPP_ */
