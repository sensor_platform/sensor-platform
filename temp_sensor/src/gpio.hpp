/* gpio.hpp */
/* Nick van den Berg - Hogeschool Rotterdam - IHC Systems B.V.
 * Made for MSP430G2452 microcontroller
 * Used documents:
 * msp430x2xx family user guide p327-p334
 * msp430g2452 datasheet p37-p48                                 */

#ifndef TEMPSENSOR_SRC_GPIO_H_
#define TEMPSENSOR_SRC_GPIO_H_

#include <cstdint>

#include "pin.hpp"

namespace GPIO{
    /* Datatypes */
    // None

    /* Prototypes */
    // None

    /* Classes */
    class Gpio{
    public:
        /* Constructors */
        Gpio(const uint8_t& p, const std::uint8_t& pp, const std::uint8_t& mode, const std::uint8_t& state);
        /* Methods */
        void setState(const std::uint8_t& state) const;
        std::uint8_t toggle() const;
        void setInput() const;
        void setOutput() const;
        void setHigh() const;
        void setLow() const;
        std::uint8_t getState() const;
    private:
        const PIN::Pin portPin;
    };
}

#endif // TEMPSENSOR_SRC_GPIO_H_
