/* gpio.cpp */
/* Nick van den Berg - Hogeschool Rotterdam - IHC Systems B.V.
 * Made for MSP430G2452 microcontroller
 * Used documents:
 * msp430x2xx family user guide p327-p334
 * msp430g2452 datasheet p37-p48                                 */

#include "gpio.hpp"

namespace GPIO
{
    GPIO::Gpio::Gpio(const uint8_t& p, const uint8_t& pp,
                     const uint8_t& mode, const uint8_t& state): portPin(PIN::Pin(p, pp))
    {
       this->portPin.setGpio();
       switch(mode)
       {
       case PIN::INPUT:
           this->setInput();
           break;
       case PIN::OUTPUT:
           this->setOutput();
           break;
       }
       this->setState(state);
    }

    void GPIO::Gpio::setState(const uint8_t& state) const
    {
        this->portPin.setState(state);
    }

    uint8_t GPIO::Gpio::toggle() const
    {
        uint8_t state = this->portPin.getState();
        switch(state){
        case PIN::LOW:
            this->setHigh();
            break;
        case PIN::HIGH:
            this->setLow();
            break;
        }
        return state;
    }

    void GPIO::Gpio::setInput() const
    {
        this->portPin.setDir(PIN::INPUT);
    }

    void GPIO::Gpio::setOutput() const
    {
        this->portPin.setDir(PIN::OUTPUT);
    }

    void GPIO::Gpio::setHigh() const
    {
        this->portPin.setState(PIN::HIGH);
    }

    void GPIO::Gpio::setLow() const
    {
        this->portPin.setState(PIN::LOW);
    }

    uint8_t GPIO::Gpio::getState() const
    {
        return this->portPin.getState();
    }
}

