# Sensor Platform for industrial electric motors

Project code for IHC sensor platform for electric motors to replace phoenix contact MINI MCR-2-RTD-UI-PT. The platform
communicates with Allen Bradley PLCs via Ethernet UDP/IP. Temperature, vibration and humidity sensors are connected with
the communications platform via CAN-FD. The project is written in C and C++; a PC is used to simulate the PLC using PYTHON 3.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

To develop or deploy this project the following software is required (only tested with these versions):

```
Code Composer Studio V10.2.0
MSP430Ware V3.80.13.03
Mbed Studio V1.3.1
mbed-os V6.9.0
```

### Installing

T.B.D.


## Built With

* [armMbed](https://os.mbed.com/mbed-os/) - C++ development platform and rtos for ARM devices
* [MSP430Ware](https://www.ti.com/tool/MSPWARE#overview) - Software development kit for MSP430 devices

## Authors

* **Nick van den Berg** - *Initial work* - [0927585_berg_nick](https://gitlab.com/0927585_berg_nick)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* ReadMe from [PurpleBooth](https://gist.github.com/PurpleBooth/109311bb0361f32d87a2#file-readme-template-md)
* Texas Instruments example projects
* [armMbed](https://os.mbed.com/) development team
* Colleagues, friends and family