# Central Sensor Processing Unit GUI - Nick van den Berg - 12-06-2021
# Command to generate new python class from ui file: "pyside2-uic sensor_gui.ui -o sensor_gui.py"

# label red #ff0000
# label green #00aa00

# Python standard modules
import sys
import time

# PyQt modules
from PySide2.QtWidgets import QApplication, QMainWindow, QLabel
from PySide2.QtCore import QTimer, QThread, QObject, Signal
from PySide2.QtGui import QIcon

# Custom modules
from qtdesigner.sensor_gui import Ui_MainWindow
from server import UdpServer
from client import UdpClient
from led import Led

# Global vars
ui_file_name = "./qtdesigner/sensor_gui.ui"


# Main Window
class MainWindow(QMainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()

        # Starting time of application for timestamps
        self.start_time = time.time()

        # Load main window from compiled qtdesigner .ui file
        #  UiLoader().loadUi(ui_file_name, self)
        self.ui = Ui_MainWindow()
        # Run ui initialization (create widgets)
        self.ui.setupUi(self)
        # Start in maximized mode (After setup!)
        self.showMaximized()

        # Add window title
        self.setWindowTitle("CSPU sensor visualisation")
        # Add window icon
        self.setWindowIcon(QIcon("./img/icon.png"))

        # Create client object so we can send messages
        self.client = UdpClient()

        # self.ui.pushButton.clicked.connect(lambda: self.draw())

        # Create timer object for updating graph
        self.graph_update_timer = QTimer(self)
        # Add action to timer
        self.graph_update_timer.timeout.connect(lambda: self.update_gui())
        # Update the timer every second
        self.graph_update_timer.start(1000)

        # Create list for sensor data
        self.x_range = 100
        self.sensor_data = []
        # 16 sensors
        for i in range(0, 16):
            self.sensor_data.append({'value': [], 'time': []})

        # Create 16 sub-plots (4 sensors per row)
        sensor_amount = 16
        self.plots = []
        for i in range(sensor_amount):
            # Create a graph
            plot_title = f'Temperature sensor {i+1}'
            plot = self.ui.graphicsView.addPlot(title=plot_title)
            self.ui.graphicsView.setAntialiasing(False)
            # Initialize the graph
            plot.setLabel('left', 'Temp', units='°C')
            plot.setLabel('bottom', 'Time', units='s')
            plot.enableAutoRange('y', False)
            plot.setRange(xRange=(0, self.x_range), yRange=(-10, 210), padding=0.05, disableAutoRange=True)
            # When plotting an empty list a plotDataItem is created, add this to the plot container
            self.plots.append(plot)
            # New row after 4 sensors
            if (i+1) % 4 == 0:
                self.ui.graphicsView.nextRow()

        # Start a separate thread for receiving the data over ethernet
        self.udp_thread = QThread()
        self.udp_worker = UdpServerWorker()
        self.udp_worker.moveToThread(self.udp_thread)
        self.udp_thread.started.connect(self.udp_worker.run)
        self.udp_worker.finished.connect(self.udp_thread.quit)
        self.udp_worker.finished.connect(self.udp_worker.deleteLater)
        self.udp_thread.finished.connect(self.udp_thread.deleteLater)
        self.udp_worker.message_signal.connect(self.add_message_to_list)
        self.udp_thread.start()

        # Set limit to log size (GUI becomes laggy with infinite logsize)
        self.max_log_size = 2000

        # Keep track of messages as they come in
        self.message_time = []

        # Send UDP packets on mode change
        self.ui.mode_selection.currentIndexChanged.connect(self.change_sensor_mode)

    def change_sensor_mode(self):
        mode = self.ui.mode_selection.currentText()
        if mode == "Temperature":
            self.client.mode_normal("Temperature")
            pass
        elif mode == "Resistance":
            self.client.mode_resistance("Platform")
            pass
        elif mode == "None":
            self.client.mode_none("Temperature")

    def print_to_log(self, string):
        log_text = self.ui.label_3.text()
        if len(log_text) >= self.max_log_size:
            log_text = self.remove_last_line(log_text)
            # log_text = self.remove_first_line(log_text)
        log_text = string + '\n' + log_text
        # log_text = log_text + string + '\n'
        self.ui.label_3.setText(log_text)
        # self.ui.scrollArea.verticalScrollBar().setValue(self.ui.scrollArea.verticalScrollBar().maximum())

    @staticmethod
    def remove_last_line(string):
        string = string[:-1]
        index = string.rfind('\n') + 1
        string = string[:index]
        return string

    @staticmethod
    def remove_first_line(string):
        index = string.find('\n') + 1
        string = string[index:]
        return string

    def add_message_to_list(self, message):
        # message = self.server.receive()
        if len(self.message_time) >= 8:
            self.message_time.pop(0)
        self.message_time.append(time.time())
        if message is not None:
            receive_time = time.time() - self.start_time
            address = message[1]
            self.print_to_log(f"Received {len(message[0])} bytes from '{address[0]}:{address[1]}' @{receive_time}")
            sensors_per_message = 2
            for i in range(sensors_per_message):
                # Disassemble the message
                data = int.from_bytes(message[0][0 + (i*4): 2 + (i*4)], signed=True, byteorder='little')
                data = data / 100  # Data is sent as integer with 2 decimals precision
                number = message[0][2 + (i*4)]
                short_or_open_circuit = chr(message[0][3 + (i*4)])

                # Check for shorts
                if short_or_open_circuit == 'O':
                    self.print_to_log(f"Sensor No#{number + 1} is open-circuit!")
                    self.set_open_circuit(number)
                elif short_or_open_circuit == 'S':
                    self.print_to_log(f"Sensor No#{number + 1} is shorted!")
                    self.set_short_circuit(number)
                else:
                    self.print_to_log(f"Sensor No#{number + 1} has value {data}")
                    self.set_normal(number)

                # Calculate sample frequency
                self.set_sample_frequency(number)

                # If samples are out of view range, remove values before adding new ones (+10 sample scroll-buffer)
                if self.sensor_data[number]['time'] and self.sensor_data[number]['time'][-1] >= self.x_range+10:
                    self.sensor_data[number]['value'].pop(0)
                    self.sensor_data[number]['time'].pop(0)

                # Add new values
                self.sensor_data[number]['value'].append(data)
                self.sensor_data[number]['time'].append(receive_time)

    def update_graph(self):
        # For every plot
        for index, plot in enumerate(self.plots):
            # If the list is not empty and the end of the screen is reached scroll one sample to the right
            if self.sensor_data[index]['time'] and self.sensor_data[index]['time'][-1] >= self.x_range:
                plot.setRange(xRange=(self.sensor_data[index]['time'][10], self.sensor_data[index]['time'][-1]))
            # Plot the data, clear samples that are no longer viewable
            plot.plot(self.sensor_data[index]['time'][-self.x_range:],
                      self.sensor_data[index]['value'][-self.x_range:], clear=True)

    def alive_check(self):
        self.client.alive_check()

    def update_gui(self):
        self.alive_check()
        self.set_message_time()
        self.update_graph()

    def set_open_circuit(self, sensor_number):
        oc_led = self.findChild(Led, "oc_led_" + str(sensor_number+1))
        sc_led = self.findChild(Led, "sc_led_" + str(sensor_number+1))
        value = self.findChild(QLabel, "value_" + str(sensor_number+1))
        if oc_led:
            oc_led.on()
        if sc_led:
            sc_led.off()
        if self.sensor_data[sensor_number]['value'] and value:
            value.setText(str(self.sensor_data[sensor_number]['value'][-1]))

    def set_short_circuit(self, sensor_number):
        oc_led = self.findChild(Led, "oc_led_" + str(sensor_number+1))
        sc_led = self.findChild(Led, "sc_led_" + str(sensor_number+1))
        value = self.findChild(QLabel, "value_" + str(sensor_number+1))
        if oc_led:
            oc_led.off()
        if sc_led:
            sc_led.on()
        if self.sensor_data[sensor_number]['value'] and value:
            value.setText(str(self.sensor_data[sensor_number]['value'][-1]))

    def set_normal(self, sensor_number):
        oc_led = self.findChild(Led, "oc_led_" + str(sensor_number+1))
        sc_led = self.findChild(Led, "sc_led_" + str(sensor_number+1))
        value = self.findChild(QLabel, "value_" + str(sensor_number+1))
        if oc_led:
            oc_led.off()
        if sc_led:
            sc_led.off()
        if self.sensor_data[sensor_number]['value'] and value:
            value.setText(str(self.sensor_data[sensor_number]['value'][-1]))

    def set_sample_frequency(self, sensor_number):
        sample_freq = self.findChild(QLabel, "sample_freq_" + str(sensor_number+1))
        sensor_time = self.sensor_data[sensor_number]['time'][-16:]
        message_amount = len(sensor_time)
        delta_time = 0
        if message_amount >= 2 and sample_freq:
            for i in range(0, message_amount-1, 1):
                delta_time = delta_time + sensor_time[i+1] - sensor_time[i]
            average_time = delta_time / (message_amount - 1)  # Always 1 less difference than sample-amount
            average_sample_freq = round(1/average_time, 3)
            sample_freq.setText(str(average_sample_freq) + " Hz")

    def set_message_time(self):
        message_amount = len(self.message_time)
        delta_time = 0
        if message_amount >= 2:
            for i in range(0, message_amount-1, 1):
                delta_time = delta_time + (self.message_time[i+1] - self.message_time[i])
            average_time = round(delta_time / (message_amount-1), 3)  # -1 cause we only have 7 differences
            self.ui.message_time.setText(str(average_time) + " s")

    def closeEvent(self, event):
        self.udp_worker.stop()
        self.udp_thread.quit()
        self.udp_thread.wait()
        event.accept()


# Worker thread (blocking udp server)
class UdpServerWorker(QObject):
    finished = Signal()
    message_signal = Signal(object)

    def __init__(self):
        super(UdpServerWorker, self).__init__()
        # Create a server object
        self.server = UdpServer()
        self.running = True

    def run(self):
        while self.running:
            message = self.server.receive()
            if message:
                self.message_signal.emit(message)
        self.finished.emit()

    def stop(self):
        self.running = False


if __name__ == "__main__":
    app = QApplication(sys.argv)

    window = MainWindow()
    window.show()

    sys.exit(app.exec_())
