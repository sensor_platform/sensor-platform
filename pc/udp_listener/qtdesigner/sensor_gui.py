# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'sensor_gui.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *

from pyqtgraph import GraphicsLayoutWidget
from led import Led


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        if not MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        MainWindow.resize(1602, 899)
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        self.gridLayout_3 = QGridLayout(self.centralwidget)
        self.gridLayout_3.setObjectName(u"gridLayout_3")
        self.verticalSpacer_3 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.gridLayout_3.addItem(self.verticalSpacer_3, 8, 0, 1, 1)

        self.groupBox_2 = QGroupBox(self.centralwidget)
        self.groupBox_2.setObjectName(u"groupBox_2")
        self.gridLayout_4 = QGridLayout(self.groupBox_2)
        self.gridLayout_4.setObjectName(u"gridLayout_4")
        self.graphicsView = GraphicsLayoutWidget(self.groupBox_2)
        self.graphicsView.setObjectName(u"graphicsView")
        self.graphicsView.setStyleSheet(u"background-color: rgb(0, 0, 0);")

        self.gridLayout_4.addWidget(self.graphicsView, 0, 1, 1, 1)


        self.gridLayout_3.addWidget(self.groupBox_2, 6, 1, 3, 5)

        self.groupBox_3 = QGroupBox(self.centralwidget)
        self.groupBox_3.setObjectName(u"groupBox_3")
        self.groupBox_3.setMinimumSize(QSize(0, 150))
        self.groupBox_3.setMaximumSize(QSize(16777215, 16777215))
        font = QFont()
        font.setPointSize(12)
        self.groupBox_3.setFont(font)
        self.gridLayout_6 = QGridLayout(self.groupBox_3)
        self.gridLayout_6.setObjectName(u"gridLayout_6")
        self.lineEdit = QLineEdit(self.groupBox_3)
        self.lineEdit.setObjectName(u"lineEdit")
        font1 = QFont()
        font1.setPointSize(8)
        self.lineEdit.setFont(font1)

        self.gridLayout_6.addWidget(self.lineEdit, 1, 1, 1, 1)

        self.pushButton = QPushButton(self.groupBox_3)
        self.pushButton.setObjectName(u"pushButton")
        font2 = QFont()
        font2.setPointSize(8)
        font2.setBold(True)
        font2.setWeight(75)
        self.pushButton.setFont(font2)

        self.gridLayout_6.addWidget(self.pushButton, 1, 2, 1, 1)

        self.scrollArea = QScrollArea(self.groupBox_3)
        self.scrollArea.setObjectName(u"scrollArea")
        self.scrollArea.setStyleSheet(u"background-color: rgb(255, 255, 255);")
        self.scrollArea.setWidgetResizable(True)
        self.scrollAreaWidgetContents = QWidget()
        self.scrollAreaWidgetContents.setObjectName(u"scrollAreaWidgetContents")
        self.scrollAreaWidgetContents.setGeometry(QRect(0, 0, 483, 140))
        self.gridLayout_5 = QGridLayout(self.scrollAreaWidgetContents)
        self.gridLayout_5.setObjectName(u"gridLayout_5")
        self.label_3 = QLabel(self.scrollAreaWidgetContents)
        self.label_3.setObjectName(u"label_3")
        self.label_3.setFont(font1)
        self.label_3.setStyleSheet(u"background-color: rgb(255, 255, 255);")
        self.label_3.setAlignment(Qt.AlignLeading|Qt.AlignLeft|Qt.AlignTop)

        self.gridLayout_5.addWidget(self.label_3, 0, 0, 2, 2)

        self.scrollArea.setWidget(self.scrollAreaWidgetContents)

        self.gridLayout_6.addWidget(self.scrollArea, 0, 1, 1, 2)


        self.gridLayout_3.addWidget(self.groupBox_3, 4, 5, 2, 1)

        self.groupBox = QGroupBox(self.centralwidget)
        self.groupBox.setObjectName(u"groupBox")
        font3 = QFont()
        font3.setPointSize(11)
        self.groupBox.setFont(font3)
        self.gridLayout_2 = QGridLayout(self.groupBox)
        self.gridLayout_2.setObjectName(u"gridLayout_2")
        self.gridLayout = QGridLayout()
        self.gridLayout.setObjectName(u"gridLayout")
        self.label_7 = QLabel(self.groupBox)
        self.label_7.setObjectName(u"label_7")
        self.label_7.setFont(font2)

        self.gridLayout.addWidget(self.label_7, 0, 1, 1, 1)

        self.message_time = QLabel(self.groupBox)
        self.message_time.setObjectName(u"message_time")
        self.message_time.setFont(font1)

        self.gridLayout.addWidget(self.message_time, 2, 1, 1, 1)

        self.label_43 = QLabel(self.groupBox)
        self.label_43.setObjectName(u"label_43")
        self.label_43.setFont(font2)

        self.gridLayout.addWidget(self.label_43, 0, 0, 1, 1)

        self.status = QLabel(self.groupBox)
        self.status.setObjectName(u"status")
        font4 = QFont()
        font4.setFamily(u"MS Shell Dlg 2")
        font4.setPointSize(12)
        font4.setBold(False)
        font4.setItalic(False)
        font4.setWeight(9)
        self.status.setFont(font4)
        self.status.setStyleSheet(u"color: rgb(0, 170, 0);\n"
"font: 75 12pt \"MS Shell Dlg 2\";")

        self.gridLayout.addWidget(self.status, 2, 0, 1, 1)

        self.label_2 = QLabel(self.groupBox)
        self.label_2.setObjectName(u"label_2")
        self.label_2.setFont(font2)

        self.gridLayout.addWidget(self.label_2, 0, 2, 1, 1)

        self.mode_selection = QComboBox(self.groupBox)
        self.mode_selection.addItem("")
        self.mode_selection.addItem("")
        self.mode_selection.addItem("")
        self.mode_selection.setObjectName(u"mode_selection")
        self.mode_selection.setFont(font1)

        self.gridLayout.addWidget(self.mode_selection, 2, 2, 1, 1)


        self.gridLayout_2.addLayout(self.gridLayout, 0, 1, 1, 1)


        self.gridLayout_3.addWidget(self.groupBox, 2, 5, 1, 1)

        self.verticalSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.gridLayout_3.addItem(self.verticalSpacer, 5, 4, 1, 1)

        self.horizontalSpacer_3 = QSpacerItem(4, 4, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.gridLayout_3.addItem(self.horizontalSpacer_3, 1, 2, 1, 1)

        self.verticalSpacer_2 = QSpacerItem(17, 319, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.gridLayout_3.addItem(self.verticalSpacer_2, 6, 0, 2, 1)

        self.horizontalSpacer = QSpacerItem(40, 0, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.gridLayout_3.addItem(self.horizontalSpacer, 9, 1, 1, 5)

        self.horizontalSpacer_2 = QSpacerItem(4, 4, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.gridLayout_3.addItem(self.horizontalSpacer_2, 1, 3, 1, 1)

        self.groupBox_4 = QGroupBox(self.centralwidget)
        self.groupBox_4.setObjectName(u"groupBox_4")
        self.groupBox_4.setFont(font)
        self.gridLayout_23 = QGridLayout(self.groupBox_4)
        self.gridLayout_23.setObjectName(u"gridLayout_23")
        self.groupBox_5 = QGroupBox(self.groupBox_4)
        self.groupBox_5.setObjectName(u"groupBox_5")
        self.groupBox_5.setFont(font2)
        self.gridLayout_7 = QGridLayout(self.groupBox_5)
        self.gridLayout_7.setObjectName(u"gridLayout_7")
        self.label_5 = QLabel(self.groupBox_5)
        self.label_5.setObjectName(u"label_5")
        font5 = QFont()
        font5.setBold(False)
        font5.setWeight(50)
        self.label_5.setFont(font5)
        self.label_5.setAlignment(Qt.AlignCenter)

        self.gridLayout_7.addWidget(self.label_5, 0, 0, 1, 1)

        self.label_6 = QLabel(self.groupBox_5)
        self.label_6.setObjectName(u"label_6")
        self.label_6.setFont(font5)
        self.label_6.setAlignment(Qt.AlignCenter)

        self.gridLayout_7.addWidget(self.label_6, 1, 0, 1, 1)

        self.label = QLabel(self.groupBox_5)
        self.label.setObjectName(u"label")
        self.label.setFont(font5)
        self.label.setAlignment(Qt.AlignCenter)

        self.gridLayout_7.addWidget(self.label, 2, 0, 1, 1)

        self.oc_led_1 = Led(self.groupBox_5)
        self.oc_led_1.setObjectName(u"oc_led_1")
        self.oc_led_1.setFont(font5)

        self.gridLayout_7.addWidget(self.oc_led_1, 2, 1, 1, 1)

        self.label_4 = QLabel(self.groupBox_5)
        self.label_4.setObjectName(u"label_4")
        self.label_4.setFont(font5)
        self.label_4.setAlignment(Qt.AlignCenter)

        self.gridLayout_7.addWidget(self.label_4, 3, 0, 1, 1)

        self.sc_led_1 = Led(self.groupBox_5)
        self.sc_led_1.setObjectName(u"sc_led_1")
        self.sc_led_1.setFont(font5)

        self.gridLayout_7.addWidget(self.sc_led_1, 3, 1, 1, 1)

        self.sample_freq_1 = QLabel(self.groupBox_5)
        self.sample_freq_1.setObjectName(u"sample_freq_1")
        self.sample_freq_1.setFont(font5)

        self.gridLayout_7.addWidget(self.sample_freq_1, 0, 1, 1, 1)

        self.value_1 = QLabel(self.groupBox_5)
        self.value_1.setObjectName(u"value_1")
        self.value_1.setFont(font5)

        self.gridLayout_7.addWidget(self.value_1, 1, 1, 1, 1)


        self.gridLayout_23.addWidget(self.groupBox_5, 0, 0, 1, 1)

        self.groupBox_6 = QGroupBox(self.groupBox_4)
        self.groupBox_6.setObjectName(u"groupBox_6")
        self.groupBox_6.setFont(font2)
        self.gridLayout_8 = QGridLayout(self.groupBox_6)
        self.gridLayout_8.setObjectName(u"gridLayout_8")
        self.label_9 = QLabel(self.groupBox_6)
        self.label_9.setObjectName(u"label_9")
        self.label_9.setFont(font5)
        self.label_9.setAlignment(Qt.AlignCenter)

        self.gridLayout_8.addWidget(self.label_9, 0, 0, 1, 1)

        self.label_10 = QLabel(self.groupBox_6)
        self.label_10.setObjectName(u"label_10")
        self.label_10.setFont(font5)
        self.label_10.setAlignment(Qt.AlignCenter)

        self.gridLayout_8.addWidget(self.label_10, 1, 0, 1, 1)

        self.label_11 = QLabel(self.groupBox_6)
        self.label_11.setObjectName(u"label_11")
        self.label_11.setFont(font5)
        self.label_11.setAlignment(Qt.AlignCenter)

        self.gridLayout_8.addWidget(self.label_11, 2, 0, 1, 1)

        self.oc_led_2 = Led(self.groupBox_6)
        self.oc_led_2.setObjectName(u"oc_led_2")
        self.oc_led_2.setFont(font5)

        self.gridLayout_8.addWidget(self.oc_led_2, 2, 1, 1, 1)

        self.label_12 = QLabel(self.groupBox_6)
        self.label_12.setObjectName(u"label_12")
        self.label_12.setFont(font5)
        self.label_12.setAlignment(Qt.AlignCenter)

        self.gridLayout_8.addWidget(self.label_12, 3, 0, 1, 1)

        self.sc_led_2 = Led(self.groupBox_6)
        self.sc_led_2.setObjectName(u"sc_led_2")
        self.sc_led_2.setFont(font5)

        self.gridLayout_8.addWidget(self.sc_led_2, 3, 1, 1, 1)

        self.sample_freq_2 = QLabel(self.groupBox_6)
        self.sample_freq_2.setObjectName(u"sample_freq_2")
        self.sample_freq_2.setFont(font5)

        self.gridLayout_8.addWidget(self.sample_freq_2, 0, 1, 1, 1)

        self.value_2 = QLabel(self.groupBox_6)
        self.value_2.setObjectName(u"value_2")
        self.value_2.setFont(font5)

        self.gridLayout_8.addWidget(self.value_2, 1, 1, 1, 1)


        self.gridLayout_23.addWidget(self.groupBox_6, 0, 1, 1, 1)

        self.groupBox_7 = QGroupBox(self.groupBox_4)
        self.groupBox_7.setObjectName(u"groupBox_7")
        self.groupBox_7.setFont(font2)
        self.gridLayout_9 = QGridLayout(self.groupBox_7)
        self.gridLayout_9.setObjectName(u"gridLayout_9")
        self.label_13 = QLabel(self.groupBox_7)
        self.label_13.setObjectName(u"label_13")
        self.label_13.setFont(font5)
        self.label_13.setAlignment(Qt.AlignCenter)

        self.gridLayout_9.addWidget(self.label_13, 0, 0, 1, 1)

        self.label_14 = QLabel(self.groupBox_7)
        self.label_14.setObjectName(u"label_14")
        self.label_14.setFont(font5)
        self.label_14.setAlignment(Qt.AlignCenter)

        self.gridLayout_9.addWidget(self.label_14, 1, 0, 1, 1)

        self.label_15 = QLabel(self.groupBox_7)
        self.label_15.setObjectName(u"label_15")
        self.label_15.setFont(font5)
        self.label_15.setAlignment(Qt.AlignCenter)

        self.gridLayout_9.addWidget(self.label_15, 2, 0, 1, 1)

        self.oc_led_3 = Led(self.groupBox_7)
        self.oc_led_3.setObjectName(u"oc_led_3")
        self.oc_led_3.setFont(font5)

        self.gridLayout_9.addWidget(self.oc_led_3, 2, 1, 1, 1)

        self.label_16 = QLabel(self.groupBox_7)
        self.label_16.setObjectName(u"label_16")
        self.label_16.setFont(font5)
        self.label_16.setAlignment(Qt.AlignCenter)

        self.gridLayout_9.addWidget(self.label_16, 3, 0, 1, 1)

        self.sc_led_3 = Led(self.groupBox_7)
        self.sc_led_3.setObjectName(u"sc_led_3")
        self.sc_led_3.setFont(font5)

        self.gridLayout_9.addWidget(self.sc_led_3, 3, 1, 1, 1)

        self.sample_freq_3 = QLabel(self.groupBox_7)
        self.sample_freq_3.setObjectName(u"sample_freq_3")
        self.sample_freq_3.setFont(font5)

        self.gridLayout_9.addWidget(self.sample_freq_3, 0, 1, 1, 1)

        self.value_3 = QLabel(self.groupBox_7)
        self.value_3.setObjectName(u"value_3")
        self.value_3.setFont(font5)

        self.gridLayout_9.addWidget(self.value_3, 1, 1, 1, 1)


        self.gridLayout_23.addWidget(self.groupBox_7, 0, 2, 1, 1)

        self.groupBox_8 = QGroupBox(self.groupBox_4)
        self.groupBox_8.setObjectName(u"groupBox_8")
        self.groupBox_8.setFont(font2)
        self.gridLayout_10 = QGridLayout(self.groupBox_8)
        self.gridLayout_10.setObjectName(u"gridLayout_10")
        self.label_17 = QLabel(self.groupBox_8)
        self.label_17.setObjectName(u"label_17")
        self.label_17.setFont(font5)
        self.label_17.setAlignment(Qt.AlignCenter)

        self.gridLayout_10.addWidget(self.label_17, 0, 0, 1, 1)

        self.label_18 = QLabel(self.groupBox_8)
        self.label_18.setObjectName(u"label_18")
        self.label_18.setFont(font5)
        self.label_18.setAlignment(Qt.AlignCenter)

        self.gridLayout_10.addWidget(self.label_18, 1, 0, 1, 1)

        self.label_19 = QLabel(self.groupBox_8)
        self.label_19.setObjectName(u"label_19")
        self.label_19.setFont(font5)
        self.label_19.setAlignment(Qt.AlignCenter)

        self.gridLayout_10.addWidget(self.label_19, 2, 0, 1, 1)

        self.oc_led_4 = Led(self.groupBox_8)
        self.oc_led_4.setObjectName(u"oc_led_4")
        self.oc_led_4.setFont(font5)

        self.gridLayout_10.addWidget(self.oc_led_4, 2, 1, 1, 1)

        self.label_20 = QLabel(self.groupBox_8)
        self.label_20.setObjectName(u"label_20")
        self.label_20.setFont(font5)
        self.label_20.setAlignment(Qt.AlignCenter)

        self.gridLayout_10.addWidget(self.label_20, 3, 0, 1, 1)

        self.sc_led_4 = Led(self.groupBox_8)
        self.sc_led_4.setObjectName(u"sc_led_4")
        self.sc_led_4.setFont(font5)

        self.gridLayout_10.addWidget(self.sc_led_4, 3, 1, 1, 1)

        self.sample_freq_4 = QLabel(self.groupBox_8)
        self.sample_freq_4.setObjectName(u"sample_freq_4")
        self.sample_freq_4.setFont(font5)

        self.gridLayout_10.addWidget(self.sample_freq_4, 0, 1, 1, 1)

        self.value_4 = QLabel(self.groupBox_8)
        self.value_4.setObjectName(u"value_4")
        self.value_4.setFont(font5)

        self.gridLayout_10.addWidget(self.value_4, 1, 1, 1, 1)


        self.gridLayout_23.addWidget(self.groupBox_8, 0, 3, 1, 1)

        self.groupBox_9 = QGroupBox(self.groupBox_4)
        self.groupBox_9.setObjectName(u"groupBox_9")
        self.groupBox_9.setFont(font2)
        self.gridLayout_11 = QGridLayout(self.groupBox_9)
        self.gridLayout_11.setObjectName(u"gridLayout_11")
        self.label_21 = QLabel(self.groupBox_9)
        self.label_21.setObjectName(u"label_21")
        self.label_21.setFont(font5)
        self.label_21.setAlignment(Qt.AlignCenter)

        self.gridLayout_11.addWidget(self.label_21, 0, 0, 1, 1)

        self.label_22 = QLabel(self.groupBox_9)
        self.label_22.setObjectName(u"label_22")
        self.label_22.setFont(font5)
        self.label_22.setAlignment(Qt.AlignCenter)

        self.gridLayout_11.addWidget(self.label_22, 1, 0, 1, 1)

        self.label_23 = QLabel(self.groupBox_9)
        self.label_23.setObjectName(u"label_23")
        self.label_23.setFont(font5)
        self.label_23.setAlignment(Qt.AlignCenter)

        self.gridLayout_11.addWidget(self.label_23, 2, 0, 1, 1)

        self.oc_led_5 = Led(self.groupBox_9)
        self.oc_led_5.setObjectName(u"oc_led_5")
        self.oc_led_5.setFont(font5)

        self.gridLayout_11.addWidget(self.oc_led_5, 2, 1, 1, 1)

        self.label_24 = QLabel(self.groupBox_9)
        self.label_24.setObjectName(u"label_24")
        self.label_24.setFont(font5)
        self.label_24.setAlignment(Qt.AlignCenter)

        self.gridLayout_11.addWidget(self.label_24, 3, 0, 1, 1)

        self.sc_led_5 = Led(self.groupBox_9)
        self.sc_led_5.setObjectName(u"sc_led_5")
        self.sc_led_5.setFont(font5)

        self.gridLayout_11.addWidget(self.sc_led_5, 3, 1, 1, 1)

        self.sample_freq_5 = QLabel(self.groupBox_9)
        self.sample_freq_5.setObjectName(u"sample_freq_5")
        self.sample_freq_5.setFont(font5)

        self.gridLayout_11.addWidget(self.sample_freq_5, 0, 1, 1, 1)

        self.value_5 = QLabel(self.groupBox_9)
        self.value_5.setObjectName(u"value_5")
        self.value_5.setFont(font5)

        self.gridLayout_11.addWidget(self.value_5, 1, 1, 1, 1)


        self.gridLayout_23.addWidget(self.groupBox_9, 0, 4, 1, 1)

        self.groupBox_10 = QGroupBox(self.groupBox_4)
        self.groupBox_10.setObjectName(u"groupBox_10")
        self.groupBox_10.setFont(font2)
        self.gridLayout_12 = QGridLayout(self.groupBox_10)
        self.gridLayout_12.setObjectName(u"gridLayout_12")
        self.label_25 = QLabel(self.groupBox_10)
        self.label_25.setObjectName(u"label_25")
        self.label_25.setFont(font5)
        self.label_25.setAlignment(Qt.AlignCenter)

        self.gridLayout_12.addWidget(self.label_25, 0, 0, 1, 1)

        self.label_26 = QLabel(self.groupBox_10)
        self.label_26.setObjectName(u"label_26")
        self.label_26.setFont(font5)
        self.label_26.setAlignment(Qt.AlignCenter)

        self.gridLayout_12.addWidget(self.label_26, 1, 0, 1, 1)

        self.label_27 = QLabel(self.groupBox_10)
        self.label_27.setObjectName(u"label_27")
        self.label_27.setFont(font5)
        self.label_27.setAlignment(Qt.AlignCenter)

        self.gridLayout_12.addWidget(self.label_27, 2, 0, 1, 1)

        self.oc_led_6 = Led(self.groupBox_10)
        self.oc_led_6.setObjectName(u"oc_led_6")
        self.oc_led_6.setFont(font5)

        self.gridLayout_12.addWidget(self.oc_led_6, 2, 1, 1, 1)

        self.label_28 = QLabel(self.groupBox_10)
        self.label_28.setObjectName(u"label_28")
        self.label_28.setFont(font5)
        self.label_28.setAlignment(Qt.AlignCenter)

        self.gridLayout_12.addWidget(self.label_28, 3, 0, 1, 1)

        self.sc_led_6 = Led(self.groupBox_10)
        self.sc_led_6.setObjectName(u"sc_led_6")
        self.sc_led_6.setFont(font5)

        self.gridLayout_12.addWidget(self.sc_led_6, 3, 1, 1, 1)

        self.sample_freq_6 = QLabel(self.groupBox_10)
        self.sample_freq_6.setObjectName(u"sample_freq_6")
        self.sample_freq_6.setFont(font5)

        self.gridLayout_12.addWidget(self.sample_freq_6, 0, 1, 1, 1)

        self.value_6 = QLabel(self.groupBox_10)
        self.value_6.setObjectName(u"value_6")
        self.value_6.setFont(font5)

        self.gridLayout_12.addWidget(self.value_6, 1, 1, 1, 1)


        self.gridLayout_23.addWidget(self.groupBox_10, 0, 5, 1, 1)

        self.groupBox_11 = QGroupBox(self.groupBox_4)
        self.groupBox_11.setObjectName(u"groupBox_11")
        self.groupBox_11.setFont(font2)
        self.gridLayout_13 = QGridLayout(self.groupBox_11)
        self.gridLayout_13.setObjectName(u"gridLayout_13")
        self.label_29 = QLabel(self.groupBox_11)
        self.label_29.setObjectName(u"label_29")
        self.label_29.setFont(font5)
        self.label_29.setAlignment(Qt.AlignCenter)

        self.gridLayout_13.addWidget(self.label_29, 0, 0, 1, 1)

        self.label_30 = QLabel(self.groupBox_11)
        self.label_30.setObjectName(u"label_30")
        self.label_30.setFont(font5)
        self.label_30.setAlignment(Qt.AlignCenter)

        self.gridLayout_13.addWidget(self.label_30, 1, 0, 1, 1)

        self.label_31 = QLabel(self.groupBox_11)
        self.label_31.setObjectName(u"label_31")
        self.label_31.setFont(font5)
        self.label_31.setAlignment(Qt.AlignCenter)

        self.gridLayout_13.addWidget(self.label_31, 2, 0, 1, 1)

        self.oc_led_7 = Led(self.groupBox_11)
        self.oc_led_7.setObjectName(u"oc_led_7")
        self.oc_led_7.setFont(font5)

        self.gridLayout_13.addWidget(self.oc_led_7, 2, 1, 1, 1)

        self.label_32 = QLabel(self.groupBox_11)
        self.label_32.setObjectName(u"label_32")
        self.label_32.setFont(font5)
        self.label_32.setAlignment(Qt.AlignCenter)

        self.gridLayout_13.addWidget(self.label_32, 3, 0, 1, 1)

        self.sc_led_7 = Led(self.groupBox_11)
        self.sc_led_7.setObjectName(u"sc_led_7")
        self.sc_led_7.setFont(font5)

        self.gridLayout_13.addWidget(self.sc_led_7, 3, 1, 1, 1)

        self.sample_freq_7 = QLabel(self.groupBox_11)
        self.sample_freq_7.setObjectName(u"sample_freq_7")
        self.sample_freq_7.setFont(font5)

        self.gridLayout_13.addWidget(self.sample_freq_7, 0, 1, 1, 1)

        self.value_7 = QLabel(self.groupBox_11)
        self.value_7.setObjectName(u"value_7")
        self.value_7.setFont(font5)

        self.gridLayout_13.addWidget(self.value_7, 1, 1, 1, 1)


        self.gridLayout_23.addWidget(self.groupBox_11, 0, 6, 1, 1)

        self.groupBox_12 = QGroupBox(self.groupBox_4)
        self.groupBox_12.setObjectName(u"groupBox_12")
        self.groupBox_12.setFont(font2)
        self.gridLayout_14 = QGridLayout(self.groupBox_12)
        self.gridLayout_14.setObjectName(u"gridLayout_14")
        self.label_33 = QLabel(self.groupBox_12)
        self.label_33.setObjectName(u"label_33")
        self.label_33.setFont(font5)
        self.label_33.setAlignment(Qt.AlignCenter)

        self.gridLayout_14.addWidget(self.label_33, 0, 0, 1, 1)

        self.label_34 = QLabel(self.groupBox_12)
        self.label_34.setObjectName(u"label_34")
        self.label_34.setFont(font5)
        self.label_34.setAlignment(Qt.AlignCenter)

        self.gridLayout_14.addWidget(self.label_34, 1, 0, 1, 1)

        self.label_35 = QLabel(self.groupBox_12)
        self.label_35.setObjectName(u"label_35")
        self.label_35.setFont(font5)
        self.label_35.setAlignment(Qt.AlignCenter)

        self.gridLayout_14.addWidget(self.label_35, 2, 0, 1, 1)

        self.oc_led_8 = Led(self.groupBox_12)
        self.oc_led_8.setObjectName(u"oc_led_8")
        self.oc_led_8.setFont(font5)

        self.gridLayout_14.addWidget(self.oc_led_8, 2, 1, 1, 1)

        self.label_36 = QLabel(self.groupBox_12)
        self.label_36.setObjectName(u"label_36")
        self.label_36.setFont(font5)
        self.label_36.setAlignment(Qt.AlignCenter)

        self.gridLayout_14.addWidget(self.label_36, 3, 0, 1, 1)

        self.sc_led_8 = Led(self.groupBox_12)
        self.sc_led_8.setObjectName(u"sc_led_8")
        self.sc_led_8.setFont(font5)

        self.gridLayout_14.addWidget(self.sc_led_8, 3, 1, 1, 1)

        self.sample_freq_8 = QLabel(self.groupBox_12)
        self.sample_freq_8.setObjectName(u"sample_freq_8")
        self.sample_freq_8.setFont(font5)

        self.gridLayout_14.addWidget(self.sample_freq_8, 0, 1, 1, 1)

        self.value_8 = QLabel(self.groupBox_12)
        self.value_8.setObjectName(u"value_8")
        self.value_8.setFont(font5)

        self.gridLayout_14.addWidget(self.value_8, 1, 1, 1, 1)


        self.gridLayout_23.addWidget(self.groupBox_12, 0, 7, 1, 1)

        self.groupBox_13 = QGroupBox(self.groupBox_4)
        self.groupBox_13.setObjectName(u"groupBox_13")
        self.groupBox_13.setFont(font2)
        self.gridLayout_15 = QGridLayout(self.groupBox_13)
        self.gridLayout_15.setObjectName(u"gridLayout_15")
        self.label_37 = QLabel(self.groupBox_13)
        self.label_37.setObjectName(u"label_37")
        self.label_37.setFont(font5)
        self.label_37.setAlignment(Qt.AlignCenter)

        self.gridLayout_15.addWidget(self.label_37, 0, 0, 1, 1)

        self.label_38 = QLabel(self.groupBox_13)
        self.label_38.setObjectName(u"label_38")
        self.label_38.setFont(font5)
        self.label_38.setAlignment(Qt.AlignCenter)

        self.gridLayout_15.addWidget(self.label_38, 1, 0, 1, 1)

        self.label_39 = QLabel(self.groupBox_13)
        self.label_39.setObjectName(u"label_39")
        self.label_39.setFont(font5)
        self.label_39.setAlignment(Qt.AlignCenter)

        self.gridLayout_15.addWidget(self.label_39, 2, 0, 1, 1)

        self.oc_led_9 = Led(self.groupBox_13)
        self.oc_led_9.setObjectName(u"oc_led_9")
        self.oc_led_9.setFont(font5)

        self.gridLayout_15.addWidget(self.oc_led_9, 2, 1, 1, 1)

        self.label_40 = QLabel(self.groupBox_13)
        self.label_40.setObjectName(u"label_40")
        self.label_40.setFont(font5)
        self.label_40.setAlignment(Qt.AlignCenter)

        self.gridLayout_15.addWidget(self.label_40, 3, 0, 1, 1)

        self.sc_led_9 = Led(self.groupBox_13)
        self.sc_led_9.setObjectName(u"sc_led_9")
        self.sc_led_9.setFont(font5)

        self.gridLayout_15.addWidget(self.sc_led_9, 3, 1, 1, 1)

        self.sample_freq_9 = QLabel(self.groupBox_13)
        self.sample_freq_9.setObjectName(u"sample_freq_9")
        self.sample_freq_9.setFont(font5)

        self.gridLayout_15.addWidget(self.sample_freq_9, 0, 1, 1, 1)

        self.value_9 = QLabel(self.groupBox_13)
        self.value_9.setObjectName(u"value_9")
        self.value_9.setFont(font5)

        self.gridLayout_15.addWidget(self.value_9, 1, 1, 1, 1)


        self.gridLayout_23.addWidget(self.groupBox_13, 1, 0, 1, 1)

        self.groupBox_14 = QGroupBox(self.groupBox_4)
        self.groupBox_14.setObjectName(u"groupBox_14")
        self.groupBox_14.setFont(font2)
        self.gridLayout_16 = QGridLayout(self.groupBox_14)
        self.gridLayout_16.setObjectName(u"gridLayout_16")
        self.label_41 = QLabel(self.groupBox_14)
        self.label_41.setObjectName(u"label_41")
        self.label_41.setFont(font5)
        self.label_41.setAlignment(Qt.AlignCenter)

        self.gridLayout_16.addWidget(self.label_41, 0, 0, 1, 1)

        self.label_42 = QLabel(self.groupBox_14)
        self.label_42.setObjectName(u"label_42")
        self.label_42.setFont(font5)
        self.label_42.setAlignment(Qt.AlignCenter)

        self.gridLayout_16.addWidget(self.label_42, 1, 0, 1, 1)

        self.label_45 = QLabel(self.groupBox_14)
        self.label_45.setObjectName(u"label_45")
        self.label_45.setFont(font5)
        self.label_45.setAlignment(Qt.AlignCenter)

        self.gridLayout_16.addWidget(self.label_45, 2, 0, 1, 1)

        self.oc_led_10 = Led(self.groupBox_14)
        self.oc_led_10.setObjectName(u"oc_led_10")
        self.oc_led_10.setFont(font5)

        self.gridLayout_16.addWidget(self.oc_led_10, 2, 1, 1, 1)

        self.label_46 = QLabel(self.groupBox_14)
        self.label_46.setObjectName(u"label_46")
        self.label_46.setFont(font5)
        self.label_46.setAlignment(Qt.AlignCenter)

        self.gridLayout_16.addWidget(self.label_46, 3, 0, 1, 1)

        self.sc_led_10 = Led(self.groupBox_14)
        self.sc_led_10.setObjectName(u"sc_led_10")
        self.sc_led_10.setFont(font5)

        self.gridLayout_16.addWidget(self.sc_led_10, 3, 1, 1, 1)

        self.sample_freq_10 = QLabel(self.groupBox_14)
        self.sample_freq_10.setObjectName(u"sample_freq_10")
        self.sample_freq_10.setFont(font5)

        self.gridLayout_16.addWidget(self.sample_freq_10, 0, 1, 1, 1)

        self.value_10 = QLabel(self.groupBox_14)
        self.value_10.setObjectName(u"value_10")
        self.value_10.setFont(font5)

        self.gridLayout_16.addWidget(self.value_10, 1, 1, 1, 1)


        self.gridLayout_23.addWidget(self.groupBox_14, 1, 1, 1, 1)

        self.groupBox_15 = QGroupBox(self.groupBox_4)
        self.groupBox_15.setObjectName(u"groupBox_15")
        self.groupBox_15.setFont(font2)
        self.gridLayout_17 = QGridLayout(self.groupBox_15)
        self.gridLayout_17.setObjectName(u"gridLayout_17")
        self.label_47 = QLabel(self.groupBox_15)
        self.label_47.setObjectName(u"label_47")
        self.label_47.setFont(font5)
        self.label_47.setAlignment(Qt.AlignCenter)

        self.gridLayout_17.addWidget(self.label_47, 0, 0, 1, 1)

        self.label_48 = QLabel(self.groupBox_15)
        self.label_48.setObjectName(u"label_48")
        self.label_48.setFont(font5)
        self.label_48.setAlignment(Qt.AlignCenter)

        self.gridLayout_17.addWidget(self.label_48, 1, 0, 1, 1)

        self.label_49 = QLabel(self.groupBox_15)
        self.label_49.setObjectName(u"label_49")
        self.label_49.setFont(font5)
        self.label_49.setAlignment(Qt.AlignCenter)

        self.gridLayout_17.addWidget(self.label_49, 2, 0, 1, 1)

        self.oc_led_11 = Led(self.groupBox_15)
        self.oc_led_11.setObjectName(u"oc_led_11")
        self.oc_led_11.setFont(font5)

        self.gridLayout_17.addWidget(self.oc_led_11, 2, 1, 1, 1)

        self.label_50 = QLabel(self.groupBox_15)
        self.label_50.setObjectName(u"label_50")
        self.label_50.setFont(font5)
        self.label_50.setAlignment(Qt.AlignCenter)

        self.gridLayout_17.addWidget(self.label_50, 3, 0, 1, 1)

        self.sc_led_11 = Led(self.groupBox_15)
        self.sc_led_11.setObjectName(u"sc_led_11")
        self.sc_led_11.setFont(font5)

        self.gridLayout_17.addWidget(self.sc_led_11, 3, 1, 1, 1)

        self.sample_freq_11 = QLabel(self.groupBox_15)
        self.sample_freq_11.setObjectName(u"sample_freq_11")
        self.sample_freq_11.setFont(font5)

        self.gridLayout_17.addWidget(self.sample_freq_11, 0, 1, 1, 1)

        self.value_11 = QLabel(self.groupBox_15)
        self.value_11.setObjectName(u"value_11")
        self.value_11.setFont(font5)

        self.gridLayout_17.addWidget(self.value_11, 1, 1, 1, 1)


        self.gridLayout_23.addWidget(self.groupBox_15, 1, 2, 1, 1)

        self.groupBox_16 = QGroupBox(self.groupBox_4)
        self.groupBox_16.setObjectName(u"groupBox_16")
        self.groupBox_16.setFont(font2)
        self.gridLayout_18 = QGridLayout(self.groupBox_16)
        self.gridLayout_18.setObjectName(u"gridLayout_18")
        self.label_51 = QLabel(self.groupBox_16)
        self.label_51.setObjectName(u"label_51")
        self.label_51.setFont(font5)
        self.label_51.setAlignment(Qt.AlignCenter)

        self.gridLayout_18.addWidget(self.label_51, 0, 0, 1, 1)

        self.label_52 = QLabel(self.groupBox_16)
        self.label_52.setObjectName(u"label_52")
        self.label_52.setFont(font5)
        self.label_52.setAlignment(Qt.AlignCenter)

        self.gridLayout_18.addWidget(self.label_52, 1, 0, 1, 1)

        self.label_53 = QLabel(self.groupBox_16)
        self.label_53.setObjectName(u"label_53")
        self.label_53.setFont(font5)
        self.label_53.setAlignment(Qt.AlignCenter)

        self.gridLayout_18.addWidget(self.label_53, 2, 0, 1, 1)

        self.oc_led_12 = Led(self.groupBox_16)
        self.oc_led_12.setObjectName(u"oc_led_12")
        self.oc_led_12.setFont(font5)

        self.gridLayout_18.addWidget(self.oc_led_12, 2, 1, 1, 1)

        self.label_54 = QLabel(self.groupBox_16)
        self.label_54.setObjectName(u"label_54")
        self.label_54.setFont(font5)
        self.label_54.setAlignment(Qt.AlignCenter)

        self.gridLayout_18.addWidget(self.label_54, 3, 0, 1, 1)

        self.sc_led_12 = Led(self.groupBox_16)
        self.sc_led_12.setObjectName(u"sc_led_12")
        self.sc_led_12.setFont(font5)

        self.gridLayout_18.addWidget(self.sc_led_12, 3, 1, 1, 1)

        self.sample_freq_12 = QLabel(self.groupBox_16)
        self.sample_freq_12.setObjectName(u"sample_freq_12")
        self.sample_freq_12.setFont(font5)

        self.gridLayout_18.addWidget(self.sample_freq_12, 0, 1, 1, 1)

        self.value_12 = QLabel(self.groupBox_16)
        self.value_12.setObjectName(u"value_12")
        self.value_12.setFont(font5)

        self.gridLayout_18.addWidget(self.value_12, 1, 1, 1, 1)


        self.gridLayout_23.addWidget(self.groupBox_16, 1, 3, 1, 1)

        self.groupBox_17 = QGroupBox(self.groupBox_4)
        self.groupBox_17.setObjectName(u"groupBox_17")
        self.groupBox_17.setFont(font2)
        self.gridLayout_19 = QGridLayout(self.groupBox_17)
        self.gridLayout_19.setObjectName(u"gridLayout_19")
        self.label_55 = QLabel(self.groupBox_17)
        self.label_55.setObjectName(u"label_55")
        self.label_55.setFont(font5)
        self.label_55.setAlignment(Qt.AlignCenter)

        self.gridLayout_19.addWidget(self.label_55, 0, 0, 1, 1)

        self.label_56 = QLabel(self.groupBox_17)
        self.label_56.setObjectName(u"label_56")
        self.label_56.setFont(font5)
        self.label_56.setAlignment(Qt.AlignCenter)

        self.gridLayout_19.addWidget(self.label_56, 1, 0, 1, 1)

        self.label_57 = QLabel(self.groupBox_17)
        self.label_57.setObjectName(u"label_57")
        self.label_57.setFont(font5)
        self.label_57.setAlignment(Qt.AlignCenter)

        self.gridLayout_19.addWidget(self.label_57, 2, 0, 1, 1)

        self.oc_led_13 = Led(self.groupBox_17)
        self.oc_led_13.setObjectName(u"oc_led_13")
        self.oc_led_13.setFont(font5)

        self.gridLayout_19.addWidget(self.oc_led_13, 2, 1, 1, 1)

        self.label_58 = QLabel(self.groupBox_17)
        self.label_58.setObjectName(u"label_58")
        self.label_58.setFont(font5)
        self.label_58.setAlignment(Qt.AlignCenter)

        self.gridLayout_19.addWidget(self.label_58, 3, 0, 1, 1)

        self.sc_led_13 = Led(self.groupBox_17)
        self.sc_led_13.setObjectName(u"sc_led_13")
        self.sc_led_13.setFont(font5)

        self.gridLayout_19.addWidget(self.sc_led_13, 3, 1, 1, 1)

        self.sample_freq_13 = QLabel(self.groupBox_17)
        self.sample_freq_13.setObjectName(u"sample_freq_13")
        self.sample_freq_13.setFont(font5)

        self.gridLayout_19.addWidget(self.sample_freq_13, 0, 1, 1, 1)

        self.value_13 = QLabel(self.groupBox_17)
        self.value_13.setObjectName(u"value_13")
        self.value_13.setFont(font5)

        self.gridLayout_19.addWidget(self.value_13, 1, 1, 1, 1)


        self.gridLayout_23.addWidget(self.groupBox_17, 1, 4, 1, 1)

        self.groupBox_18 = QGroupBox(self.groupBox_4)
        self.groupBox_18.setObjectName(u"groupBox_18")
        self.groupBox_18.setFont(font2)
        self.gridLayout_20 = QGridLayout(self.groupBox_18)
        self.gridLayout_20.setObjectName(u"gridLayout_20")
        self.label_59 = QLabel(self.groupBox_18)
        self.label_59.setObjectName(u"label_59")
        self.label_59.setFont(font5)
        self.label_59.setAlignment(Qt.AlignCenter)

        self.gridLayout_20.addWidget(self.label_59, 0, 0, 1, 1)

        self.label_60 = QLabel(self.groupBox_18)
        self.label_60.setObjectName(u"label_60")
        self.label_60.setFont(font5)
        self.label_60.setAlignment(Qt.AlignCenter)

        self.gridLayout_20.addWidget(self.label_60, 1, 0, 1, 1)

        self.label_61 = QLabel(self.groupBox_18)
        self.label_61.setObjectName(u"label_61")
        self.label_61.setFont(font5)
        self.label_61.setAlignment(Qt.AlignCenter)

        self.gridLayout_20.addWidget(self.label_61, 2, 0, 1, 1)

        self.oc_led_14 = Led(self.groupBox_18)
        self.oc_led_14.setObjectName(u"oc_led_14")
        self.oc_led_14.setFont(font5)

        self.gridLayout_20.addWidget(self.oc_led_14, 2, 1, 1, 1)

        self.label_62 = QLabel(self.groupBox_18)
        self.label_62.setObjectName(u"label_62")
        self.label_62.setFont(font5)
        self.label_62.setAlignment(Qt.AlignCenter)

        self.gridLayout_20.addWidget(self.label_62, 3, 0, 1, 1)

        self.sc_led_14 = Led(self.groupBox_18)
        self.sc_led_14.setObjectName(u"sc_led_14")
        self.sc_led_14.setFont(font5)

        self.gridLayout_20.addWidget(self.sc_led_14, 3, 1, 1, 1)

        self.sample_freq_14 = QLabel(self.groupBox_18)
        self.sample_freq_14.setObjectName(u"sample_freq_14")
        self.sample_freq_14.setFont(font5)

        self.gridLayout_20.addWidget(self.sample_freq_14, 0, 1, 1, 1)

        self.value_14 = QLabel(self.groupBox_18)
        self.value_14.setObjectName(u"value_14")
        self.value_14.setFont(font5)

        self.gridLayout_20.addWidget(self.value_14, 1, 1, 1, 1)


        self.gridLayout_23.addWidget(self.groupBox_18, 1, 5, 1, 1)

        self.groupBox_19 = QGroupBox(self.groupBox_4)
        self.groupBox_19.setObjectName(u"groupBox_19")
        self.groupBox_19.setFont(font2)
        self.gridLayout_21 = QGridLayout(self.groupBox_19)
        self.gridLayout_21.setObjectName(u"gridLayout_21")
        self.label_63 = QLabel(self.groupBox_19)
        self.label_63.setObjectName(u"label_63")
        self.label_63.setFont(font5)
        self.label_63.setAlignment(Qt.AlignCenter)

        self.gridLayout_21.addWidget(self.label_63, 0, 0, 1, 1)

        self.label_64 = QLabel(self.groupBox_19)
        self.label_64.setObjectName(u"label_64")
        self.label_64.setFont(font5)
        self.label_64.setAlignment(Qt.AlignCenter)

        self.gridLayout_21.addWidget(self.label_64, 1, 0, 1, 1)

        self.label_65 = QLabel(self.groupBox_19)
        self.label_65.setObjectName(u"label_65")
        self.label_65.setFont(font5)
        self.label_65.setAlignment(Qt.AlignCenter)

        self.gridLayout_21.addWidget(self.label_65, 2, 0, 1, 1)

        self.oc_led_15 = Led(self.groupBox_19)
        self.oc_led_15.setObjectName(u"oc_led_15")
        self.oc_led_15.setFont(font5)

        self.gridLayout_21.addWidget(self.oc_led_15, 2, 1, 1, 1)

        self.label_66 = QLabel(self.groupBox_19)
        self.label_66.setObjectName(u"label_66")
        self.label_66.setFont(font5)
        self.label_66.setAlignment(Qt.AlignCenter)

        self.gridLayout_21.addWidget(self.label_66, 3, 0, 1, 1)

        self.sc_led_15 = Led(self.groupBox_19)
        self.sc_led_15.setObjectName(u"sc_led_15")
        self.sc_led_15.setFont(font5)

        self.gridLayout_21.addWidget(self.sc_led_15, 3, 1, 1, 1)

        self.sample_freq_15 = QLabel(self.groupBox_19)
        self.sample_freq_15.setObjectName(u"sample_freq_15")
        self.sample_freq_15.setFont(font5)

        self.gridLayout_21.addWidget(self.sample_freq_15, 0, 1, 1, 1)

        self.value_15 = QLabel(self.groupBox_19)
        self.value_15.setObjectName(u"value_15")
        self.value_15.setFont(font5)

        self.gridLayout_21.addWidget(self.value_15, 1, 1, 1, 1)


        self.gridLayout_23.addWidget(self.groupBox_19, 1, 6, 1, 1)

        self.groupBox_20 = QGroupBox(self.groupBox_4)
        self.groupBox_20.setObjectName(u"groupBox_20")
        self.groupBox_20.setFont(font2)
        self.gridLayout_22 = QGridLayout(self.groupBox_20)
        self.gridLayout_22.setObjectName(u"gridLayout_22")
        self.label_67 = QLabel(self.groupBox_20)
        self.label_67.setObjectName(u"label_67")
        self.label_67.setFont(font5)
        self.label_67.setAlignment(Qt.AlignCenter)

        self.gridLayout_22.addWidget(self.label_67, 0, 0, 1, 1)

        self.label_68 = QLabel(self.groupBox_20)
        self.label_68.setObjectName(u"label_68")
        self.label_68.setFont(font5)
        self.label_68.setAlignment(Qt.AlignCenter)

        self.gridLayout_22.addWidget(self.label_68, 1, 0, 1, 1)

        self.label_69 = QLabel(self.groupBox_20)
        self.label_69.setObjectName(u"label_69")
        self.label_69.setFont(font5)
        self.label_69.setAlignment(Qt.AlignCenter)

        self.gridLayout_22.addWidget(self.label_69, 2, 0, 1, 1)

        self.oc_led_16 = Led(self.groupBox_20)
        self.oc_led_16.setObjectName(u"oc_led_16")
        self.oc_led_16.setFont(font5)

        self.gridLayout_22.addWidget(self.oc_led_16, 2, 1, 1, 1)

        self.label_70 = QLabel(self.groupBox_20)
        self.label_70.setObjectName(u"label_70")
        self.label_70.setFont(font5)
        self.label_70.setAlignment(Qt.AlignCenter)

        self.gridLayout_22.addWidget(self.label_70, 3, 0, 1, 1)

        self.sc_led_16 = Led(self.groupBox_20)
        self.sc_led_16.setObjectName(u"sc_led_16")
        self.sc_led_16.setFont(font5)

        self.gridLayout_22.addWidget(self.sc_led_16, 3, 1, 1, 1)

        self.sample_freq_16 = QLabel(self.groupBox_20)
        self.sample_freq_16.setObjectName(u"sample_freq_16")
        self.sample_freq_16.setFont(font5)

        self.gridLayout_22.addWidget(self.sample_freq_16, 0, 1, 1, 1)

        self.value_16 = QLabel(self.groupBox_20)
        self.value_16.setObjectName(u"value_16")
        self.value_16.setFont(font5)

        self.gridLayout_22.addWidget(self.value_16, 1, 1, 1, 1)


        self.gridLayout_23.addWidget(self.groupBox_20, 1, 7, 1, 1)


        self.gridLayout_3.addWidget(self.groupBox_4, 2, 1, 4, 3)

        MainWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QStatusBar(MainWindow)
        self.statusbar.setObjectName(u"statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)

        QMetaObject.connectSlotsByName(MainWindow)
    # setupUi

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QCoreApplication.translate("MainWindow", u"MainWindow", None))
        self.groupBox_2.setTitle(QCoreApplication.translate("MainWindow", u"Sensors", None))
        self.groupBox_3.setTitle(QCoreApplication.translate("MainWindow", u"Console", None))
        self.pushButton.setText(QCoreApplication.translate("MainWindow", u"Send!", None))
        self.label_3.setText("")
        self.groupBox.setTitle(QCoreApplication.translate("MainWindow", u"Platform status", None))
        self.label_7.setText(QCoreApplication.translate("MainWindow", u"Message \u27e8\u0394t\u27e9:", None))
        self.message_time.setText(QCoreApplication.translate("MainWindow", u"0 s", None))
        self.label_43.setText(QCoreApplication.translate("MainWindow", u"Status:", None))
        self.status.setText(QCoreApplication.translate("MainWindow", u"Online", None))
        self.label_2.setText(QCoreApplication.translate("MainWindow", u"Sensor mode:", None))
        self.mode_selection.setItemText(0, QCoreApplication.translate("MainWindow", u"Temperature", None))
        self.mode_selection.setItemText(1, QCoreApplication.translate("MainWindow", u"Resistance", None))
        self.mode_selection.setItemText(2, QCoreApplication.translate("MainWindow", u"None", None))

        self.groupBox_4.setTitle(QCoreApplication.translate("MainWindow", u"Sensor status", None))
        self.groupBox_5.setTitle(QCoreApplication.translate("MainWindow", u"Sensor 1", None))
        self.label_5.setText(QCoreApplication.translate("MainWindow", u"Sample\n"
"Freq", None))
        self.label_6.setText(QCoreApplication.translate("MainWindow", u"Value", None))
        self.label.setText(QCoreApplication.translate("MainWindow", u"OC", None))
        self.label_4.setText(QCoreApplication.translate("MainWindow", u"SC", None))
        self.sample_freq_1.setText(QCoreApplication.translate("MainWindow", u"0", None))
        self.value_1.setText(QCoreApplication.translate("MainWindow", u"0", None))
        self.groupBox_6.setTitle(QCoreApplication.translate("MainWindow", u"Sensor 2", None))
        self.label_9.setText(QCoreApplication.translate("MainWindow", u"Sample\n"
"Freq", None))
        self.label_10.setText(QCoreApplication.translate("MainWindow", u"Value", None))
        self.label_11.setText(QCoreApplication.translate("MainWindow", u"OC", None))
        self.label_12.setText(QCoreApplication.translate("MainWindow", u"SC", None))
        self.sample_freq_2.setText(QCoreApplication.translate("MainWindow", u"0", None))
        self.value_2.setText(QCoreApplication.translate("MainWindow", u"0", None))
        self.groupBox_7.setTitle(QCoreApplication.translate("MainWindow", u"Sensor 3", None))
        self.label_13.setText(QCoreApplication.translate("MainWindow", u"Sample\n"
"Freq", None))
        self.label_14.setText(QCoreApplication.translate("MainWindow", u"Value", None))
        self.label_15.setText(QCoreApplication.translate("MainWindow", u"OC", None))
        self.label_16.setText(QCoreApplication.translate("MainWindow", u"SC", None))
        self.sample_freq_3.setText(QCoreApplication.translate("MainWindow", u"0", None))
        self.value_3.setText(QCoreApplication.translate("MainWindow", u"0", None))
        self.groupBox_8.setTitle(QCoreApplication.translate("MainWindow", u"Sensor 4", None))
        self.label_17.setText(QCoreApplication.translate("MainWindow", u"Sample\n"
"Freq", None))
        self.label_18.setText(QCoreApplication.translate("MainWindow", u"Value", None))
        self.label_19.setText(QCoreApplication.translate("MainWindow", u"OC", None))
        self.label_20.setText(QCoreApplication.translate("MainWindow", u"SC", None))
        self.sample_freq_4.setText(QCoreApplication.translate("MainWindow", u"0", None))
        self.value_4.setText(QCoreApplication.translate("MainWindow", u"0", None))
        self.groupBox_9.setTitle(QCoreApplication.translate("MainWindow", u"Sensor 5", None))
        self.label_21.setText(QCoreApplication.translate("MainWindow", u"Sample\n"
"Freq", None))
        self.label_22.setText(QCoreApplication.translate("MainWindow", u"Value", None))
        self.label_23.setText(QCoreApplication.translate("MainWindow", u"OC", None))
        self.label_24.setText(QCoreApplication.translate("MainWindow", u"SC", None))
        self.sample_freq_5.setText(QCoreApplication.translate("MainWindow", u"0", None))
        self.value_5.setText(QCoreApplication.translate("MainWindow", u"0", None))
        self.groupBox_10.setTitle(QCoreApplication.translate("MainWindow", u"Sensor 6", None))
        self.label_25.setText(QCoreApplication.translate("MainWindow", u"Sample\n"
"Freq", None))
        self.label_26.setText(QCoreApplication.translate("MainWindow", u"Value", None))
        self.label_27.setText(QCoreApplication.translate("MainWindow", u"OC", None))
        self.label_28.setText(QCoreApplication.translate("MainWindow", u"SC", None))
        self.sample_freq_6.setText(QCoreApplication.translate("MainWindow", u"0", None))
        self.value_6.setText(QCoreApplication.translate("MainWindow", u"0", None))
        self.groupBox_11.setTitle(QCoreApplication.translate("MainWindow", u"Sensor 7", None))
        self.label_29.setText(QCoreApplication.translate("MainWindow", u"Sample\n"
"Freq", None))
        self.label_30.setText(QCoreApplication.translate("MainWindow", u"Value", None))
        self.label_31.setText(QCoreApplication.translate("MainWindow", u"OC", None))
        self.label_32.setText(QCoreApplication.translate("MainWindow", u"SC", None))
        self.sample_freq_7.setText(QCoreApplication.translate("MainWindow", u"0", None))
        self.value_7.setText(QCoreApplication.translate("MainWindow", u"0", None))
        self.groupBox_12.setTitle(QCoreApplication.translate("MainWindow", u"Sensor 8", None))
        self.label_33.setText(QCoreApplication.translate("MainWindow", u"Sample\n"
"Freq", None))
        self.label_34.setText(QCoreApplication.translate("MainWindow", u"Value", None))
        self.label_35.setText(QCoreApplication.translate("MainWindow", u"OC", None))
        self.label_36.setText(QCoreApplication.translate("MainWindow", u"SC", None))
        self.sample_freq_8.setText(QCoreApplication.translate("MainWindow", u"0", None))
        self.value_8.setText(QCoreApplication.translate("MainWindow", u"0", None))
        self.groupBox_13.setTitle(QCoreApplication.translate("MainWindow", u"Sensor 9", None))
        self.label_37.setText(QCoreApplication.translate("MainWindow", u"Sample\n"
"Freq", None))
        self.label_38.setText(QCoreApplication.translate("MainWindow", u"Value", None))
        self.label_39.setText(QCoreApplication.translate("MainWindow", u"OC", None))
        self.label_40.setText(QCoreApplication.translate("MainWindow", u"SC", None))
        self.sample_freq_9.setText(QCoreApplication.translate("MainWindow", u"0", None))
        self.value_9.setText(QCoreApplication.translate("MainWindow", u"0", None))
        self.groupBox_14.setTitle(QCoreApplication.translate("MainWindow", u"Sensor 10", None))
        self.label_41.setText(QCoreApplication.translate("MainWindow", u"Sample\n"
"Freq", None))
        self.label_42.setText(QCoreApplication.translate("MainWindow", u"Value", None))
        self.label_45.setText(QCoreApplication.translate("MainWindow", u"OC", None))
        self.label_46.setText(QCoreApplication.translate("MainWindow", u"SC", None))
        self.sample_freq_10.setText(QCoreApplication.translate("MainWindow", u"0", None))
        self.value_10.setText(QCoreApplication.translate("MainWindow", u"0", None))
        self.groupBox_15.setTitle(QCoreApplication.translate("MainWindow", u"Sensor 11", None))
        self.label_47.setText(QCoreApplication.translate("MainWindow", u"Sample\n"
"Freq", None))
        self.label_48.setText(QCoreApplication.translate("MainWindow", u"Value", None))
        self.label_49.setText(QCoreApplication.translate("MainWindow", u"OC", None))
        self.label_50.setText(QCoreApplication.translate("MainWindow", u"SC", None))
        self.sample_freq_11.setText(QCoreApplication.translate("MainWindow", u"0", None))
        self.value_11.setText(QCoreApplication.translate("MainWindow", u"0", None))
        self.groupBox_16.setTitle(QCoreApplication.translate("MainWindow", u"Sensor 12", None))
        self.label_51.setText(QCoreApplication.translate("MainWindow", u"Sample\n"
"Freq", None))
        self.label_52.setText(QCoreApplication.translate("MainWindow", u"Value", None))
        self.label_53.setText(QCoreApplication.translate("MainWindow", u"OC", None))
        self.label_54.setText(QCoreApplication.translate("MainWindow", u"SC", None))
        self.sample_freq_12.setText(QCoreApplication.translate("MainWindow", u"0", None))
        self.value_12.setText(QCoreApplication.translate("MainWindow", u"0", None))
        self.groupBox_17.setTitle(QCoreApplication.translate("MainWindow", u"Sensor 13", None))
        self.label_55.setText(QCoreApplication.translate("MainWindow", u"Sample\n"
"Freq", None))
        self.label_56.setText(QCoreApplication.translate("MainWindow", u"Value", None))
        self.label_57.setText(QCoreApplication.translate("MainWindow", u"OC", None))
        self.label_58.setText(QCoreApplication.translate("MainWindow", u"SC", None))
        self.sample_freq_13.setText(QCoreApplication.translate("MainWindow", u"0", None))
        self.value_13.setText(QCoreApplication.translate("MainWindow", u"0", None))
        self.groupBox_18.setTitle(QCoreApplication.translate("MainWindow", u"Sensor 14", None))
        self.label_59.setText(QCoreApplication.translate("MainWindow", u"Sample\n"
"Freq", None))
        self.label_60.setText(QCoreApplication.translate("MainWindow", u"Value", None))
        self.label_61.setText(QCoreApplication.translate("MainWindow", u"OC", None))
        self.label_62.setText(QCoreApplication.translate("MainWindow", u"SC", None))
        self.sample_freq_14.setText(QCoreApplication.translate("MainWindow", u"0", None))
        self.value_14.setText(QCoreApplication.translate("MainWindow", u"0", None))
        self.groupBox_19.setTitle(QCoreApplication.translate("MainWindow", u"Sensor 15", None))
        self.label_63.setText(QCoreApplication.translate("MainWindow", u"Sample\n"
"Freq", None))
        self.label_64.setText(QCoreApplication.translate("MainWindow", u"Value", None))
        self.label_65.setText(QCoreApplication.translate("MainWindow", u"OC", None))
        self.label_66.setText(QCoreApplication.translate("MainWindow", u"SC", None))
        self.sample_freq_15.setText(QCoreApplication.translate("MainWindow", u"0", None))
        self.value_15.setText(QCoreApplication.translate("MainWindow", u"0", None))
        self.groupBox_20.setTitle(QCoreApplication.translate("MainWindow", u"Sensor 16", None))
        self.label_67.setText(QCoreApplication.translate("MainWindow", u"Sample\n"
"Freq", None))
        self.label_68.setText(QCoreApplication.translate("MainWindow", u"Value", None))
        self.label_69.setText(QCoreApplication.translate("MainWindow", u"OC", None))
        self.label_70.setText(QCoreApplication.translate("MainWindow", u"SC", None))
        self.sample_freq_16.setText(QCoreApplication.translate("MainWindow", u"0", None))
        self.value_16.setText(QCoreApplication.translate("MainWindow", u"0", None))
    # retranslateUi

