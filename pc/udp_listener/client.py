import socket
from time import sleep


class UdpClient:
    def __init__(self):
        self.server_ip = '192.168.178.1'
        self.server_port = 5006
        self.server_address = (self.server_ip, self.server_port)
        self.modes = {"Normal": 1, "None": 2, "Resistance": 3}
        self.sensors = {"Platform": 0,  "Temperature": 1}

    def send_test_message(self):
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as s:
            sensor_amount = 8  # 16 sensors (2 per message)
            temp_range = range(0, 200)
            for i in temp_range:
                for x in range(sensor_amount):
                    message = bytearray([0x00+i, 0x00, 0x00+2*x, 0x00, 0xC8-i, 0x00, 0x01+2*x, 0x00])
                    s.sendto(message, self.server_address)
                    sleep(0.1)
                    print(f"Sent: {message}")
                    
    def send(self, message):
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as s:
            try:
                s.sendto(message, self.server_address)
                print(f"Sent: {message}, to: {self.server_address[0]}:{self.server_address[1]}")
            except Exception as e:
                print(f"Error: {e}")
                
    def mode_none(self, target):
        m = self.modes["None"]
        t = self.sensors[target]
        m = (m << 8) | t
        m = m.to_bytes(2, "little")      
        self.send(m)
    
    def mode_normal(self, target):
        m = self.modes["Normal"]
        t = self.sensors[target]
        m = (m << 8) | t
        m = m.to_bytes(2, "little")      
        self.send(m)

    def mode_resistance(self, target):
        m = self.modes["Resistance"]
        t = self.sensors[target]
        m = (m << 8) | t
        m = m.to_bytes(2, "little")
        self.send(m)
        
    def alive_check(self):
        pass


if __name__ == "__main__":
    client = UdpClient()
    client.mode_normal("Temperature")
    #client.mode_none("Temperature")
