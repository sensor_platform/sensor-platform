from PySide2 import QtCore, QtGui, QtWidgets
from PySide2.QtCore import Qt


class Led(QtWidgets.QWidget):
    def __init__(self, *args, **kwargs):
        super(Led, self).__init__(*args, **kwargs)

        layout = QtWidgets.QVBoxLayout()
        self.led = QtWidgets.QWidget()
        layout.addWidget(self.led)

        self.led_off = Qt.black
        self.led_on = Qt.red
        self.color = self.led_off

        self.setLayout(layout)

    def paintEvent(self, event):
        painter = QtGui.QPainter(self)
        painter.setBrush(QtGui.QBrush(self.color, Qt.SolidPattern))
        painter.drawEllipse(self.led.geometry().x(), self.led.geometry().y()-4, 10, 10)

    def on(self):
        self.color = self.led_on
        self.trigger_refresh()

    def off(self):
        self.color = self.led_off
        self.trigger_refresh()

    def trigger_refresh(self):
        self.update()
