import sys
import datetime
import socket
import select
import errno


class UdpServer:
    def __init__(self, blocking=1):
        self.client_ip = ''  # Accept any connections
        self.socket_port = 5006
        self.host_address = (self.client_ip, self.socket_port)
        self.blocking = blocking

    def receive(self):
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as s:
            s.bind(self.host_address)
            s.setblocking(self.blocking)
            try:
                data_available = select.select([s], [], [], 1)
                if data_available[0]:
                    data, addr = s.recvfrom(1024)  # Buffer-size is 1024 bytes
                    return data, addr
                return None
            except socket.error as e:
                err = e.args[0]
                if err == errno.EAGAIN or err == errno.EWOULDBLOCK:
                    return  # No data
                else:
                    print(f"Error occurred: {e}. \nClosing application...")
                    sys.exit(1)

    @staticmethod
    def check_device_status(self, ip_address):
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as s:
            print("Yo :>")


if __name__ == "__main__":
    server = UdpServer()
    while 1:
        data, addr = server.receive()
        time = (datetime.datetime.now()).time().strftime("%H:%M:%S.%f")
        print(f"Received {len(data)} bytes from '{addr[0]}:{addr[1]}' @{time}")
        print(f"Sensor No#{data[2]} has value {data[1] << 8 | data[0]}")  # 4th byte discarded
        print(f"Sensor No#{data[6]} has value {data[5] << 8 | data[4]}")  # 8th byte discarded
