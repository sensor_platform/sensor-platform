/* Nick van den Berg - IHC Systems B.V. */

#ifndef COMMUNICATION_SRC_MCP2517FD_HPP_
#define COMMUNICATION_SRC_MCP2517FD_HPP_

#include <string>
#include <chrono>
#include <cstdint>
#include <tuple>

#include "mbed.h"
#include "ThisThread.h"
//#include "SPI.h"
//#include "DigitalInOut.h"

//#include "msp430.h"

//#include "pin.hpp"
//#include "gpio.hpp"
//#include "spi.hpp"


namespace MCP2517FD
{

    /* Datatypes */
    //None

    /* Classes */
    class Mcp2517fd: public mbed::SPI
    {
    public:
        /* Constructors */
        Mcp2517fd(PinName ppSclk, PinName ppSdo, PinName ppSdi,
        PinName chipSelectPin);
      /* Power Management */
        void wake();        // Device exists power down mode
        void sleep();       // Device enters power down mode
        /* SPI */
        void select();
        void deselect();
        void resetSpi() ;
        void resetDevice() ;
        /* Registers */
        void sendData(std::uint16_t data, std::uint8_t sensorNo);
        std::pair<std::tuple<int16_t, uint16_t>, std::tuple<int16_t, uint16_t>> readData(); // Returns PAIR of TUPLES
        void configurePayload();
        void addDataToPayload(int16_t data1, int16_t data2);
        void sendPayload();
        void testRegisters();
        void sendTestMessage();
    private:
        /* Methods */
        /* initialization */
        void init();
        /* ADS1148 internal multiplexer (ADS1148.pfd p48) */

        /* External multiplexer */

        /* Register manipulation */
        std::uint32_t readRegister(const std::uint16_t& address);
        void writeRegister(const std::uint16_t& address, const std::uint32_t& data);
        /* Variables */
        /* Commands */
        /* Reading and writing from/to registers or message memory is done with the read/write commands. A transaction
         * starts with nCS going LOW. Then 4 command bits are sent, followed by 12 address bits. The chip accepts new
         * data or puts data on the bus as long as nCS is LOW. The data that is put on the bus starts at the address
         * given and is incremented with 1 for as long as nCS is LOW. The transaction only ends when nCS is pulled
         * high. (MCP2517FD.pdf p67-p69)                                                                                */
        static constexpr std::uint8_t commandReset = 0x0;
        static constexpr std::uint8_t commandRead  = 0x3;
        static constexpr std::uint8_t commandWrite = 0x2; // Writes must always be a multiple of 4 bytes. Else data is discarded.
        static constexpr std::uint8_t commandNoOperation = 0x00;
        /* addresses */
        static constexpr  std::uint16_t addressReset                            = 0x0000;
        static constexpr  std::uint16_t addressClock                            = 0x0E00;
        static constexpr  std::uint16_t addressInputOutput                      = 0x0E04;
        static constexpr  std::uint16_t addressCrc                              = 0x0E08;
        static constexpr  std::uint16_t addressCanControl                       = 0x0000;
        static constexpr  std::uint16_t addressNominalBitTime                   = 0x0004;
        static constexpr  std::uint16_t addressDataBitTime                      = 0x0008;
        static constexpr  std::uint16_t addressTransmitterDelayCompensation     = 0x000C;
        static constexpr std::uint16_t addressTxFifoControl                     = 0x005C;
        static constexpr std::uint16_t addressTxFifoStatus                      = 0x0060;
        static constexpr std::uint16_t addressTxFifoUserAddress                 = 0x0064;
        static constexpr std::uint16_t addressRxFifoControl                     = 0x0068;
        static constexpr std::uint16_t addressRxFifoStatus                      = 0x006C;
        static constexpr std::uint16_t addressRxFifoUserAddress                 = 0x0070;
        static constexpr std::uint16_t addressRxFilterControl                   = 0x01D0; // Filter 0 to 3
        static constexpr std::uint16_t addressRxFilterObject                    = 0x01F0; // Filter 0
        static constexpr std::uint16_t addressRxFilterMask                      = 0x01F4; // Filter 0
        /* settings */
        static constexpr std::uint32_t settingsClock                           = 0x00000060;
        static constexpr std::uint32_t settingsInputOutput                     = 0x20000000; //0x20010103 (<- this turns on GPIO0) // Push/Pull mode, SOF on CLKOpin
        static constexpr std::uint32_t settingsCrc                             = 0x00000000; // No CRC
        static constexpr std::uint32_t settingsCanControl                      = 0x00010100;
        static constexpr std::uint32_t settingsNominalBitTime                  = 0x003E0F0F; // 500K_2M MCP25xxFD_family_user_guide.pdf p.21
        static constexpr std::uint32_t settingsDataBitTime                     = 0x000E0303; // 500K_2M MCP25xxFD_family_user_guide.pdf p.21
        static constexpr std::uint32_t settingsTransmitterDelayCompensation    = 0x00020F00; // 500K_2M MCP25xxFD_family_user_guide.pdf p.21
        static constexpr std::uint32_t settingsTransmitEventFifoControl        = 0x01000080;
        static constexpr std::uint32_t settingsTxFifoControl                   = 0x03000080; // 0x03000080;
        static constexpr std::uint32_t settingsRxFifoControl                   = 0x03000000;
        static constexpr std::uint32_t settingsRxFilterControl                 = 0x00000082;
        static constexpr std::uint32_t settingsRxFilterObject                  = 0x00000000;
        static constexpr std::uint32_t settingsRxFilterMask                    = 0x00000000;
        static constexpr std::uint32_t settingsCanConfigurationMode            = 0x04000000;
        static constexpr std::uint32_t settingsCanDebugModeExt                 = 0x05010100;
        static constexpr std::uint32_t settingsCanDebugModeInt                 = 0x02010100;
        static constexpr std::uint32_t settingsCanLegacyMode                   = 0x06010100;
        static constexpr std::uint32_t requestSend                             = 0x03000380; // 0x03000380;
        static constexpr std::uint32_t incrementTail                           = 0x03000100;
        /* User-Vars */
        static std::uint32_t messageObjectAddr;
        static constexpr std::chrono::milliseconds resetTime = 160ms;   // 160mS (must be known at compile time)
        static constexpr std::uint8_t   SID     = 0x01;     // ID 2
        static constexpr std::uint8_t   SID11   = 0x00;     // 11th ID bit (can be used in FD mode)
        static constexpr std::uint32_t  EID     = 0x0;      // Extended identifier (unused)
        static constexpr std::uint16_t  DLC     = 0x4;      // 4 byte message
        static constexpr std::uint8_t   IDE     = 0x0;      // Extended identifier disabled (use 11 bit ID)
        static constexpr std::uint8_t   RTR     = 0x0;      // This system uses no remote transmission requests
        static constexpr std::uint8_t   FDF     = 0x1;      // Normal CAN frame (non FD) (not possible during legacy mode)
        static constexpr std::uint8_t   BRS     = 0x0;      // Disable bit-rate switching (500kbps only ((no 2mbps data transmission)))
        static constexpr std::uint8_t   ESI     = 0x1;      // Error passive node
        static constexpr std::uint32_t  SEQ     = 0x0;      // Sequence bits not implemented on MCP2517FD hardware
        /* ID and message settings for transmission */
        static constexpr std::uint32_t t0 = (0x00000000 | (((static_cast<uint32_t>(0x00) & 0x03)<<30) | ((static_cast<uint32_t>(SID11) & 0x1)<<29) | ((EID & 0x3FFFF)<<11) | ((SID & 0x7FF)<<0)));
        static constexpr std::uint32_t t1 = (0x00000000 | (((SEQ & 0x00FFFFFE)<<9)                    | ((ESI & 0x01)<<8)                          | ((FDF & 0x01)<<7)     | ((BRS & 0x01)<<6)    | ((RTR & 0x01)<<5) | ((IDE & 0x01)<<4) | ((DLC & 0x0F)<<0)));
    };

    /* Prototypes */
    // None
}



#endif /* COMMUNICATION_SRC_MCP2517FD_HPP_ */
