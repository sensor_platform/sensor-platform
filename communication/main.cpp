#include <algorithm>
#include <string>
#include <tuple>

#include "mbed.h"
#include "EthernetInterface.h"
//include "LWIPStack.h"
#include "netif.h"

#include "src/mcp2517fd.hpp"

#define USE_LINEAR_APPROXIMATION 0

#define SDO_PIN     PB_5
#define SDI_PIN     PA_6
#define SCLK_PIN    PA_5
#define CS_PIN      PA_4


using MCP2517FD::Mcp2517fd;

// Ethernet network interface
EthernetInterface eth;
SocketAddress serverAddr;
SocketAddress recvAddr;
UDPSocket sock;
bool BLOCK_MODE = true;
int BLOCK_TIMEOUT = 100; // ms

// Ethernet (UDP) server address
const char* server_ip = "192.168.178.66";
const uint16_t server_port = 5006;

// CAN-bus interface
Mcp2517fd canController(SCLK_PIN, SDO_PIN, SDI_PIN, CS_PIN); // No SPIsettings since only one SPI IC used

// Remap USART to PA9 (TX) and PA10 (RX) for virtual COM
FileHandle *mbed::mbed_override_console(int fd)
{
    static BufferedSerial custom_target_console(PA_9, PA_10);
    return &custom_target_console;
}

double calculate_presice_resistance_from_adc_val(int16_t adc_val)
{
    uint16_t Rref = 820;
     // ADCout * Rref / (4 * 2^15) ( in two steps else value was truncated)
    double resistance = (adc_val*Rref);
    resistance = resistance / 131072;
    return resistance;
}

int16_t calculate_resistance_from_adc_val(int16_t adc_val)
{
    double resistance = calculate_presice_resistance_from_adc_val(adc_val);
    if(resistance <= 92) // Short-Circuit
    {
        resistance = 92; // Cap at -20 degrees C
    }
    else if(resistance >= 180) // Open-Circuit
    {
        resistance = 180; // Cap at 210 degrees C
    }
    return static_cast<int16_t>(resistance * 100);
}

int16_t calculate_temperature_from_adc_val(int16_t adc_val)
{
    double resistance = calculate_presice_resistance_from_adc_val(adc_val);
    double temperature;
    if(resistance >= 180) // 180 Ohm
    {
        temperature = 21000; // Cap at 210 degrees C 
    }
    else if(resistance <= 0) // 0 Ohm
    {
        temperature = -1000; // Cap at -10 degrees C  
    }
    else  // Calculate temp
    {
        #if USE_LINEAR_APPROXIMATION
        /* R = 0.37986*t + 100.30694 is used (from RTD research excel sheet: "Limited range 2") */
        temperature = (resistance - 100.30694)/0.37986;
        temperature = temperature * 100;
        #endif
        #if !USE_LINEAR_APPROXIMATION
        /* Use Callendar-Van Dusen equation for more precision */
        temperature = (-0.0039083+sqrt((0.0000152748 + 0.00000231*(1 - resistance/100))))/(-0.000001155);
        temperature = temperature * 100;
        #endif
    }
    return static_cast<int16_t>(temperature);
}

uint8_t check_short_or_open_circuit(int sensor_value)
{
    uint8_t short_or_open = 'G';    // Sensor good! :)
    if(sensor_value == 0x7FFF)      // Open-circuit
    {
        short_or_open = 'O';
    }
    else if(sensor_value == 0x0000) // Short-circuit
    {
        short_or_open = 'S';
    }
    return short_or_open;
}

void switch_mode(uint8_t* message)
{

}

int main()
{
    enum class Event {NORMAL = 0x1, NONE = 0x2, CONNECT = 0x3, DISCONNECT = 0x4};
    Event event = Event::CONNECT;
    enum class Sensor {PLATFORM = 0x0, TEMPERATURE = 0x1};
    while(1)
    {
        switch(event)
        {
            case Event::CONNECT: // Connect to server
            {
                // Bring up the ethernet interface
                printf("Connecting to ethernet interface...\n");
                nsapi_error_t status;
                do {
                    static int retryNo = 0;
                    status = eth.connect();
                    netif_set_hostname(netif_default, "CSPU"); // Connecting and reconnecting is required to set hostname (feature not available in MBEDos)
                    eth.disconnect();
                    status = eth.connect();
                    if (status != 0 )
                    {
                        retryNo++;
                        printf("Error connecting: %d! Retry No#%d\n", status, retryNo);
                        eth.disconnect();
                        ThisThread::sleep_for(1s);
                    }
                }while (status != 0);

                // Get assigned IP-address
                eth.get_ip_address(&serverAddr);
                printf("IP assigned: %s\n",     serverAddr.get_ip_address() ? serverAddr.get_ip_address() : "No IP");

                // Open datagram socket
                sock.open(&eth);
                sock.set_blocking(BLOCK_MODE);
                sock.set_timeout(BLOCK_TIMEOUT);

                // Set (target) server address
                //eth.gethostbyname(serverAddr, &serverAddr);
                serverAddr.set_ip_address(server_ip);
                serverAddr.set_port(server_port);

                // Bind socket to port
                sock.bind(server_port);
                printf("Socket bound to %s:%d!\n", server_ip, server_port);
                
                // Start normal event loop
                event = Event::NORMAL;
                break;
            }
            case Event::DISCONNECT: // If an error occurs close the socket and disconnect the ethernet interface, then try to reconnect.
            {
                printf("Closing socket and disconnecting from ethernet interface...\n");
                sock.close();
                eth.disconnect();
                printf("Attempting reconnect!\n");
                event = Event::CONNECT;
                break;
            }
            case Event::NORMAL:
            {
                std::pair<std::tuple<int16_t, uint16_t>, std::tuple<int16_t, uint16_t>> data = canController.readData(); // Data is pair of tuples
                // If data is received (ID != 0xFF)
                if(std::get<1>(data.first) != 0xFF && std::get<1>(data.second) != 0xFF)
                {
                    std::get<1>(data.first) = check_short_or_open_circuit(std::get<0>(data.first))<<8 | std::get<1>(data.first);
                    std::get<1>(data.second) = check_short_or_open_circuit(std::get<0>(data.second))<<8 | std::get<1>(data.second);
                    std::get<0>(data.first) = calculate_temperature_from_adc_val(std::get<0>(data.first));
                    std::get<0>(data.second) = calculate_temperature_from_adc_val(std::get<0>(data.second));
                    nsapi_size_or_error_t status = sock.sendto(serverAddr, &data, sizeof(data));
                    if (status < 0) // If an error occurd
                    {
                        printf("Error sending data: %d\n", status);
                        event = Event::DISCONNECT;
                        break;
                    }
                    printf("%d bytes sent to %s:%d!\n", status, server_ip, server_port);
                }
                else 
                {
                    //printf("Received nothing on CAN-bus\n");
                    //ThisThread::sleep_for(100ms);
                }
                break;
            }
            case Event::NONE:
            {
                break;
            }
            default:
            {
                break;
            }
        }
        // Read a message to see if event loop needs to change mode or if a request is made for a sensor to change mode
        char recvBuffer[10]{'\0'};
        nsapi_size_or_error_t status = sock.recvfrom(&recvAddr, recvBuffer, sizeof(recvBuffer));
        if(status == NSAPI_ERROR_WOULD_BLOCK)  // Do nothing, no data
        {
            // Empty
        }
        else if (status == NSAPI_ERROR_NO_SOCKET || status < 0) // Error, reconnect
        {
            printf("Error receiving data: %d\n", status);
            event = Event::DISCONNECT;
            break;
        }
        else if (status >= 0) // Data received
        {   
            recvBuffer[status] = '\0';
            printf("%d bytes received from %s:%d!\n", status, recvAddr.get_ip_address(), recvAddr.get_port());
            Sensor device = static_cast<Sensor>(recvBuffer[0]);
            Event device_event = static_cast<Event>(recvBuffer[1]);
            if(device == Sensor::PLATFORM)
            {
                event = device_event;
            }
            else if(device == Sensor::TEMPERATURE)
            {
                canController.sendData(static_cast<uint16_t>(device_event), 1);
            }
        }
    }
    return 0; // End of main
}
